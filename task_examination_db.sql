CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_purview` int(2) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  `last_login` varchar(100) DEFAULT NULL,
  `login_ip` varchar(100) DEFAULT NULL,
  `admin_password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

CREATE TABLE `bj_table` (
  `zhy_id` int(255) NOT NULL COMMENT '班级所属专业',
  `bj_name` varchar(50) NOT NULL COMMENT '班级名称',
  `bj_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '班级唯一标号',
  PRIMARY KEY (`bj_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `findpwd_table` (
  `find_key` varchar(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  PRIMARY KEY (`find_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `log_table` (
  `log_id` bigint(255) NOT NULL AUTO_INCREMENT COMMENT '日志唯一标识符',
  `log_content` varchar(100) DEFAULT NULL COMMENT '日志内容',
  `log_people` varchar(100) DEFAULT NULL COMMENT '产生该日志的人',
  `log_date` bigint(100) DEFAULT NULL COMMENT '产生该日志的时间',
  `log_ip` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=849 DEFAULT CHARSET=utf8;
CREATE TABLE `ls_table` (
  `ls_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '老师唯一标号',
  `ls_name` varchar(100) DEFAULT NULL COMMENT '师老姓名',
  `ls_email` varchar(50) DEFAULT NULL COMMENT '老师邮箱',
  `ls_tele` varchar(50) DEFAULT NULL COMMENT '老师电话',
  `xy_id` int(255) DEFAULT NULL COMMENT '老师所属的学院',
  `ls_tx` varchar(100) DEFAULT NULL COMMENT '老师的头像',
  `ls_yz` int(2) DEFAULT NULL COMMENT '老师是否被管理员验证',
  `ls_describe` text COMMENT '老师描述',
  `ls_xb` int(2) DEFAULT NULL COMMENT '老师性别',
  `ls_loginname` varchar(50) DEFAULT NULL COMMENT '老师登录名',
  `ls_loginpsw` varchar(100) DEFAULT NULL COMMENT '老师登录密码',
  PRIMARY KEY (`ls_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
CREATE TABLE `ly_table` (
  `ly_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '留言的唯一标记',
  `ly_title` varchar(200) DEFAULT NULL COMMENT '留言标题',
  `ly_date` varchar(50) DEFAULT NULL COMMENT '留言时间',
  `ly_hf` int(2) DEFAULT NULL,
  `ly_content` text COMMENT '留言内容',
  `xs_id` int(255) DEFAULT NULL,
  `hf_content` text,
  `ls_id` int(255) DEFAULT NULL,
  `hf_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ly_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(100) DEFAULT NULL,
  `news_content` text,
  `news_date` varchar(100) DEFAULT NULL,
  `news_department` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
CREATE TABLE `system` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(100) DEFAULT NULL,
  `site_descript` text,
  `site_email` varchar(50) DEFAULT NULL,
  `site_email_password` varchar(100) DEFAULT NULL,
  `page_size` varchar(5) DEFAULT NULL,
  `siteUrl` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
CREATE TABLE `wt_bj_table` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `wt_id` bigint(255) DEFAULT NULL,
  `bj_id` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

CREATE TABLE `wt_table` (
  `wt_id` bigint(255) NOT NULL AUTO_INCREMENT COMMENT '专题唯一标识符',
  `wt_name` varchar(100) DEFAULT NULL COMMENT '专题名',
  `wt_describe` text COMMENT '专题描述',
  `ls_id` int(255) DEFAULT NULL COMMENT '创建该专题的老师',
  `start_time` bigint(255) DEFAULT NULL COMMENT '交提该作业的开始时间',
  `end_time` bigint(255) DEFAULT NULL COMMENT '提交该作业的结束时间',
  `wt_type` varchar(3) DEFAULT NULL,
  `xy_id` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`wt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
CREATE TABLE `wt_xs_table` (
  `wt_id` bigint(255) NOT NULL,
  `xs_id` bigint(255) NOT NULL,
  `state` int(5) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `bj_id` bigint(255) DEFAULT NULL,
  `fs` int(100) DEFAULT NULL,
  PRIMARY KEY (`wt_id`,`xs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `xs_table` (
  `xs_id` bigint(255) NOT NULL AUTO_INCREMENT COMMENT '学生唯一标识符',
  `xs_name` varchar(100) DEFAULT NULL COMMENT '学生姓名',
  `xs_email` varchar(50) DEFAULT NULL COMMENT '学生姓名',
  `zhy_id` int(255) DEFAULT NULL COMMENT '学生所在专业',
  `xs_tele` varchar(50) DEFAULT NULL COMMENT '学生电话',
  `xs_tx` varchar(100) DEFAULT NULL COMMENT '学生头像',
  `bj_id` int(255) DEFAULT NULL COMMENT '学生所在班级',
  `xy_id` int(255) DEFAULT NULL COMMENT '学生所在学院',
  `xs_xb` int(2) DEFAULT NULL COMMENT '学生所在性别',
  `xs_loginname` varchar(50) DEFAULT NULL COMMENT '学生登录名',
  `xs_loginpwd` varchar(100) DEFAULT NULL COMMENT '学生登录密码',
  `xs_xh` int(255) DEFAULT NULL COMMENT '学号',
  PRIMARY KEY (`xs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
CREATE TABLE `xy_table` (
  `xy_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '学院的唯一标识符',
  `xy_name` varchar(100) NOT NULL COMMENT '学院名字',
  `xy_describe` text COMMENT '学院描述',
  PRIMARY KEY (`xy_id`),
  UNIQUE KEY `xy_index` (`xy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
CREATE TABLE `zhy_table` (
  `zhy_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '专业唯一标识符',
  `zhy_name` varchar(100) DEFAULT NULL COMMENT '专业名',
  `zhy_describe` text COMMENT '专业描述',
  `xy_id` int(255) DEFAULT NULL COMMENT '专业所在学院',
  PRIMARY KEY (`zhy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

CREATE TABLE `zy_table` (
  `zy_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '作业唯一标识符',
  `zy_name` varchar(100) DEFAULT NULL COMMENT '作业名',
  `xs_id` int(255) DEFAULT NULL COMMENT '作业所属的学生',
  `wt_id` int(255) DEFAULT NULL COMMENT '该作业所属的专题',
  `zy_file` varchar(255) DEFAULT NULL COMMENT '作业文件地址',
  `zy_content` text COMMENT '可以在网页直接浏览的作业内容',
  `zy_py` text COMMENT '老师对该作业的评语',
  `zy_fs` float(100,0) DEFAULT NULL COMMENT '老师所给的分数',
  `ls_id` bigint(255) DEFAULT NULL COMMENT '该作业所要提交给的老师',
  `bj_id` bigint(255) DEFAULT NULL,
  `tj_date` bigint(255) DEFAULT NULL,
  `zy_state` int(10) DEFAULT NULL,
  PRIMARY KEY (`zy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
INSERT INTO admin
   (`admin_id`, `admin_name`, `admin_purview`, `admin_email`, `last_login`, `login_ip`, `admin_password`)
VALUES
   (27, 'jxustie', 3, '1184350505@qq.com', '2011-07-20 17:48:38', '127.0.0.1', '1c204c566889c4ff03bc86cd05a400f5');
INSERT INTO system
   (`id`, `site_name`, `site_descript`, `site_email`, `site_email_password`, `page_size`, `siteUrl`)
VALUES
   (1, '江西理工大学作业提交;审核系统', '江西理工大学作业提交;审核系统 ', 'hebibo314@163.com', 'hebibo0314@.com', '10', 'http://localhost:8089/E_T_System');