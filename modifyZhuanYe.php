<?php
include 'fckeditor/fckeditor.php';
include_once'admin_core/services/ZhuanYeService.php';
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
$zhyId = $_GET["zhyId"];
$zhyService = new ZhuanYeService();
$zhy = $zhyService->getZhuanYeById($zhyId);
$xyService = new XueYuanService();
$xyList = $xyService->getAllList();
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('zhyDescribt');
$ed->BasePath=$sBasePath;
$ed->Height=300;
$ed->Value = $zhy->getZhy_describe();
$ed->Width='100%';
$fun = new fun();
$fun->closeDB();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
input{
	color:#3CC;
	width:200px;
	height:20px;
	}
-->
</style>
<script type="text/javascript"  src="js/comment.js"></script>
<script type="text/javascript"  src="js/ajax.js" charset="utf-8"></script>
<script type="text/javascript">
function submitForm()
{
	if(document.getElementById("zhyName").value=="")
	{
		alert("专业名不能为空！");
		document.getElementById("xyName").focus();
		}
		else
		{
			document.getElementById("zhyForm").submit();
		}
}
</script>
</head>

<body style="text-align:center;">
<center>

<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">添加专业</span></legend>
<form action="admin_core/control.php?action=upDateZhy" method="post" id="zhyForm"> 
<table width="100%">
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">所属学院/部门名：</td>
<td align="left" width="88%">
<select name="xyId">
<?php 
for($i=0; $i<count($xyList); $i++)
{
?>
<option value="<?php echo $xyList[$i]->getXy_id();?>"  <?php if($xyList[$i]->getXy_id()==$zhy->getXy()->getXy_id()){?>selected="selected"<?php } ?> >
<?php echo $xyList[$i]->getXy_name();?>
</option>
<?php 
}
?>
</select>
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">输入专业名：</td>
<td align="left" width="88%">
<input name="zhyName" id="zhyName" type="text" onfocus="lightBorder(this)" onblur="checkZhY(this)" value="<?php echo $zhy->getZhy_name(); ?>" />
<input name="oldName" id="oldName" type="hidden" onfocus="lightBorder(this)" onblur="checkZhY(this)" value="<?php echo $zhy->getZhy_name(); ?>" />
<input name="zhyId" type="hidden" value="<?php echo $zhy->getZhy_id(); ?>" />
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">输入专业简介：</td>
<td align="left">
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">
<input type="button" value=" 修 改  "   id="submitid"  onclick="submitForm()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
<td align="left">
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
</tr>
</table>
</form>
</fieldset>
</center>
</body>
</html>
