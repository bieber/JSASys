<?php
session_start();
include 'fckeditor/fckeditor.php';
include 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('lyContent');
$ed->BasePath=$sBasePath;
$ed->Height=250;
$ed->Width='90%';
$ed->ToolbarSet = 'Basic';
$xyService = new XueYuanService();
$xyList = $xyService->getAllList();
$fun  = new fun();
$fun->closeDB();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加留言</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
input{
	color:#3CC;
	width:200px;
	height:20px;
	}
-->
</style>
<script type="text/javascript"  src="js/comment.js"></script>
<script type="text/javascript"  src="js/ajax.js" charset="utf-8"></script>
<script type="text/javascript">
function submitForm()
{
	if(document.getElementById("lyTitle").value=="")
	{
		alert("留言主题不能为空！");
		document.getElementById("lyTitle").focus();
		}
		else if(document.getElementById('lsId')=='none')
		{
			alert("请选择留言的老师！");
			}
		else
		{
			document.getElementById("lyForm").submit();
		}
}
			var xmlHttp;
	function setXmlHttpRequest()
{
	if(window.ActiveXObject)
	{
		xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
		}
		else if(window.XMLHttpRequest)
		{
			xmlHttp = new XMLHttpRequest();
			}
	}
	
	function ajaxFun(url,funName)
	{
		setXmlHttpRequest();
		url = encodeURI(url);
		xmlHttp.open("GET",url,true);
		xmlHttp.onreadystatechange = funName;
		xmlHttp.send(null);
		}
			function getLs()
			{
				var xyId = document.getElementById('xyId').value;
				if(xyId!="none")
					{
				var url = "admin_core/controlXs.php?action=getLs&xyId="+xyId;
				ajaxFun(url,setLs);
			}	
			}
				function setLs()
				{
				if(xmlHttp.readyState == 4)
				{
					if(xmlHttp.status == 200)
					{
						document.getElementById('lsId').innerHTML=xmlHttp.responseText;;
						
						}
					}		
					}
</script>
</head>

<body style="text-align:center;">
<center>

<fieldset  style=" width:1110px;">
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">添加留言</span></legend>
<form action="admin_core/controlXs.php?action=addLeaveword&type=select" method="post" id="lyForm"> 
<table width="100%">
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">选择老师：</td>
<td align="left" width="88%">
  <select name="xyId" id="xyId" style="width:150px; height:20px; color:#09C; border:#999 1px solid;" onchange="getLs()">
    <option value="none">设定搜索的学院/部门</option>
<?php 
for($i=0; $i<count($xyList); $i++)
{
?>
    <option value="<?php echo $xyList[$i]->getXy_id();?>"><?php echo $xyList[$i]->getXy_name();?></option>
<?php 
}
?>
  </select>
<font color="#000000" style="font-weight:bolder">--></font>
<span id="lsSelect">
<select name="lsId" id="lsId" style="width:140px; height:20px; color:#09C; border:#999 1px solid;">
  <option value="none">设定搜索的老师</option>
</select>
</span>

</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">留言主题：</td>
<td align="left" width="88%">
<input name="lyTitle" id="lyTitle" type="text" onfocus="lightBorder(this)"/>

</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">留言内容：</td>
<td align="left" width="88%">
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">

</td>
<td align="left">
<input type="button" value=" 留 言 "   id="submitid"  onclick="submitForm()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
</tr>
</table>

</form>
</fieldset>

</center>
</body>
</html>
