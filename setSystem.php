<?php 
include_once'admin_core/services/SystemService.php';
include_once 'admin_core/utils/Function.php';
$systemService = new SystemService();
$system = $systemService->getSystem();
$fun = new fun();
$fun->closeDB();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
input {
	color:#3CC;
	width:200px;
	height:20px;
	}
textarea{
	color:#3CC;
	width:400px;
	height:20px;
	}
-->
</style>
<script src="js/comment.js" type="text/javascript"></script>
<script type="text/javascript">
function submitForm()
{
	var siteName = document.getElementById('siteName').value;
	var siteDescript = document.getElementById('siteDescript').value;
	var siteEmail = document.getElementById('siteEmail').value;
	var siteEmailPassword = document.getElementById('siteEmailPassword').value;
	var pageSize = document.getElementById('pageSize').value;
	if(siteName == "")
	{
		alert("网站名不能为空!");
		document.getElementById('siteName').focus();
		}
		else if(siteDescript == "")
	{
		alert("网站概述不能为空！");
		document.getElementById('siteDescript').focus();
		}
			else if(!checkEmail(siteEmail))
			{
				alert("邮箱格式不正确！");
				document.getElementById('siteEmail').focus();
				}
				else if(siteEmailPassword == "")
				{
					alert("邮箱密码不能为空！");
					document.getElementById('siteEmailPassword').focus();
					}
					else if(pageSize.search(/[0-9]/)==-1)
					{
						alert("每页显示最多条数必须为数字！");
						document.getElementById('pageSize').focus();
						}
						else
						{
							document.getElementById('systemForm').submit();
							}
	}
	
</script>
</head>

<body style="text-align:center;">
<center>
<div style="height:50px;">
</div>
<div style="width:100%; height:50px;">

<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">系统属性设置</span></legend>
<form id="systemForm" action="admin_core/control.php?action=updateSystem" method="post">
<table width="80%">
<tr>
<td width="30%" height="22"  style="text-align:right; font-size:14px; color:#09C;">网站名：</td>
<td align="left">
<input name="siteName" id="siteName" value="<?php echo $system->getSiteName();?>"  onfocus="lightBorder(this)"/>
<input name="id" type="hidden" value="<?php echo $system->getId(); ?>"></input>
</td>
</tr>
<tr>
<td width="30%" height="22"  style="text-align:right; font-size:14px; color:#09C;">网站地址：</td>
<td align="left"><input name="siteUrl" id="siteUrl" value="<?php echo $system->getSiteUrl();?>"  onfocus="lightBorder(this)"/></td>
</tr>
<tr>
<td width="30%"  style="text-align:right; font-size:14px; color:#09C;">网站概述：</td>
<td align="left"><textarea name="siteDescript" id="siteDescript" onfocus="lightBorder(this)" style="height:100px;">
<?php echo $system->getSiteDescpribt();?>
</textarea></td>
</tr>
<tr>
<td width="30%"  style="text-align:right; font-size:14px; color:#09C;">站长邮箱：</td>
<td align="left">
<input name="siteEmail" id="siteEmail" value="<?php echo $system->getSiteEmail();?>"  onfocus="lightBorder(this)"/>
</td>
</tr>
<tr>
<td width="30%"  style="text-align:right; font-size:14px; color:#09C;">站长邮箱密码：</td>
<td align="left">
<input name="siteEmailPassword" id="siteEmailPassword" value="<?php echo $system->getSiteEmailPassword();?>"  onfocus="lightBorder(this)"/>
</td>
</tr>
<tr>
<td width="30%"  style="text-align:right; font-size:14px; color:#09C;">每页显示最多条数：</td>
<td align="left">
<input name="pageSize" id="pageSize" value="<?php echo $system->getPageSize();?>"  onfocus="lightBorder(this)"/>
</td>
</tr>
<tr>
<td width="30%" style="text-align:right; font-size:14px; color:#09C;">

</td>
<td align="left">
<input type="button" value=" 修 改 "   id="submitid"  onclick="submitForm()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
</tr>
</table>
</form>
</fieldset>
</div>
</center>
</body>
</html>
