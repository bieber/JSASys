<?php 
session_start();
include_once 'fckeditor/fckeditor.php';
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/services/WenTiService.php';
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/models/ZuoYe.php';
include_once 'admin_core/services/ZuoYeService.php';
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('content');
$ed->BasePath=$sBasePath;
$ed->Height=300;
$ed->Width='100%';
$hwId =$_GET['hwId'];
$wtService = new WenTiService();
$wt=$wtService->getWenTiById($hwId);
$xs = unserialize($_SESSION['user']);
$zyService = new ZuoYeService();
$zy = $zyService->getZuoYeByXSORWT($xs->getXs_id(),$hwId);
$fun = new fun();
$fun->closeDB();
$now = mktime();
$start = strtotime($wt->getStart_time());
$end = strtotime($wt->getEnd_time());
$size = 1024*1024*1024*30;
if($zy==null)
{
if(!$_GET['type'])
{if($now<$start)
{
	$fun->alertMessage("还未到开始提交时间！请稍后...", "getPositionHW.php");
}
else if($now>$end)
{
	$fun->alertMessage("该作业已到截至提交时间！已不允许提交！", "getPositionHW.php");
}
}
else
{if($now<$start)
{
	$fun->alertMessage("还未到开始提交时间！请稍后...", "admin_core/controlXs.php?action=searchHw");
}
else if($now>$end)
{
	$fun->alertMessage("该作业已到截至提交时间！已不允许提交！", "admin_core/controlXs.php?action=searchHw");
}
}
}
else if($zy[0]->getZy_sate()==0||$zy[0]->getZy_sate()==1)
{
	$fun->alertMessage("你已经提交该作业！无需重复提交！", "admin_core/controlXs.php?action=searchHw");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提交作业</title>
<style type="text/css">
img{
	border:0px;}
</style>
 <link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="uploadify/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"  src="js/comment.js"></script>
<script type="text/javascript" >
function checkForm()
{
	if(document.getElementById('zyName').value=="")
	{
		alert("请输入作业名！");
		document.getElementById('zyName').focus();
		}
		else
		{
			document.getElementById('zyForm').submit();
			}
	}
</script>
</head>

<body>
<center>
<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold; ">提交作业</span></legend>
<form  action="admin_core/controlXs.php?action=submitHW" id="zyForm" method="post" enctype="multipart/form-data">
<table width="90%">
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业名：</td>
<td align="left" style="font-size:14px; color:#09C;">
<input  name='zyName' id="zyName" type="text" style="width:150px; height:20px; border:1px #999 solid;"/>
<input type="hidden" name="hwId" value="<?php echo $wt->getWt_id();?>" /> 
<?php 
if($_GET['type'])
{
?>
<input type="hidden" name="type" value="<?php echo $_GET['type'];?>"></input>
<?php 
}
?>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业提交时间：</td>
<td align="left" style="font-size:14px; color:#09C;"><?php echo $wt->getStart_time();?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">截至提交时间：</td>
<td align="left" style="font-size:14px; color:#09C;"><?php echo $wt->getEnd_time();?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业类型：</td>
<td align="left" style="font-size:14px; color:#09C;"><?php 
if($wt->getWt_type()==0)
{
	echo "选做";
}
else
{
	echo "必做";
}
?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业描述：</td>
<td align="left" style="font-size:14px; color:#09C;"><?php 
echo $wt->getWt_describe();
?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业附件：</td>
<td align="left" style="font-size:14px; color:#09C;"> <input id="attachFile" name="attachFile" type="file" style="width:250px; height:20px; border:1px #999 solid;" />
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业内容：</td>
<td align="left" style="font-size:14px; color:#09C;">
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;"></td>
<td align="left" style="font-size:14px; color:#09C;"><input type="button" value=" 提 交 " onclick="checkForm()"   id="submitid"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" /></td>
</tr>
</table>
</form>
</fieldset>
</center>
</body>
</html>
