<?php 
session_start();
include_once 'fckeditor/fckeditor.php';
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/services/WenTiService.php';
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/models/ZuoYe.php';
include_once 'admin_core/services/ZuoYeService.php';
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('py');
$ed->BasePath=$sBasePath;
$ed->Height=300;
$ed->Width='100%';
$zyId =$_GET['zyId'];
$zyService = new ZuoYeService();
$zy = $zyService->getZuoYeById($zyId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提交作业</title>
<style type="text/css">
img{
	border:0px;}
</style>
 <link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="uploadify/jquery-1.4.2.min.js"></script>
<script type="text/javascript"  src="js/comment.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<style type="text/css">
table
{
	border:1px #06C solid;
	border-collapse:collapse;
	}
	td{
		border:1px #06C solid;
		}
</style>
<script type="text/javascript"><!--
function checkSh()
{
	var score = document.getElementById('score').value;
	if(score=="")
	{
		alert("请输入分数！");
		document.getElementById('score').focus();
		}
		else{
			document.getElementById("zyForm").submit();
			}
	}
	function displayPy(obj)
	{
		if(document.getElementById('pyState').value==0)
		{
			JqueryShow($("#py"));
			document.getElementById('pyState').value=1;
			obj.innerHTML="<img src=\"images/notepad_(delete)_16x16.gif\" width=\"14\" height=\"14\" />取消添加";
			}
			else
			{
				Jqueryhidden($("#py"));
				document.getElementById('pyState').value=0;
			    obj.innerHTML="<img src=\"images/notepad_(add)_16x16.gif\" width=\"14\" height=\"14\" />添加评语";
				}
		}
		function JqueryShow(nodeObj)
		{
			nodeObj.show();
			}
			function Jqueryhidden(nodeObj)
		{
			nodeObj.hide();
			}
		function reDo()
		{
			var zyId = document.getElementById('zyId').value;
			document.location.href="admin_core/controlLs.php?action=reDoHW&zyId="+zyId;
			}
	

</script>
</head>

<body>
<center>
<fieldset style="width:1110px;">
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold; ">批改作业</span></legend>
<div style="width:99% ; height:20px; border-top:2px dashed #666; border-bottom:2px dashed #666; font-size:16px; color:#999;font-weight:bold; margin-top:5px; margin-bottom:5px; line-height:20px;">
作业内容
</div>

<table width="90%">
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业名：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;">
<?php echo $zy->getZy_name();?>

</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业类型：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php 
if($zy->getWt()->getWt_type()==0)
{
	echo "选做";
}
else
{
	echo "必做";
}
?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业描述：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php 
echo $zy->getWt()->getWt_describe();
?></td>
</tr>
<?php 
$fileName = $zy->getZy_file();

$fileName = explode(".", $fileName);
$name = $fileName[count($fileName)-1];
?>
<?php 
if($zy->getZy_file()!='none')
{
?>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C; ">文件格式为：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php echo $name;?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业附件：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"> 

<a href="studentHomework/<?php echo $zy->getZy_file();?>" style="text-decoration: none;" target="_blank"><img src="fileimages/<?php echo $name.".png";?>" alt="附件"  width="80" height="80"/>
【<img src="images/last_16x16.gif" width="14" height="14" />下载】</a>
</td>
</tr>
<?php 
}
?>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业内容：</td>
<td align="left" style="font-size:14px; color:#09C;">
<div style="padding:5px; height:200px; width:700px;overflow:auto; ">
<?php 
echo $zy->getZy_content();
?>
</div>
</td>
</tr>

</table>
<div style="width:99% ; height:20px; border-top:2px dashed #666; border-bottom:2px dashed #666; font-size:16px; color:#999;font-weight:bold; margin-top:5px; margin-bottom:5px; line-height:20px;">
批改内容
</div>
<form action="admin_core/controlLs.php?action=confirmZY" id="zyForm" method="post">
<table width="90%" style="border:0px;">

<tr>
<td width="7%" style="text-align:right; font-size:14px; color:#09C; border:0px;">请打分：</td>
<td align="left" style="font-size:14px; color:#09C; border:0px;">
<input name="score" id="score" style="width:150px; height:20px; border:1px #999 solid;" type="text" />&nbsp;分
<input type="hidden" name="zyId" id="zyId" value="<?php echo $zy->getZy_id();?>" /> 
<input type="hidden" name="pyState" value="0" id="pyState" />
<span style="font-size:13px; color:#9C0; font-weight:bold; cursor:pointer;" onclick="displayPy(this)"> <img src="images/notepad_(add)_16x16.gif" width="14" height="14" />添加评语</span>
</td>
</tr>

<tr id="py" style="display:none;">
<td width="7%" style="text-align:right; font-size:14px; color:#09C; border:0px;">评语：</td>
<td align="left" style="font-size:14px; color:#09C; border:0px;">
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
<td width="7%" style="text-align:right; font-size:14px; color:#09C; border:0px;"></td>
<td align="left" style="font-size:14px; color:#09C; border:0px;"><input type="button" value=" 通 过  "    id="submitid"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" onclick="checkSh()" />
<input type="button" value=" 重 做  " onclick="reDo()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" /></td>
</tr>
</table>
</form>
</fieldset>
</center>
</body>
</html>
