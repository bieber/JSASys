<?php
include_once 'admin_core/utils/Function.php';
include_once 'fckeditor/fckeditor.php';
include_once 'admin_core/services/XueYuanService.php';
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('descript');
$ed->BasePath=$sBasePath;
$ed->Height=300;
$ed->Width='100%';
$xsService = new XueYuanService();
$xsList = $xsService->getAllList();
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">

body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
input{
	color:#3CC;
	width:200px;
	height:20px;
	}
</style>
<script type="text/javascript"  src="js/comment.js"></script>
<script type="text/javascript" src="date/WdatePicker.js"></script>
<script type="text/javascript">
var xmlHttp;
	function setXmlHttpRequest()
{
	if(window.ActiveXObject)
	{
		xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
		}
		else if(window.XMLHttpRequest)
		{
			xmlHttp = new XMLHttpRequest();
			}
	}
	
	function ajaxFun(url,funName)
	{
		setXmlHttpRequest();
		url = encodeURI(url);
		xmlHttp.open("GET",url,true);
		xmlHttp.onreadystatechange = funName;
		xmlHttp.send(null);
		}
		//获得所选择学院的专业
function getZhy(obj)
{
	//alert(obj.value.length);
	var selected="";
	for(i=0; i<obj.options.length; i++)
	{
		if(obj.options[i].selected==true)
		{
			selected=selected+obj.options[i].value+"//";
			}
		}
		document.getElementById('bj').style.display="none";
		var url = "admin_core/controlLs.php?action=getZhy&xyList="+selected;
		ajaxFun(url,setZhy);
		
	}
	function setZhy()
	{
		document.getElementById('zhy').style.display='block';
	if(xmlHttp.readyState == 4)
				{
					if(xmlHttp.status == 200)
					{
						document.getElementById('zhy').innerHTML=xmlHttp.responseText;;
						
						}
					}	
		}
		//end
		
		//获得所选择专业的班级
	function getBJ(obj)
	{
			var selected="";
	for(i=0; i<obj.options.length; i++)
	{
		if(obj.options[i].selected==true)
		{
			selected=selected+obj.options[i].value+"//";
			}
		}
		var url = "admin_core/controlLs.php?action=getBj&zhyList="+selected;
		ajaxFun(url,setBj);
		
		}
		function setBj()
		{
			document.getElementById('bj').style.display='block';
			if(xmlHttp.readyState == 4)
				{
					if(xmlHttp.status == 200)
					{
						document.getElementById('bj').innerHTML=xmlHttp.responseText;;
						
						}
					}	
			}
			//end
			function submitForm()
			{
				var hwName = document.getElementById('hwName').value;
				var start = document.getElementById('start').value;
				var end = document.getElementById('end').value;
				var type = document.getElementById('hwType').value;
			
			if(hwName=="")
			{
				alert("请输入作业名！");
				 document.getElementById('hwName').focus();
				}
				else if(start=="")
				{
					alert("请选择作业开始提交时间！");
					document.getElementById('start').focus();
					}
					else if(end=="")
					{
						alert("请选择作业截至提交时间！");
						 document.getElementById('end').focus();
						}
						else if(type==1&&(null == document.getElementById('bj[]')||document.getElementById('bj[]').value==""))
						{
							alert("请选择做该作业的班级！");
							}
							
							else
							{
								document.getElementById('addHW').submit();
								}
			
				}
	
</script>
</head>

<body style="text-align:center;">
<center>
<fieldset style="width:1110px;">
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold; ">发布作业</span></legend>
<form action="admin_core/controlLs.php?action=addHW" method="post" id="addHW">
<table width="100%">
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业名：</td>
<td align="left">
<input name="hwName" id="hwName" type="text" style="border:1px #999 solid;" />
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">开始提交时间：</td>
<td align="left">
<input name="start" id="start" type="text" onfocus="var after=$dp.$('end');WdatePicker({onpicked:function(){after.focus();},maxDate:'#F{$dp.$D(\'end\')}'})" class="Wdate" /><span style="font-size:13px; color:#F00;">（注意：当已到提交时间后不可修改提交作业的班级！）</span>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">截至提交时间：</td>
<td align="left">
<input name="end" id="end" type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'start\')}'}) " class="Wdate" />
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业类型：</td>
<td align="left">
<select name="hwType" id="hwType" style="font-size:13px; color:#09C;">
<option value="1"> 必做 </option>
<option value="0"> 选做 </option>
</select>
<font color="#FF0000" style=" font-size:12px;">&nbsp;&nbsp;(&lsquo;选做&rsquo;可以不选择班级)</font>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">选择提交作业的班级：</td>
<td align="left" >
<div style="color:#09C; font-size:13px; width:auto; height:80px;border:1px #999 solid; float:left; margin-left:3px;">
&nbsp;选择学院：
<div style="clear:both; ">
</div>
<select name="select" size="4" multiple="multiple" style="border:0px; margin:0px; padding:0px; color:#666;"  onchange="getZhy(this)" >
  <?php 
for($i=0; $i<count($xsList); $i++)
{
?>
  <option value="<?php echo $xsList[$i]->getXy_id();?>"><?php echo $xsList[$i]->getXy_name();?></option>
  <?php 
}
?>
</select>
</div>
<div id="zhy" style="color:#09C; font-size:13px; width:auto; height:80px;border:1px #999 solid; float:left; margin-left:3px; display:none;">
<br />
<br />
载入中....
</div>
<div id="bj"  style="color:#09C; font-size:13px; width:auto; height:80px;border:1px #999 solid; float:left; margin-left:3px; display:none;">
<br />
<br />
载入中...
</div>
<div style="font-size:12px; color:#F00; float:left; height:80px; line-height:140px;">

(按住【ctrl】可以多选)
</div>
</div>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业描述：</td>
<td align="left">
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;"></td>
<td align="left">
<input type="button" value=" 发 布 "   id="submitid"  onclick="submitForm()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; " onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
</tr>
</table>
</form>
</fieldset>
</center>
</body>
</html>
