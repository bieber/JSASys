<?php
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
/**
 * 
 * Enter description 该类是对学院对象各种操作的控制
 * @author hebibo
 *
 */
class XueYuanControl {
   /**
    * 
    * Enter description 添加学院的操作
    * @param fun $fun
    */
	public function addXY(fun $fun) {
		$xyName = $_POST ['xyName'];
		$xyDescribt = $_POST ['xyDescribt'];
		$xy = new XueYuan ();
		$xy->setXy_name ( $xyName );
		$xy->setXy_describe ( $xyDescribt );
		$xyService = new XueYuanService ();
		if ($xyService->addXueYuan ( $xy )) {
			$fun->addLog("添加学院<".$xy->getXy_name().">");
			$fun->closeDB ();
			$fun->alertMessage ( "学院添加成功！", "control.php?action=xyList" );
		} else {
			$fun->addLog("添加学院<".$xy->getXy_name().">失败");
			$fun->closeDB ();
			$fun->alertMessage ( "学院添加失败！", "control.php?action=xyList" );
		}
	
	}
	/**
	 * 
	 * Enter description 检测学院重名的操作
	 * @param fun $fun
	 */
	public function checkXY(fun $fun) {
		header ( "content-type:text/html; charset=utf-8" );
		
		$xyName = $_GET ['xyName'];
		$oldName = $_GET ['oldName'];
		$xyService = new XueYuanService ();
		$xyName = iconv ( "utf-8", "gbk", $xyName );
		$oldName = iconv ( "utf-8", "gbk", $oldName );
		//$fun->alertMessage($xyName."  ".$oldName, "/");
		if ($oldName == "") {
			if ($xyService->checkXYByName ( $xyName )) {
				$fun->closeDB ();
				return true;
			} else {
				$fun->closeDB ();
				return false;
			}
		} else {
			if ($xyName == $oldName) {
				return false;
			} else {
				if ($xyService->checkXYByName ($xyName)) {
					$fun->closeDB ();
					return true;
				} else {
					$fun->closeDB ();
					return false;
				}
			}
		}
	
	}
	
	/**
	 * 
	 * Enter description 获取学院列表的操作
	 * @param fun $fun
	 * @param Smarty $smarty
	 */
	public function xyList(fun $fun, Smarty $smarty) {
		$xyService = new XueYuanService ();
		$fun->listPage($xyService, $smarty);
		$fun->closeDB ();
		$smarty->display ( "admin/xyList.html" );
	}
	/**
	 * 
	 * Enter description 删除学院的操作
	 * @param fun $fun
	 */
	public function deleteXY(fun $fun) {
		$xyService = new XueYuanService ();
		if ($xyService->deleteXueYuanById ( $_GET ['xyId'] )) {
			$fun->addLog("删除了一个学院");
			$fun->closeDB ();
			$fun->alertMessage ( "删除成功！", "control.php?action=xyList" );
		} else {
			$fun->addLog("删除了一个学院失败");
			$fun->closeDB ();
			$fun->alertMessage ( "删除失败！", "control.php?action=xyList" );
		}
	}
	/**
	 * 
	 * Enter description 修改学院的操作
	 * @param fun $fun
	 */
	public function  modifyXY(fun $fun)
	{
		$xyService = new XueYuanService ();
		$xyId = $_POST['xyId'];
		$xy = $xyService->getXueYuanById($xyId);
		$xyName = $_POST['xyName'];
		$xyDescribt = $_POST['xyDescribt'];
		$xy->setXy_name($xyName);
		$xy->setXy_describe($xyDescribt);
		if($xyService->updateXueYuan($xy))
		{
			$fun->addLog("更新了学院<".$xy->getXy_name().">信息");
			$fun->closeDB ();
			$fun->alertMessage ( "更新成功！", "control.php?action=xyList" );
		}
	else {
			$fun->addLog("更新了学院<".$xy->getXy_name().">信息失败");
			$fun->closeDB ();
			$fun->alertMessage ( "更新失败！", "control.php?action=xyList" );
		}
	}
}

?>