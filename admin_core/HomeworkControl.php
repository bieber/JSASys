<?php

class HomeworkControl{
	
	public function addHW(fun $fun)
	{
		$hwName = $_POST['hwName'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$hwType = $_POST['hwType'];
		$bj = $_POST['bj'];
		$descript = $_POST['descript'];
		$wt = new WenTi();
		$wt->setWt_name($hwName);
		$wt->setWt_type($hwType);
		$wt->setWt_describe($descript);
		$bjService = new BanJiService();
		for($i=0; $i<count($bj); $i++)
		{
			$bjList[] = $bjService->getBanJiById($bj[$i]);
		}
		$wt->setBjList($bjList);
		$wt->setStart_time(strtotime($start));
	    $wt->setEnd_time(strtotime($end));
	    $ls = unserialize($_SESSION['user']);
	    $wt->setLs($ls);
	    $wt->setXy($ls->getXy());
	    $wtService = new WenTiService();
	    if($wtService->addWenTi($wt))
	    {
	    	$fun->addLog("发布了作业<".$wt->getWt_name().">");
	    	$fun->closeDB();
	    	$fun->alertMessage("发布成功！", "controlLs.php?action=searchHW");
	    }
	    else
	    {
	    	$fun->addLog("发布了作业<".$wt->getWt_name().">失败");
	    	$fun->closeDB();
	    	$fun->alertMessage("发布失败！", "../addHomework.php");
	    }
	}
    public function hwList(Smarty $smarty,fun $fun)
    {
    	$wtService = new WenTiService();
    	$ls = unserialize($_SESSION['user']);
    	$fun->listPageById($wtService, $smarty,$ls->getLs_id());
    	$fun->closeDB();
    	$smarty->display("teacher/hwList.html");
    }
    public function deleteHW(fun $fun)
    {
    	$wtService = new WenTiService();
    	$wtList = $_POST['hwId'];
    	if($wtService->deleteWenTiBatch($wtList))
    	{
    		$fun->addLog("删除了作业");
	    	$fun->closeDB();
	    	$fun->alertMessage("删除成功！", "controlLs.php?action=hwList");
    	}
    	else 
    	{
    		$fun->addLog("删除了作业失败");
	    	$fun->closeDB();
	    	$fun->alertMessage("删除失败！", "controlLs.php?action=hwList");
    	}
    }
    public function searchHW(Smarty $smarty,fun $fun)
    {
    	$wtService = new WenTiService();
    	$ls = unserialize($_SESSION['user']);
    	$before=0;
    	$after=0;
    	$keyword=null;
    	$type = 3;
    	$hwType=3;
    	if($_GET['before'])
    	{
    		$before = strtotime($_GET['before']);
    	}
    	if($_GET['after'])
    	{
    		$after =strtotime($_GET['after']);
    	}
    	if($_GET['keyword'])
    	{
    		$keyword = $_GET['keyword'];
    	}
    	if($_GET['type'])
    	{
    		$type = $_GET['type'];
    	}
    	if($_GET['hwType'])
    	{
    		$hwType = $_GET['hwType'];
    	}
    	
    	$fun->listPageForHW($wtService, $smarty,$before,$after,$keyword,$ls->getLs_id(),$ls->getXy()->getXy_id(),$type-1,$hwType-1);
    	$smarty->assign("keyword",$keyword);
    	$smarty->assign("before",date ( "Y-m-d H:i:s",$before));
    	$smarty->assign("after",date ( "Y-m-d H:i:s",$after));
    	$smarty->assign("type",$type);
    	$smarty->assign("hwType",$hwType);
    	$fun->closeDB();
    	$smarty->display("teacher/hwList.html");
    }
	public function updateHW(fun $fun)
	{
		 $wtService = new WenTiService();
		$hwName = $_POST['hwName'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$hwType = $_POST['hwType'];
		$hwId = $_POST['hwId'];
		$descript = $_POST['descript'];
		$stateBj = $_POST['stateBj'];
		//$wt = new WenTi();
		$wt=$wtService->getWenTiById($hwId);
		$wt->setWt_name($hwName);
		$wt->setWt_type($hwType);
		$wt->setWt_describe($descript);
		$bjService = new BanJiService();
		$state=0;
		if($stateBj!=1)
		{
		if($_POST['bj']&&$_POST['bj']!='none')
		{
			$state=1;
			$bj = $_POST['bj'];
           
		for($i=0; $i<count($bj); $i++)
		{
			$bjList[] = $bjService->getBanJiById($bj[$i]);
		}
		$wt->setBjList($bjList);
		}
		}
		else {
			
			$wt->setBjList(null);
		}
		$wt->setStart_time(strtotime($start));
	    $wt->setEnd_time(strtotime($end));
	    $ls = unserialize($_SESSION['user']);
	    $wt->setLs($ls);
	    $wt->setXy($ls->getXy());
	    if($wtService->updateWenTi($wt,$state))
	    {
	    	$fun->addLog("修改了作业<".$wt->getWt_name().">");
	    	$fun->closeDB();
	    	$fun->alertMessage("修改成功！", "controlLs.php?action=searchHW");
	    }
	    else
	    {
	    	$fun->addLog("修改了作业<".$wt->getWt_name().">失败");
	    	$fun->closeDB();
	    	$fun->alertMessage("修改失败！", "controlLs.php?action=searchHW");
	    }
	}
}
?>