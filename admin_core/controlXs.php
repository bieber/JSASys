<?php

session_start ();
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
include_once ROOT . '/utils/ChineseSpell .php';
include_once ROOT . '/utils/email.class.php';
include_once ROOT . '/models/XueYuan.php';
include_once ROOT . '/models/BanJi.php';
include_once ROOT . '/models/System.php';
include_once ROOT . '/models/Log.php';
include_once ROOT . '/models/LaoShi.php';
include_once ROOT . '/models/XueSheng.php';
include_once ROOT . '/models/News.php';
include_once ROOT.'/models/LiuYan.php';
include_once ROOT . '/services/XueYuanService.php';
include_once ROOT . '/services/ZhuanYeService.php';
include_once ROOT . '/services/BanJiService.php';
include_once ROOT . '/services/SystemService.php';
include_once ROOT . '/services/LogService.php';
include_once ROOT . '/services/LaoShiService.php';
include_once ROOT . '/services/XueShengService.php';
include_once ROOT . '/services/NewsService.php';
include_once ROOT.'/models/WenTi.php';
include_once ROOT.'/models/WT2XSRelation.php';
include_once ROOT.'/models/ZuoYe.php';
include_once ROOT.'/services/WenTiService.php';
include_once ROOT.'/services/WT2XSRelationService.php';
include_once ROOT.'/services/ZuoYeService.php';
include_once ROOT.'/services/LiuYanService.php';
include_once 'smarty_inc.php';
include_once 'StudentControl.php';
$action = $_GET ["action"];
$studentControl = new StudentControl ();
$fun = new fun ();
if ($action != "login") {
	$fun->checkLogin ();
}
 if($action == "updateXs")
{
	$studentControl->updateXS($fun);
}
else if($action == "updateXsInfo")
{
$studentControl->updateXSInfo($fun);
	
}
else if($action=='submitHW')
{
	$studentControl->submitHW($fun);
}
else if($action=='searchHw')
{
	$studentControl->searchHw($smarty, $fun);
}
else if($action=="getLs")
{
	$xyId = $_GET['xyId'];
	$lsService = new LaoShiService();
	$lsService->setAction("validatedTeachers");
	$lsList = $lsService->getAllListById($xyId);
	if($_GET['type'])
	{
	$content = "<select onchange=\"getWt()\" name=\"lsId\" id=\"lsId\" style=\"width:140px; height:20px; color:#09C; border:#999 1px solid;\">
<option value=\"none\">设定搜索的老师</option>";
	}
	else
	{
		$content = "<select name=\"lsId\" id=\"lsId\" style=\"width:140px; height:20px; color:#09C; border:#999 1px solid;\">
<option value=\"none\">设定搜索的老师</option>";
	}
	if(count($lsList)>0)
	{
	for($i=0; $i<count($lsList); $i++)
	{
		$content = $content."<option value=".$lsList[$i]->getLs_id().">".$lsList[$i]->getLs_name()."</option>";
	}
	}
	else
	{
		$content = $content."<option value='none'>该学院/部门还未有老师注册</option>";
	}
	$content=$content."</select>";
	echo iconv ( "gbk", "utf-8", $content);
}
else if($action == 'getWt')
{
	$lsId = $_GET['lsId'];
	$lsService = new LaoShiService();
	$ls = $lsService->getLaoShiById($lsId);
	$wtService = new WenTiService();
	$wtList = $wtService->searchWenTi(0,0,null,$ls->getLs_id(),$ls->getXy()->getXy_id(),0,2);
	$content = "<select name=\"wtId\" id=\"wtId\"  style=\"width:140px; height:20px; color:#09C; border:#999 1px solid; \">
<option value=\"none\">设定搜索的作业</option>";
	if(count($wtList)>0)
	{
	for($i=0;$i<count($wtList); $i++)
	{
		$content = $content."<option value=".$wtList[$i]->getWt_id().">".$wtList[$i]->getWt_name()."</option>";
	}
	}else
	{
		$content = "<option value=\"none\">该老师还未发布作业</option>";
	}
	echo iconv ( "gbk", "utf-8", $content);
}
else if($action == 'getUnread' || $action == 'getPassed' || $action == 'getUnPassed')
{
	$state = 0;
	if($action == 'getUnread')
	{
		$state = 1;
	}
	else if($action == 'getPassed')
	{
		$state = 2;
	}
	else
	{
		$state = 3;
	}
	$smarty->assign("action",$action);
	$studentControl->getHW($smarty, $fun, $state);
}
else if($action == 'addLeaveword')
{
	$title = $_POST['lyTitle'];
	$lsId = $_POST['lsId'];
	$content = $_POST['lyContent'];
	$ly = new LiuYan();
	$xs = unserialize($_SESSION['user']);
	$lsService = new LaoShiService();
	$ls = $lsService->getLaoShiById($lsId);
	$ly->setLs($ls);
	$ly->setLy_content($content);
	$ly->setLy_date(mktime());
	$ly->setLy_hf(0);
	$ly->setLy_title($title);
    $ly->setXs($xs);
	$lyService = new LiuYanServie();
	$system = unserialize($_SESSION['system']);
	$emailContent = $ls->getLs_name()."老师：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;在" . $system->getSiteName () . "！有一名学生给你留言！请注意查收！点击下面地址进入系统，
				如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
    
	if($lyService->addLiuYan($ly))
	{
		$fun->sendMail($ls->getLs_email(), $emailContent, "留言通知",$system->getSiteEmail (), $system->getSiteEmailPassword ());
		$fun->closeDB();
		if(!$_GET['type'])
		{
			$fun->alertMessage("留言成功！", "../addLeaveWord.php");
		}
		else
		{
			$fun->alertMessage("留言成功！", "controlXs.php?action=getUnRecive");
		}
		
	}
	else
	{
		$fun->closeDB();
	   if(!$_GET['type'])
		{
			$fun->alertMessage("留言失败！", "../addLeaveWord.php");
		}
		else
		{
			$fun->alertMessage("留言失败！", "controlXs.php?action=getUnRecive");
		}
	}
	
}
else if($action == 'getUnRecive')
{
	$smarty->assign("action",$action);
  $studentControl->getLeaveWordList($smarty, $fun, 0);
}
else if($action =='getRecived')
{
		$smarty->assign("action",$action);
  $studentControl->getLeaveWordList($smarty, $fun, 1);
}
else if($action=='deletLy')
{
	$lyId= $_POST['lyId'];
	$lyService = new LiuYanServie();
	if($lyService->deleteLYBatch($lyId))
	{
		$fun->closeDB();
		$fun->alertMessage("删除成功！","controlXs.php?action=getRecived");
	}
	else 
	{
		$fun->closeDB();
		$fun->alertMessage("删除失败！","controlXs.php?action=getRecived");
	}
}
?>