<?php
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
/**
 * 
 * Enter description 该类是对管理员各种操作的控制
 * @author hebibo
 *
 */
class AdminControl {
	/**
	 * 
	 * Enter description 该类是管理员登录的操作
	 * @param fun $fun
	 */
	function login(fun $fun) {
		if (isset ( $_SESSION ['userId'] )) {
			$fun->alertMessage ( "登录失败！请确认你登录的其他账号已退出！或者重新打开浏览器！", "../index.php" );
		} else {
			
			$user_name = $_POST ["username"];
			$user_psw = $_POST ["password"];
			$validate = $_POST ["chknumber"];
			$user_name = addslashes ( $user_name );
			$user_psw = addslashes ( $user_psw );
			$admin = new Admin ();
			$admin->setAdmin_name ( $user_name );
			//echo $user_name;
			$admin->setAdmin_password ( $user_psw );
			$adminService = new AdminService ();
			$chineseSpell = new ChineseSpell ();
			if ($validate != $_SESSION ["validate"] && $validate != $chineseSpell->getFullSpell ( $_SESSION ['validate'] )) {
				$fun->closeDB ();
				$fun->alertMessage ( "你输入的验证码错误！请重新出入...", "../index.php" );
				return;
			} else if ($adminService->checkAdmin ( $admin )) {
				$admin = $adminService->checkAdmin ( $admin );
				$adminService->updateAdmin ( $admin );
				$_SESSION ["user"] = serialize ( $admin );
				$userId = strtotime ( date ( "Y-m-d H:i:s" ) );
				$_SESSION ['userId'] = $userId;
				$_SESSION ['time'] = mktime ();
				$fun->addLog ( "登录成功" );
				$fun->closeDB ();
				$fun->alertMessage ( "欢迎" . $admin->getAdmin_name () . "使用该系统！", "../main.php" );
				return;
			} else {
				$fun->addLog ( "登录失败" );
				$fun->closeDB ();
				$fun->alertMessage ( "输入的管理员名或密码错误！请重新输入...", "../index.php" );
			}
		}
	}
	/**
	 * 
	 * Enter description 该类是查看当前登录的管理员信息操作.
	 * @param unknown_type $smarty
	 * @param fun $fun
	 */
	function viewAdmin($smarty, fun $fun) {
		
		$admin = unserialize ( $_SESSION ["user"] );
		
		$smarty->assign ( "userName", $admin->getAdmin_name () );
		$smarty->assign ( "email", $admin->getAdmin_email () );
		$smarty->assign ( "adminId", $admin->getAdmin_id () );
		$smarty->display ( "admin/modifyAdmin.html" );
	}
	/**
	 * 
	 * Enter description 更新管理员信息操作
	 * @param unknown_type $fun
	 */
	function updateAdmin($fun) {
		
		$adminId = $_POST ["adminId"];
		$adminService = new AdminService ();
		$admin = $adminService->getAdminById ( $adminId );
		$userName = $_POST ["userName"];
		$email = $_POST ["email"];
		$password = $_POST ["password"];
		$password = $fun->StringToMD5 ( $password );
		$admin->setAdmin_email ( $email );
		$admin->setAdmin_name ( $userName );
		$admin->setAdmin_password ( $password );
		if ($adminService->updateAdmin ( $admin )) {
			$fun->addLog ( "更改<" . $admin->getAdmin_name () . ">了的信息成功" );
			$fun->closeDB ();
			$fun->alertMessage ( "信息修改成功！", "control.php?action=adminList" );
			$_SESSION ["user"] = serialize ( $admin );
		} else {
			$fun->addLog ( "更改<" . $admin->getAdmin_name () . ">了的信息失败" );
			$fun->closeDB ();
			$fun->alertMessage ( "修改失败！", "control.php?action=modifyAdmin" );
		}
	
	}
	/**
	 * 
	 * Enter description 获取管理员列表操作
	 * @param unknown_type $smarty
	 * @param unknown_type $fun
	 */
	function adminList(Smarty $smarty, fun $fun) {
		
		$adminService = new AdminService ();
		$admins = $adminService->getAllAdmin ();
		$smarty->assign ( "admins", $admins );
		$admin = unserialize($_SESSION['user']);
		$smarty->assign("user",$admin);
		$fun->closeDB ();
		$smarty->display ( "admin/adminList.html" );
	
	}
	/**
	 * 
	 * Enter description 添加管理员操作
	 * @param unknown_type $fun
	 */
	function addAdmin(fun $fun) {
		$userName = $_POST ["userName"];
		$userPurview = $_POST ["purview"];
		$userPassword = $_POST ["password"];
		$email = $_POST ["email"];
		$admin = new Admin ();
		$admin->setAdmin_name ( $userName );
		$admin->setAdmin_password ( $userPassword );
		$admin->setAdmin_purview ( $userPurview );
		$admin->setAdmin_email ( $email );
		$adminService = new AdminService ();
		$adminService->addAdmin ( $admin );
		$fun->addLog ( "添加用户<" . $admin->getAdmin_name () . ">成功" );
		$fun->closeDB ();
		$fun->alertMessage ( "管理员添加成功！", "control.php?action=adminList" );
	}
	/**
	 * 
	 * Enter description 删除管理员操作
	 * @param fun $fun
	 */
	function deleteAdmin(fun $fun) {
		
		$adminService = new AdminService ();
		if ($adminService->deletAdmin ( $_GET ["adminId"] )) {
			$fun->addLog ( "删除用户" );
			$fun->closeDB ();
			$fun->alertMessage ( "删除成功!", "control.php?action=adminList" );
		} else {
			$fun->addLog ( "删除用户失败" );
			$fun->closeDB ();
			$fun->alertMessage ( "删除失败!", "control.php?action=adminList" );
		}
	
	}
	/**
	 * 
	 * Enter description 更新管理员权限的操作
	 * @param unknown_type $fun
	 */
	function updatePurview(fun $fun) {
		
		$adminId = $_POST ["adminId"];
		$adminPurview = $_POST ["purview"];
		$adminService = new AdminService ();
		$admin = $adminService->getAdminById ( $adminId );
		$admin->setAdmin_purview ( $adminPurview );
		if ($adminService->updateAdmin ( $admin )) {
			$fun->addLog ( "修改了用户<" . $admin->getAdmin_name () . ">的权限" );
			$fun->closeDB ();
			$fun->alertMessage ( "信息修改成功！", "control.php?action=adminList" );
		} else {
			$fun->addLog ( "修改了用户<" . $admin->getAdmin_name () . ">的权限失败" );
			$fun->closeDB ();
			$fun->alertMessage ( "信息修改失败！", "control.php?action=adminList" );
		}
	}
	/**
	 * 
	 * Enter description 修改管理员权限的操作
	 * @param fun $fun
	 * @param Smarty $smarty
	 */
	function modifyPurview(fun $fun, Smarty $smarty) {
		
		$adminId = $_GET ["adminId"];
		$adminService = new AdminService ();
		$admin = $adminService->getAdminById ( $adminId );
		$smarty->assign ( "userName", $admin->getAdmin_name () );
		$smarty->assign ( "email", $admin->getAdmin_email () );
		$smarty->assign ( "purview", $admin->getAdmin_purview () );
		$smarty->assign ( "adminId", $adminId );
		$smarty->display ( "admin/modifyPurview.html" );
	
	}
	/**
	 * 
	 * Enter description 查看当前登录的管理员信息操作
	 * @param unknown_type $fun
	 * @param unknown_type $smarty
	 */
	function adminInfo(fun $fun, Smarty $smarty) {
		
		$admin = unserialize ( $_SESSION ["user"] );
		$smarty->assign ( "admin", $admin );
		$smarty->display ( "admin/adminInfo.html" );
	}
	/**
	 * 
	 * Enter description 检测管理员是否同名操作
	 * @param unknown_type $fun
	 */
	function checkUserName(fun $fun) {
		
		header ( "content-type:text/html; charset=utf-8" );
		$name = $_GET ['name'];
		$admnService = new AdminService ();
		$name = iconv ( "utf-8", "gbk", $name );
		if (! $_GET ['oldName']) {
			if ($admnService->checkAdminByName ( $name )) {
				$fun->closeDB ();
				return true;
			} else {
				$fun->closeDB ();
				return false;
			}
		} else {
			$oldName = $_GET ['oldName'];
			$oldName = iconv ( "utf-8", "gbk", $oldName );
			
			if ($name == $oldName) {
				$fun->closeDB ();
				return false;
			} else {
				if ($admnService->checkAdminByName ( $name )) {
					$fun->closeDB ();
					return true;
				} else {
					$fun->closeDB ();
					return false;
				}
			}
		}
	}
	function getHomeworkList(Smarty $smarty,fun $fun)
	{
		$wtService = new WenTiService();
    	$before=0;
    	$after=0;
    	$keyword=null;
    	$type = 3;
    	$hwType=3;
    	$xyId = 0;
    	$lsId = 0;
    	if($_GET['before'])
    	{
    		$before = strtotime($_GET['before']);
    	}
    	if($_GET['after'])
    	{
    		$after =strtotime($_GET['after']);
    	}
    	if($_GET['keyword'])
    	{
    		$keyword = $_GET['keyword'];
    	}
    	if($_GET['type'])
    	{
    		$type = $_GET['type'];
    	}
    	if($_GET['hwType'])
    	{
    		$hwType = $_GET['hwType'];
    	}
    	if($_GET['xyId'])
    	{
    		$xyId=$_GET['xyId'];
    	}
    	if($_GET['lsId'])
    	{
    		$lsId= $_GET['lsId'];
    	}
    	//echo $_GET['xyId'];
    	$xyService = new XueYuanService();
    	$zyService = new ZuoYeService();
    	$fun->listPageForHW($wtService, $smarty,$before,$after,$keyword,$lsId,$xyId,$type-1,$hwType-1);
    	$smarty->assign("keyword",$keyword);
    	$smarty->assign("before",date ( "Y-m-d H:i:s",$before));
    	$smarty->assign("after",date ( "Y-m-d H:i:s",$after));
    	$smarty->assign("type",$type);
    	$smarty->assign("hwType",$hwType);
    	$smarty->assign("now",mktime());
    	$smarty->assign("xyList",$xyService->getAllList());
    	$fun->closeDB();
    	$smarty->display("admin/homeworkList.html");
	}
	function getZyList(Smarty $smarty,fun $fun)
	{
		
		$wt_id=0;
		$zy_name=null;
		$xs_id=0;
		$ls_id=0;
		$bj_id=0;
		$zy_state=0;
		$tj_date=0;
		$start_time=0;
		$end_time=0;
		if($_GET['wt_id'])
		{
			$wt_id= $_GET['wt_id'];
		}
		if($_GET['zy_name']&&$_GET['zy_name']!="null")
		{
			$zy_name=$_GET['zy_name'];
		}
		if($_GET['xs_id'])
		{
			$xs_id = $_GET['xs_id'];
		}
		if($_GET['ls_id'])
		{
			$ls_id = $_GET['ls_id'];
		}
		if($_GET['bj_id'])
		{
		$bj_id = $_GET['bj_id'];
		}
		if($_GET['zy_state'])
		{
			$zy_state = $_GET['zy_state'];
		}
		if($_GET['tj_date'])
		{
			$tj_date=$_GET['tj_date'];
			$start_time = strtotime($_GET['start_time']);
			$end_time = strtotime($_GET['end_time']);
		}
		$zyService = new ZuoYeService();
		$fun->getListForZY($zyService,$smarty,$wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start_time,$end_time);
	    $xyService = new XueYuanService();
	    $wtService = new WenTiService();
	    $smarty->assign("xyList",$xyService->getAllList());
		$smarty->assign("wt_id",$wt_id);
		$smarty->assign("zy_name",$zy_name);
		$smarty->assign("xs_id",$xs_id);
	    $smarty->assign("ls_id",$ls_id);
		$smarty->assign("bj_id",$bj_id);
		$smarty->assign("zy_sate",$zy_state);
		$smarty->assign("tj_date",$tj_date);
		$smarty->assign("start_time",$start_time);
		$smarty->assign("end_time",$end_time);
		$fun->closeDB();
		$smarty->display("admin/zyList.html");
	}

}
?>