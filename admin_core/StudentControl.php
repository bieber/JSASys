<?php

class StudentControl {
	
	public function checkStudentByName(fun $fun) {
		header ( "content-type:text/html; charset=utf-8" );
		$xsService = new XueShengService ();
		$name = $_GET ['xsName'];
		$oldName = $_GET ['oldName'];
		$name = iconv ( "utf-8", "gbk", $name );
		$oldName = iconv ( "utf-8", "gbk", $oldName );
		if ($oldName == "") {
			if ($xsService->checkXueSheng ( $name )) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($name == $oldName) {
				return false;
			} else {
				if ($xsService->checkXueSheng ( $name )) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
	public function checkXH(fun $fun) {
		$xh = $_GET ['xh'];
		$oldXH = $_GET ['oldXH'];
		$xy = $_GET ['xyId'];
		$zhy = $_GET ['zhyId'];
		$bj = $_GET ['bjId'];
		$xsService = new XueShengService ();
		if ($oldXH == "") {
			if ($xsService->checkXSByXH ( $xy, $zhy, $bj, $xh )) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($oldXH == $xh) {
				return false;
			} else {
				if ($xsService->checkXSByXH ( $xy, $zhy, $bj, $xh )) {
					return true;
				} else {
					return false;
				}
			}
		}
	
	}
	public function studentRegist(fun $fun) {
		$validata = $_POST ['validata'];
		if ($validata != $_SESSION ['validate']) {
			$fun->alertMessage ( "验证码错误！", "../studentRegist.php" );
		} else {
			$xs = new XueSheng ();
			$xs->setXs_loginname ( $_POST ['loginName'] );
			$xs->setXs_loginpwd ( $fun->stringToMD5 ( $_POST ['pwd'] ) );
			$xs->setXs_name ( $_POST ['rname'] );
			$xs->setXs_tele ( $_POST ['tele'] );
			$xs->setXs_email ( $_POST ['email'] );
			$xs->setXs_tx ( $fun->uploadImage ( "../studentTX/", "tx", 10000000, "../studentRegist.php" ) );
			$xs->setXs_xb ( $_POST ['xb'] );
			$xs->setXs_xh ( $_POST ['xh'] );
			$bjService = new BanJiService ();
			$bj = $bjService->getBanJiById ( $_POST ['bj'] );
			$xs->setBj ( $bj );
			$xs->setZhy ( $bj->getZhy () );
			$xs->setXy ( $bj->getZhy ()->getXy () );
			
			$xsService = new XueShengService ();
			if ($xsService->addXueSheng ( $xs )) {
				$systemService = new SystemService ();
				$system = $systemService->getSystem ();
				$fun->sendMail ( $_POST ['email'], $_POST ['rname'] . "同学：<br />&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;您好：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				欢迎你使用" . $system->getSiteName () . "！本网站主要是" . $system->getSiteDescpribt () . "！
				点击下面地址进入系统，
				如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>", "欢迎注册" . $system->getSiteName (), $system->getSiteEmail (), $system->getSiteEmailPassword () );
				$fun->closeDB ();
				$fun->alertRegistMessage ( "../studentRegist.html" );
			} else {
				$fun->alertMessage ( "注册失败！请重新注册！", "../studentRegist.php" );
			}
		}
	}
	public function studentList(Smarty $smarty, fun $fun) {
		$xsService = new XueShengService ();
		$xyService = new XueYuanService ();
		$zhyService = new ZhuanYeService ();
		$bjService = new BanJiService ();
		$searchContent = "";
		if ($_GET ['xsName']) {
			if ($_GET ['bjId']) {
				$bj = $bjService->getBanJiById ( $_GET ['bjId'] );
				$searchContent = $searchContent . "在<font color=red>" . $bj->getZhy ()->getXy ()->getXy_name () . "</font>的<font color=red>" . $bj->getZhy ()->getZhy_name () . $bj->getBj_name () . "</font>学生集合中包含<font color=red>" . $_GET ['xsName'] . "</font>的";
			} else if ($_GET ['zhyId'] && ! $_GET ['bjId']) {
				$zhy = $zhyService->getZhuanYeById ( $_GET ['zhyId'] );
				$searchContent = $searchContent . "在<font color=red>" . $zhy->getXy ()->getXy_name () . "</font>的<font color=red>" . $zhy->getZhy_name () . "</font>的专业学生集合中包含<font color=red>" . $_GET ['xsName'] . "</font>的";
			} else if ($_GET ['xyId'] && ! $_GET ['zhyId'] && ! $_GET ['bjId']) {
				$xy = $xyService->getXueYuanById ( $_GET ['xyId'] );
				$searchContent = $searchContent . "在<font color=red>" . $xy->getXy_name () . "</font>的学生集合中包含<font color=red>" . $_GET ['xsName'] . "</font>的";
			} else if (! $_GET ['xyId'] && ! $_GET ['zhyId'] && ! $_GET ['bjId']) {
				$searchContent = $searchContent . "在全校的学生集合中包含<font color=red>" . $_GET ['xsName'] . "</font>的";
			}
		} else {
			if ($_GET ['bjId']) {
				$bj = $bjService->getBanJiById ( $_GET ['bjId'] );
				$searchContent = $searchContent . "在<font color=red>" . $bj->getZhy ()->getXy ()->getXy_name () . "</font>的<font color=red>" . $bj->getZhy ()->getZhy_name () . $bj->getBj_name () . "</font>";
			} else if ($_GET ['zhyId'] && ! $_GET ['bjId']) {
				$zhy = $zhyService->getZhuanYeById($_GET['zhyId']);
			$searchContent = $searchContent."在<font color=red>".$zhy->getXy()->getXy_name()."</font>的<font color=red>".$zhy->getZhy_name()."</font>的专业";
		}
		else if($_GET['xyId']&&!$_GET['zhyId']&&!$_GET['bjId'])
		{
			$xy = $xyService->getXueYuanById($_GET['xyId']);
			$searchContent = $searchContent."在<font color=red>".$xy->getXy_name()."</font>的";
		}
		}
		$smarty->assign("searchContent",$searchContent);
		$smarty->assign("xyList",$xyService->getAllList());
		$fun->listPageByIdForStudent($xsService, $smarty);
		$fun->closeDB();
		$smarty->display("admin/studentList.html");
	}
	public function deleteXSBatch(fun $fun)
	{
		$xsList = $_POST['xsId'];
		$xsService = new XueShengService();
		$xs = $xsService->getXueShengById($xsList[0]); 
		
		if($xsService->deleteXueShengBatch($xsList))
		{
			$fun->addLog("批量删除学生");
			$fun->closeDB();
			$fun->alertMessage("操作成功！", "control.php?action=studentList");
			
		}
		else
		{
			$fun->addLog("批量删除学生失败");
			$fun->closeDB();
			$fun->alertMessage("操作失败！", "control.php?action=studentList");
		}
	}
	public function login(fun $fun)
	{
		$user_name = $_POST ["username"];
		$user_psw = $_POST ["password"];
		$validate = $_POST ["chknumber"];
		$user_name = addslashes ( $user_name );
		$user_psw = addslashes ( $user_psw );
		$user_psw = $fun->stringToMD5 ( $user_psw );
		$xs = new XueSheng();
		$xs->setXs_loginname($user_name);
		$xs->setXs_loginpwd($user_psw);
		$chineseSpell = new ChineseSpell ();
		$xsService = new XueShengService();
		$xs = $xsService->checkXSLogin($xs);
		if ($validate != $_SESSION ["validate"] && $validate != $chineseSpell->getFullSpell ( $_SESSION ['validate'] )) {
			$fun->closeDB ();
			$fun->alertMessage ( "你输入的验证码错误！请重新出入...", "../index.php" );
			return;
		} else if ($xs) {
			$_SESSION ["user"] = serialize ( $xs );
			$userId = strtotime ( date ( "Y-m-d H:i:s" ) );
			$_SESSION ['userId'] = $userId;
			$_SESSION ['time'] = mktime ();
			$fun->addLog("登录成功");
			$fun->closeDB ();
			$fun->alertMessage ( "欢迎" . $xs->getXs_name () . "使用该系统！", "../main.php" );
		} else {
			$fun->closeDB ();
			$fun->alertMessage ( "输入的登录名、密码错误或者还未通过管理员审核！请重新输入...", "../index.php" );
		}
		
	}
	public function updateXS(fun $fun)
	{
		$xsService = new XueShengService();
		$loginName = $_POST['loginName'];
		$userId = $_POST['userId'];
		$pwd = $_POST['pwd'];
		$xs = $xsService->getXueShengById($userId);
		$xs->setXs_loginname($loginName);
		$xs->setXs_loginpwd($fun->StringToMD5($pwd));
		if($xsService->updateXueSheng($xs))
		{
			$fun->addLog("修改密码");
			$_SESSION ["user"] = serialize ( $xs );
			$fun->closeDB();
			$fun->alertMessage("修改成功！", "../right.php");
		}
		else 
		{
			$fun->addLog("修改密码失败");
			$fun->closeDB();
			$fun->alertMessage("修改失败！", "../right.php");
		}
	}

	public function updateXSInfo(fun $fun)
	{
		$xs = unserialize($_SESSION["user"]);
		$xs->setXs_name ( $_POST ['rname'] );
		$xs->setXs_tele ( $_POST ['tele'] );
		$xs->setXs_email ( $_POST ['email'] );
		$file = $_FILES ["tx"];
		if($file ["name"])
		{
		$fun->deleteFile("../studentTX/", $xs->getXs_tx());
		$xs->setXs_tx ( $fun->uploadImage ( "../studentTX/", "tx", 10000000, "../modifyXsInfo.php" ) );
		}
		$xs->setXs_xb ( $_POST ['xb'] );
		$xs->setXs_xh ( $_POST ['xh'] );
		$bjService = new BanJiService ();
		$bj = $bjService->getBanJiById ( $_POST ['bj'] );
		$xs->setBj ( $bj );
		$xs->setZhy ( $bj->getZhy () );
		$xs->setXy ( $bj->getZhy ()->getXy () );
		$xsService = new XueShengService ();
		
		if($xsService->updateXueSheng($xs))
		{
			$fun->addLog("修改个人信息");
			$_SESSION ["user"] = serialize ( $xs );
			$fun->closeDB();
			$fun->alertMessage("修改成功！", "../right.php");
		}else 
		{
			$fun->addLog("修改个人信息失败");
			$fun->closeDB();
			$fun->alertMessage("修改失败！", "../right.php");
		}
	}
	public function submitHW(fun $fun)
	{
		$content = $_POST['content'];
		$attach = 'none';
		$zyName = $_POST['zyName'];
		$hwId = $_POST['hwId'];

		$wtService = new WenTiService();
		$wt = $wtService->getWenTiById($hwId);
		$file = $_FILES['attachFile'];
		
		$xs = unserialize($_SESSION['user']);
		$zyService = new ZuoYeService();
		$zyList = $zyService->getZuoYeByXSORWT($xs->getXs_id(),$hwId);
		if($zyList==null)//学生第一次提交作业
		{
		if($file['name'])
		{
			
			$attach = $fun->uploadFile('attachFile', "../studentHomework/", "../submitHomework.php?hwId=".$wt->getWt_id());
		}
		$zyService = new ZuoYeService();
		$zy = new ZuoYe();
		$zy->setBj($xs->getBj());
		$zy->setLs($wt->getLs());
		$zy->setTj_date(mktime());
		$zy->setWt($wt);
		$zy->setXs($xs);
		$zy->setZy_content($content);
		$zy->setZy_file($attach);
		$zy->setZy_fs(0);
		$zy->setZy_name($zyName);
		$zy->setZy_py("还未审核....");
		$zy->setZy_sate(0);
		if($zyService->submitZuoYe($zy))
		{
			$fun->addLog("提交作业<".$wt->getWt_name().">");
			$fun->closeDB();
			if($_POST['type'])
			{
			$fun->alertMessage("作业提交成功！", "controlXs.php?action=searchHw");
			}
			else
			{
				$fun->alertMessage("作业提交成功！", "../getPositionHW.php");
			}
			}else
		{
			$fun->addLog("提交作业失败<".$wt->getWt_name().">");
			$fun->closeDB();
		if($_POST['type'])
			{
			$fun->alertMessage("作业提交失败！", "controlXs.php?action=searchHw");
			}
			else
			{
				$fun->alertMessage("作业提交失败！", "../getPositionHW.php");
			}
		}
		}
		else//学生提交重做作业
		{
		
		$zy = $zyList[0];
		if($file['name'])
		{
			$fun->deleteFile("../studentHomework/", $zy->getZy_file());
			$attach = $fun->uploadFile('attachFile', "../studentHomework/", "../submitHomework.php?hwId=".$wt->getWt_id());
		}
		$zy->setTj_date(mktime());
		$zy->setZy_content($content);
		$zy->setZy_file($attach);
		$zy->setZy_fs(0);
		$zy->setZy_name($wt->getWt_name());
		$zy->setZy_py("还未审核....");
		$zy->setZy_sate(0);
		if($zyService->reDoZuoYe($zy))
		{
			$fun->addLog("重做作业<".$wt->getWt_name().">");
			$fun->closeDB();
		if($_POST['type']=='search')
			{
			$fun->alertMessage("作业提交成功！", "controlXs.php?action=searchHw");
			}
			else if ($_POST['type']=='getUnPassed')
			{
				$fun->alertMessage("作业提交成功！", "controlXs.php?action=getUnPassed");
			}
			else
			{
				$fun->alertMessage("作业提交成功！", "../getPositionHW.php");
			}
			
		}else
		{
			$fun->addLog("重做作业提交失败<".$wt->getWt_name().">");
			$fun->closeDB();
		if($_POST['type']=='search')
			{
			$fun->alertMessage("作业提交成功！", "controlXs.php?action=searchHw");
			}
			else if ($_POST['type']=='getUnPassed')
			{
				$fun->alertMessage("作业提交成功！", "controlXs.php?action=getUnPassed");
			}
			else
			{
				$fun->alertMessage("作业提交成功！", "../getPositionHW.php");
			}
		}
		}
		
	}
	public function searchHw(Smarty $smarty,fun $fun)
	{
		$wtService = new WenTiService();
    	$before=0;
    	$after=0;
    	$keyword=null;
    	$type = 3;
    	$hwType=3;
    	$xyId = 0;
    	$lsId = 0;
    	if($_GET['before'])
    	{
    		$before = strtotime($_GET['before']);
    	}
    	if($_GET['after'])
    	{
    		$after =strtotime($_GET['after']);
    	}
    	if($_GET['keyword'])
    	{
    		$keyword = $_GET['keyword'];
    	}
    	if($_GET['type'])
    	{
    		$type = $_GET['type'];
    	}
    	if($_GET['hwType'])
    	{
    		$hwType = $_GET['hwType'];
    	}
    	if($_GET['xyId'])
    	{
    		$xyId=$_GET['xyId'];
    	}
    	if($_GET['lsId'])
    	{
    		$lsId= $_GET['lsId'];
    	}
    	//echo $_GET['xyId'];
    	$xyService = new XueYuanService();
    	$zyService = new ZuoYeService();
    	$fun->listPageForHW($wtService, $smarty,$before,$after,$keyword,$lsId,$xyId,$type-1,$hwType-1);
    	$smarty->assign("keyword",$keyword);
    	$smarty->assign("before",date ( "Y-m-d H:i:s",$before));
    	$smarty->assign("after",date ( "Y-m-d H:i:s",$after));
    	$smarty->assign("type",$type);
    	$smarty->assign("hwType",$hwType);
    	$smarty->assign("now",mktime());
    	$smarty->assign("xyList",$xyService->getAllList());
    	$fun->closeDB();
    	$smarty->display("student/searchHomework.html");
	}
	public function getHW(Smarty $smarty,fun $fun,$state)
	{
		$wt_id=0;
		$zy_name=null;
		$xs_id=0;
		$ls_id=0;
		$bj_id=0;
		$zy_state=0;
		$tj_date=0;
		$start_time=0;
		$end_time=0;
		if($_GET['wt_id'])
		{
			$wt_id= $_GET['wt_id'];
		}
		if($_GET['zy_name']&&$_GET['zy_name']!="null")
		{
			$zy_name=$_GET['zy_name'];
		}
		if($_GET['xs_id'])
		{
			$xs_id = $_GET['xs_id'];
		}
		if($_GET['ls_id'])
		{
			$ls_id = $_GET['ls_id'];
		}
		if($_GET['bj_id'])
		{
		$bj_id = $_GET['bj_id'];
		}
		if($_GET['zy_state'])
		{
			$zy_state = $_GET['zy_state'];
		}
		if($_GET['tj_date'])
		{
			$tj_date=$_GET['tj_date'];
			$start_time = strtotime($_GET['start_time']);
			$end_time = strtotime($_GET['end_time']);
		}
		$xs = unserialize($_SESSION['user']);
		$xs_id = $xs->getXs_id();
		$bj_id = $xs->getBj()->getBj_id();
		$zy_state = $state;
		$zyService = new ZuoYeService();
		$fun->getListForZY($zyService,$smarty,$wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start_time,$end_time);
	    $xyService = new XueYuanService();
	    $wtService = new WenTiService();
	    $smarty->assign("xyList",$xyService->getAllList());
		$smarty->assign("wt_id",$wt_id);
		$smarty->assign("zy_name",$zy_name);
		$smarty->assign("xs_id",$xs_id);
	    $smarty->assign("ls_id",$ls_id);
		$smarty->assign("bj_id",$bj_id);
		$smarty->assign("zy_sate",$zy_state);
		$smarty->assign("tj_date",$tj_date);
		$smarty->assign("start_time",$start_time);
		$smarty->assign("end_time",$end_time);
		$fun->closeDB();
		$smarty->display("student/zyList.html");
		
	}
	function getLeaveWordList(Smarty $smarty,fun $fun,$state)
	{
		$before=0;
		if($_GET['before'])
		{
			$before = $_GET['before'];
		}
		$after =0;
		if($_GET['after'])
		{
			$after = $_GET['after'];
		}
		$type = 0;
		if($_GET['type'])
		{
			$type=$_GET['type'];
		}
		$lyService = new LiuYanServie();
		$xs = unserialize($_SESSION['user']);
		$fun->getListForLY($lyService, $smarty, $xs->getXs_id(), $state,$type,strtotime($before),strtotime($after));
		$fun->closeDB();
		$smarty->assign("before",$before);
		$smarty->assign("after",$after);
		$smarty->display("student/leaveWordList.html");
		
	}
}
?>