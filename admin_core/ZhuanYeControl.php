<?php
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
/**
 * 
 * Enter description 该类是对专业实体的各种操作的控制
 * @author hebibo
 *
 */
class ZhuanYeControl{
	
	/**
	 * 
	 * Enter description 检测专业重名的操作
	 */
	public function checkZhY($fun)
	{
		header ( "content-type:text/html; charset=utf-8" );
		$zhyName = $_GET['zhyName'];
		$xyId = $_GET['xyId'];
		$oldName = $_GET['oldName'];
		$zhyName = iconv("utf-8", "gbk", $zhyName);
		$xyId = iconv("utf-8", "gbk", $xyId);
		$oldName = iconv("utf-8", "gbk", $oldName);
			$zhyService = new ZhuanYeService();
		if($oldName=="")
		{
		
			if($zhyService->checkZhuanYe($zhyName, $xyId))
			{
				$fun->closeDB();
				return true;
			}else
			{
				$fun->closeDB();
				return false;
			}
		}
		else
		{
			if($oldName==$zhyName)
			{
				$fun->closeDB();
				return false;
			}
			else {
			if($zhyService->checkZhuanYe($zhyName, $xyId))
			{
				$fun->closeDB();
				return true;
			}else
			{
				$fun->closeDB();
				return false;
			}
			}
		}
		
	}
	/**
	 * 
	 * Enter description 获得专业列表的操作
	 * @param fun $fun
	 * @param Smarty $smarty
	 */
	public function  getList(fun $fun,Smarty $smarty)
	{
		$zhyService = new ZhuanYeService();
		$id = $_GET['xyId'];
		$xyService = new XueYuanService();
		$smarty->assign("xy",$xyService->getXueYuanById($id));
		$fun->listPageById($zhyService, $smarty, $id);
		$fun->closeDB();
		$smarty->display("admin/zhyList.html");
		
	}
	/**
	 * 
	 * Enter description 添加专业操作
	 * @param fun $fun
	 */
	public function addZhy(fun $fun)
	{
		$zhyService  = new ZhuanYeService();
		$xyService = new XueYuanService();
		$xy = new XueYuan();
		$xyId = $_POST['xyId'];
		$zhyName = $_POST['zhyName'];
		$zhyDescribte = $_POST['zhyDescribt'];
		$xy=$xyService->getXueYuanById($xyId);
		$zhy = new ZhuanYe();
		$zhy->setXy($xy);
		$zhy->setZhy_describe($zhyDescribte);
		$zhy->setZhy_name($zhyName);
		if($zhyService->addZhuanYe($zhy))
		{
		$fun->addLog("在学院<".$xy->getXy_name().">添加了<".$zhy->getZhy_name().">专业");
			$fun->closeDB ();
		$fun->alertMessage("专业添加成功！", "control.php?action=zhyList&xyId=$xyId");
	}}
	/**
	 * 
	 * Enter description 删除专业操作
	 * @param fun $fun
	 */
	public function deleteZhy(fun $fun)
	{
		$zhyService = new ZhuanYeService();
		$zhyId = $_GET['zhyId'];
		$xyId = $_GET['xyId'];
		if($zhyService->deleteZhuanYeById($zhyId))
		{
			$fun->addLog("删除了一个专业");
			$fun->closeDB();
			$fun->alertMessage("删除专业成功！", "control.php?action=zhyList&xyId=$xyId");
		}
		else 
		{
			$fun->addLog("删除了一个专业失败");
			$fun->closeDB();
			$fun->alertMessage("删除专业失败！", "control.php?action=zhyList&xyId=$xyId");
		}
	}
	/**
	 * 
	 * Enter description 更新专业操作
	 * @param fun $fun
	 */
	public function updateZhy(fun $fun)
	{
		$zhyName = $_POST['zhyName'];
		$zhyDescribt = $_POST['zhyDescribt'];
	    $xyId = $_POST['xyId'];
	    $zhyId = $_POST['zhyId'];
	    $xyService = new XueYuanService();

	    $xy = $xyService->getXueYuanById($xyId);
	    
	    $zhyService = new ZhuanYeService();
	    $zhy = $zhyService->getZhuanYeById($zhyId);
	    $zhy->setXy($xy);
	    $zhy->setZhy_describe($zhyDescribt);
	    $zhy->setZhy_name($zhyName);

	  if($zhyService->updateZhuanYe($zhy,$fun))
	  {
	  	$fun->addLog("更新了<".$xy->getXy_name().">学院的<".$zhy->getZhy_name().">专业的信息");
			$fun->closeDB();
	  	$fun->alertMessage("修改专业成功！", "control.php?action=zhyList&xyId=$xyId");
	  }
	  else {
	  	$fun->addLog("更新了<".$xy->getXy_name().">学院的<".$zhy->getZhy_name().">专业的信息失败");
			$fun->closeDB();
	  		$fun->alertMessage("修改专业失败！", "control.php?action=zhyList&xyId=$xyId");
	  }
	}
}
?>