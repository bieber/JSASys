<?php
/**
 * 说明：该文件主要作用是对用户对服务器的请求进行控制实现MVC中的控制层
 * @author hebibo
 */
session_start ();
include_once 'config.php';
include_once ROOT . '/services/AdminService.php';
include_once ROOT . '/models/Admin.php';
include_once ROOT . '/utils/Function.php';
include_once ROOT . '/utils/ChineseSpell .php';
include_once ROOT . '/utils/email.class.php';
include_once ROOT . '/utils/DBBak.php';
include_once ROOT . '/models/XueYuan.php';
include_once ROOT . '/models/BanJi.php';
include_once ROOT . '/models/System.php';
include_once ROOT . '/models/Log.php';
include_once ROOT . '/models/LaoShi.php';
include_once ROOT . '/models/XueSheng.php';
include_once ROOT . '/models/News.php';
include_once ROOT.'/models/WenTi.php';
include_once ROOT.'/models/WT2XSRelation.php';
include_once ROOT.'/models/ZuoYe.php';
include_once ROOT . '/services/XueYuanService.php';
include_once ROOT . '/services/ZhuanYeService.php';
include_once ROOT . '/services/BanJiService.php';
include_once ROOT . '/services/SystemService.php';
include_once ROOT . '/services/LogService.php';
include_once ROOT . '/services/LaoShiService.php';
include_once ROOT . '/services/XueShengService.php';
include_once ROOT . '/services/NewsService.php';
include_once ROOT.'/services/WenTiService.php';
include_once ROOT.'/services/WT2XSRelationService.php';
include_once ROOT.'/services/ZuoYeService.php';
include_once 'smarty_inc.php';
include_once 'AdminControl.php';
include_once 'XueYuanControl.php';
include_once 'ZhuanYeControl.php';
include_once 'BanJiControl.php';
include_once 'SystemControl.php';
include_once 'LogControl.php';
include_once 'TeacherControl.php';
include_once 'StudentControl.php';
include_once 'NewsControl.php';
include_once 'HomeworkControl.php';
$action = $_GET ["action"];
$adminControl = new AdminControl ();
$controlXueYuan = new XueYuanControl ();
$controlZhY = new ZhuanYeControl ();
$bjControl = new BanJiControl ();
$systemControl = new SystemControl ();
$fun = new fun ();
$logControl = new LogControl ();
$teacherControl = new TeacherControl ();
$studentControl = new StudentControl ();
$newsControl = new NewsControl();
/**
 * 判断用户是否登录
 */
if ($action != "login" && $action != "registTeacher" && $action != "checkTeacherName" && $action != "getZhyAjax" && $action != "getBJAjax" && $action != "checkStudentName" && $action != "checkXH" && $action != 'studentRegist') {
	
	$fun->checkLogin ();
}
/**
 * 一下均为对用户请求的事件进行控制响应
 */
if ($action == "login") {
	$logintype = $_POST ['logintype'];
	$_SESSION ['logintype'] = $logintype;
	if ($logintype == 'admin') {
		$adminControl->login ( $fun );
	} else if ($logintype == 'ls') {
		$teacherControl->login ( $fun );
	
	} else if ($logintype == 'xs') {
	    $studentControl->login($fun);
	} else {
		$fun->alertMessage ( "非法登录！", "../index.php" );
	}
} else if ($action == "modifyAdmin") {
	$adminControl->viewAdmin ( $smarty, $fun );
} else if ($action == "updateAdmin") {
	$adminControl->updateAdmin ( $fun );
} else if ($action == "adminList") {
	$adminControl->adminList ( $smarty, $fun );
} else if ($action == "addAdmin") {
	
	$adminControl->addAdmin ( $fun );
} else if ($action == "deleteAdmin") {
	
	$adminControl->deleteAdmin ( $fun );
} else if ($action == "modifyPurview") {
	$adminControl->modifyPurview ( $fun, $smarty );
} else if ($action == "updatePurview") {
	$adminControl->updatePurview ( $fun );
} else if ($action == "adminInfo") {
	$adminControl->adminInfo ( $fun, $smarty );
} else if ($action == "addXY") {
	$controlXueYuan->addXY ( $fun );
} else if ($action == "checkXY") {
	
	if ($controlXueYuan->checkXY ( $fun )) {
		echo "true";
	} else {
		echo "false";
	}
} else if ($action == "checkUserName") {
	if ($adminControl->checkUserName ( $fun )) {
		echo "true";
	
	} else {
		echo "false";
	}
} else if ($action == "xyList") {
	$controlXueYuan->xyList ( $fun, $smarty );
} else if ($action == "deleteXY") {
	$controlXueYuan->deleteXY ( $fun );
} else if ($action == "modifyXY") {
	$controlXueYuan->modifyXY ( $fun );
} else if ($action == "checkZhY") {
	if ($controlZhY->checkZhY ( $fun )) {
		
		echo "true";
	} else {
		
		echo "false";
	}
} else if ($action == "zhyList") {
	$controlZhY->getList ( $fun, $smarty );
} else if ($action == "addZhy") {
	$controlZhY->addZhy ( $fun );
} else if ($action == "deleteZhY") {
	$controlZhY->deleteZhy ( $fun );
} else if ($action == "upDateZhy") {
	$controlZhY->updateZhy ( $fun );
} else if ($action == "exit") {
	$logintype = $_SESSION ['logintype'];
	if ($logintype == "admin") {
		$fun->addLog ( "退出系统" );
	}
	session_destroy ();
	$fun->alertMessage ( "退出成功！欢迎下次使用！", "../index.php" );
} else if ($action == "checkBJ") {
	if ($bjControl->checkBJ ( $fun )) {
		echo "true";
	} else {
		echo "false";
	}
} else if ($action == "addBJ") {
	$bjControl->addBJ ( $fun );
} else if ($action == "updateBJ") {
	$bjControl->updateBJ ( $fun );
} else if ($action == "deleteBJ") {
	$bjControl->deleteBJ ( $fun );
} else if ($action == "updateSystem") {
	$systemControl->updateSystem ( $fun );
} else if ($action == "logList") {
	if ($_GET ['before'] && $_GET ['after']) {
		$logControl->searchLogByDate ( $smarty, $fun );
	} else {
		$logControl->logList ( $smarty, $fun );
	}
} else if ($action == "deletLogBatch") {
	$logControl->deleteLogBetch ( $fun );

} else if ($action == "registTeacher") {
	$teacherControl->registerTeacher ( $fun );
} else if ($action == "checkTeacherName") {
	
	if ($teacherControl->checkTeacher ( $fun )) {
		echo "true";
	} else {
		echo "false";
	}
} else if ($action == "waitValidateTeachers" || $action == "validatedTeachers" || $action == "reValidatedTeachers") {
	if (! $_GET ['xyId']) {
		$teacherControl->getTeachers ( $smarty, $fun, $action );
	} else {
		$teacherControl->getTeachersById ( $smarty, $fun, $action );
	}
} else if ($action == "teacherPass") {
	$teacherControl->teacherPassing ( $fun );
} else if ($action == "deletePassed") {
	$teacherControl->deletePassed ( $fun );
} else if ($action == "deleteTeacher") {
	$teacherControl->deleteTeacher ( $fun );
} else if ($action == "getZhyAjax") {
	header ( "content-type:text/html; charset=utf-8" );
	$xyId = $_GET ['xyid'];
	$xyId = iconv ( "utf-8", "gbk", $xyId );
	$zhyService = new ZhuanYeService ();
	$zhyList = $zhyService->getAllListById ( $xyId );
	$content = "<select class=\"textFile\" name=\"zhy\" id=\"zhy\" style=\"text-align:center;\" onchange=\"getBJ(this)\"  ><option style=\" text-align:center;\" value=\"none\">====" . iconv ( "gbk", "utf-8", "请 选 择" ) . "====</option>";
	if ($zhyList) {
		
		for($i = 0; $i < count ( $zhyList ); $i ++) {
			$content = $content . "<option  value=\"" . $zhyList [$i]->getZhy_id () . "\">" . iconv ( "gbk", "utf-8", $zhyList [$i]->getZhy_name () ) . "</option>";
		}
	} else {
		$content = $content . "<option value=\"none\">" . iconv ( "gbk", "utf-8", "该学院还未创建专业" ) . "</option>";
	}
	echo $content . "</select>";
} else if ($action == "getBJAjax") {
	header ( "content-type:text/html; charset=utf-8" );
	$zhyId = $_GET ['zhyid'];
	$zhyId = iconv ( "utf-8", "gbk", $zhyId );
	$bjService = new BanJiService ();
	$bjList = $bjService->getBanJiByZhy ( $zhyId );
	$content = "<select class=\"textFile\" name=\"bj\" id=\"bj\" style=\"text-align:center;\" ><option style=\" text-align:center;\" value=\"none\">====" . iconv ( "gbk", "utf-8", "请 选 择" ) . "====</option>";
	if ($bjList) {
		for($i = 0; $i < count ( $bjList ); $i ++) {
			$content = $content . "<option  value=\"" . $bjList [$i]->getBj_id () . "\">" . iconv ( "gbk", "utf-8", $bjList [$i]->getBj_name () ) . "</option>";
		}
	} else {
		$content = $content . "<option  value=\"none\">" . iconv ( "gbk", "utf-8", "该专业还未创建班级" ) . "</option>";
	}
	echo $content . "</select>";
} else if ($action == "checkStudentName") {
	if ($studentControl->checkStudentByName ( $fun )) {
		echo "true";
	} else {
		echo "false";
	}
} else if ($action == "checkXH") {
	if ($studentControl->checkXH ( $fun )) {
		echo "true";
	} else {
		echo "false";
	}
} else if ($action == "studentRegist") {
	$studentControl->studentRegist ( $fun );
}
else if($action == "studentList")
{
	$studentControl->studentList($smarty,$fun);
}
else if($action == "deletXSBatch")
{
	$studentControl->deleteXSBatch($fun);
}
else if($action =="addNews")
{
	$newsControl->addNews($fun);
}
else if($action=="newsList")
{
	$newsControl->newsList($smarty, $fun);
}
else if($action =="deletNewsBatch")
{
	$newsControl->deletNewsBatch($fun);
}
else if($action == "updateNews")
{
$newsControl->updateNews($fun);
}
else if($action == "AjaxBJ")
{
	header ( "content-type:text/html; charset=utf-8" );
	$bj_id = $_GET['bjId'];
    $xsService = new XueShengService();
    $xsList = $xsService->getAllListById(0,$bj_id);
    if(count($xsList)==0)
    {
    
    $content = $content.iconv ( "gbk", "utf-8", "该班还未有学生注册" );
    }
    else{
    	$content = $content."<table border=\"0\" width=\"100%\">";
    for($i=0; $i<count($xsList); $i++)
    {
    	$content = $content."<tr onmousemove=\"changeBackColor(this)\" onmouseout=\"removeBackColor(this)\">";
    	$content = $content."<td style=\"text-align:left; list-style:none; border:0px; height:20px; line-height:20px; border:0px; width:20%;\" >";
    	$content = $content."|-- ".iconv ( "gbk", "utf-8", $xsList[$i]->getXs_name());
    	$content = $content."</td>";
    	$content = $content."<td style=\"text-align:left; list-style:none; border:0px; height:20px; line-height:20px; border:0px;\" >";
    	$content = $content.iconv ( "gbk", "utf-8", " <a href=\"#\" onclick=\"checkStudentDetail(".$xsList[$i]->getXs_id().")\">[查看详细信息]</a>" );
    	$content = $content."</td></tr>";
    }
    $content = $content."</table>";
    }
     echo $content;
}
else if($action == "BakDB")
{
$connectid = mysql_connect('localhost','root','root');
mysql_query ( "set names 'utf8'" ) or die ( "设置失败...." );
$backupDir = '../bakup';
$DbBak = new DBBak($connectid,$backupDir);
$DbBak->backupDb('task_examination_db');
$fun->addLog("备份数据库");
$fun->closeDB();
echo "success";
}
else if($action =="restore")
{
$connectid = mysql_connect('localhost','root','root');
mysql_query ( "set names 'utf8'" ) or die ( "设置失败...." );
$backupDir = '../bakup';
$DbBak = new DbBak($connectid,$backupDir);
$DbBak->restoreDb('task_examination_db');
$fun->addLog("恢复数据库");
$fun->closeDB();
echo "success";
}
else if($action == 'getHomeworkList')
{
	$adminControl->getHomeworkList($smarty, $fun);
}
else if($action == 'deleteHW')
{
        $wtService = new WenTiService();
    	$wtList = $_POST['hwId'];
    	if($wtService->deleteWenTiBatch($wtList))
    	{
    		$fun->addLog("删除了作业");
	    	$fun->closeDB();
	    	$fun->alertMessage("删除成功！", "control.php?action=getHomeworkList");
    	}
    	else 
    	{
    		$fun->addLog("删除了作业失败");
	    	$fun->closeDB();
	    	$fun->alertMessage("删除失败！", "control.php?action=getHomeworkList");
    	}
}
else if($action  == 'getZyList')
{
	$adminControl->getZyList($smarty, $fun);
}
?>