<?php /* Smarty version Smarty-3.0.7, created on 2014-09-11 06:45:46
         compiled from "./templates\admin/adminInfo.html" */ ?>
<?php /*%%SmartyHeaderCode:209165411451a6b4386-30794762%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e60ae9f97d70a1559dd930c49d3c287e20090f5' => 
    array (
      0 => './templates\\admin/adminInfo.html',
      1 => 1410414664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '209165411451a6b4386-30794762',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
input{
	color:#3CC;}
-->
</style>
<script type="text/javascript"  src="../admin_core/templates/admin/js/comment.js" charset="gbk" >
</script>
<script type="text/javascript">
function conf()
{
	if(confirm('确认修改<?php echo $_smarty_tpl->getVariable('userName')->value;?>
权限吗？'))
	{
	document.getElementById("messgeForm").submit();	
	}
}
</script>
</head>

<body>
<center>
<div style="width:100%; height:50px;">
</div>
<fieldset style="width:80%;">
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">管理员信息</span></legend>
<table width="60%">
<tr>
<td width="50%" style="text-align:right; font-size:14px; color:#09C;">
用户名：
</td>
<td style="text-align:left; font-size:14px; color:#00F;">
<?php echo $_smarty_tpl->getVariable('admin')->value->getAdmin_name();?>

</td>
</tr>
<tr>
<td width="50%" style="text-align:right; font-size:14px; color:#09C;">
Email：
</td>
<td style="text-align:left; font-size:14px; color:#00F;">
<?php echo $_smarty_tpl->getVariable('admin')->value->getAdmin_email();?>

</td>
</tr>
<tr>
<td width="50%" style="text-align:right; font-size:14px; color:#09C;">
权限：
</td>
<td style="text-align:left; font-size:14px; color:#00F;">
<?php if ($_smarty_tpl->getVariable('admin')->value->getAdmin_purview()==1){?>
普通管理员
<?php }elseif($_smarty_tpl->getVariable('admin')->value->getAdmin_purview()==2){?>
管理员
<?php }elseif($_smarty_tpl->getVariable('admin')->value->getAdmin_purview()==3){?>
超级管理员
<?php }?>
</td>
</tr>
<tr>
<td width="50%" style="text-align:right; font-size:14px; color:#09C;">
最后登录时间：
</td>
<td style="text-align:left; font-size:14px; color:#00F;">
<?php echo $_smarty_tpl->getVariable('admin')->value->getLast_login();?>

</td>
</tr>
<tr>
<td width="50%" style="text-align:right; font-size:14px; color:#09C;">
最后登录的Ip：
</td>
<td style="text-align:left; font-size:14px; color:#00F;">
<?php echo $_smarty_tpl->getVariable('admin')->value->getLogin_ip();?>

</td>
</tr>

</table>
</fieldset>
</center>
</body>
</html>
