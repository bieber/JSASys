<?php
session_start ();
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
include_once ROOT . '/utils/ChineseSpell .php';
include_once ROOT . '/utils/email.class.php';
include_once ROOT . '/models/XueYuan.php';
include_once ROOT . '/models/BanJi.php';
include_once ROOT . '/models/System.php';
include_once ROOT . '/models/Log.php';
include_once ROOT . '/models/LaoShi.php';
include_once ROOT . '/models/XueSheng.php';
include_once ROOT . '/models/News.php';
include_once ROOT . '/models/WenTi.php';
include_once ROOT.'/models/LiuYan.php';
include_once ROOT . '/models/WT2XSRelation.php';
include_once ROOT . '/services/XueYuanService.php';
include_once ROOT . '/services/ZhuanYeService.php';
include_once ROOT . '/services/BanJiService.php';
include_once ROOT . '/services/SystemService.php';
include_once ROOT . '/services/LogService.php';
include_once ROOT . '/services/LaoShiService.php';
include_once ROOT . '/services/XueShengService.php';
include_once ROOT . '/services/NewsService.php';
include_once ROOT . '/services/WenTiService.php';
include_once ROOT . '/services/WT2XSRelationService.php';
include_once ROOT.'/services/LiuYanService.php';
include_once 'smarty_inc.php';
include_once 'TeacherControl.php';
include_once 'HomeworkControl.php';
$action = $_GET ["action"];
$teacherControl = new TeacherControl ();
$hwControl = new HomeworkControl();
$fun = new fun ();
if ($action != "login") {
	$fun->checkLogin ();
}
if($action =="updateLs")
{
	$teacherControl->updateLs($fun);
}
else if($action== "updateLsInfo")
{
	$teacherControl->updateLsInfo($fun);
}
else if($action=="getZhy")
{
$zhyList=$teacherControl->getZhy($fun);
$content = "&nbsp;选择专业：
<div style=\"clear:both; \">
</div>
<select multiple=\"multiple\" size=\"4\"  onchange=\"getBJ(this)\" style=\"border:0px; margin:0px; padding:0px; color:#666;\" >";
for($i=0; $i<count($zhyList); $i++)
{
	$content=$content."<option value=".$zhyList[$i]->getZhy_id().">".$zhyList[$i]->getZhy_name()."</option>";
}
$content=$content."</select>";
echo iconv ( "gbk", "utf-8", $content);
}
else if($action=="getBj")
{
	$bjList = $teacherControl->getBj($fun);
	$content = "&nbsp;选择班级：
<div style=\"clear:both; \">
</div>
<select multiple=\"multiple\" size=\"4\" id=\"bj[]\" name=\"bj[]\"  style=\"border:0px; margin:0px; padding:0px; color:#666;\" >";
for($i=0; $i<count($bjList); $i++)
{
	$content=$content."<option value=".$bjList[$i]->getBj_id().">[".$bjList[$i]->getZhy()->getZhy_name()."]".$bjList[$i]->getBj_name()."</option>";
}
$content=$content."</select>";
echo iconv ( "gbk", "utf-8", $content);
}
else if($action=="addHW")
{
	$hwControl->addHW($fun);
}
else if($action=="hwList")
{
	$hwControl->hwList($smarty, $fun);
}
else if($action =="deleteHW")
{
	$hwControl->deleteHW($fun);
}
else if($action =="searchHW")
{
	$hwControl->searchHW($smarty, $fun);
}
else if($action=="updateHW")
{
	$hwControl->updateHW($fun);
}
else if($action == "getUndoHW")
{
	$smarty->assign("action",$action);
	$teacherControl->getHW($smarty, $fun,1);
}
else if($action == 'getDoneHW' )
{
	$smarty->assign("action",$action);
	$teacherControl->getHW($smarty, $fun,2);
}
else if($action == 'getReDoHW')
{
	$smarty->assign("action",$action);
	$teacherControl->getHW($smarty, $fun,3);
}
else if($action =='getZhyAjax')
{
	$zhyService = new ZhuanYeService();
	$zhyList =  $zhyService->getAllListById($_GET['xyId']);
	$fun->closeDB();
	$content="<select name=\"zhyId\" id=\"zhyId\" style=\"width:140px; height:20px; color:#09C; border:#999 1px solid;\" onchange=\"getBj()\"><option value=\"none\">设定搜索的专业</option>";
	for($i=0; $i<count($zhyList); $i++)
	{
		$content = $content."<option value=".$zhyList[$i]->getZhy_id().">".$zhyList[$i]->getZhy_name()."</option>";
	}
	$content=$content."</select>";
	echo iconv ( "gbk", "utf-8", $content);
}else if($action =='getBjAjax')
{
	$bjService = new BanJiService();
	$bjList = $bjService->getBanJiByZhy($_GET['zhyId']);
	$fun->closeDB();
	$content="<select name=\"bjId\" id=\"bjId\" style=\"width:140px; height:20px; color:#09C; border:#999 1px solid;\" onchange=\"getXs()\"><option value=\"none\">设定搜索的班级</option>";
	for($i=0; $i<count($bjList); $i++)
	{
		$content = $content."<option value=".$bjList[$i]->getBj_id().">".$bjList[$i]->getBj_name()."</option>";
	}
	$content=$content."</select>";
	echo iconv ( "gbk", "utf-8", $content);
}
else if($action =='getXsAjax')
{
	$xsService = new XueShengService();
	$xsList = $xsService->getAllListById(0,$_GET['bjId']);
	$fun->closeDB();
	$content="<select name=\"xsId\" id=\"xsId\" style=\"width:140px; height:20px; color:#09C; border:#999 1px solid;\" ><option value=\"none\">设定搜索的学生</option>";
	for($i=0; $i<count($xsList); $i++)
	{
		$content = $content."<option value=".$xsList[$i]->getXs_id().">".$xsList[$i]->getXs_name()."</option>";
	}
	$content=$content."</select>";
	echo iconv ( "gbk", "utf-8", $content);
}
else if($action == 'confirmZY')
{
	$zyId=$_POST['zyId'];
	$score = $_POST['score'];
	$pyState = $_POST['pyState'];
	$py = "none";
	if($pyState==1)
	{
		$py = $_POST['py'];
	}
	$zyService = new ZuoYeService();
	$zy = $zyService->getZuoYeById($zyId);
	$zy->setZy_py($py);
	$zy->setZy_sate(1);
	$zy->setZy_fs($score);
	if($zyService->updateZuoYe($zy))
	{
		$system = unserialize($_SESSION['system']);
	$emailContent = $zy->getXs()->getXs_name()."同学：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;在" . $system->getSiteName () . "！你的作业".$zy->getZy_name()."已通过".$zy->getLs()->getLs_name()."老师的审核！请注意查收！点击下面地址进入系统，
				如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
	$fun->sendMail($zy->getXs()->getXs_email(), $emailContent, "作业审核通过通知",$system->getSiteEmail (), $system->getSiteEmailPassword ());
		$fun->addLog("批改了作业<".$zy->getZy_name().">");
		$fun->closeDB();
		$fun->alertMessage("提交成功！", "controlLs.php?action=getUndoHW");
	}
	else
	{
		$fun->addLog("批改了作业<".$zy->getZy_name().">提交失败");
		$fun->closeDB();
		$fun->alertMessage("提交失败！", "../confirmZY.php?zyId='".$zyId."'");
	}
}
else if($action == 'reDoHW')
{
    $zyId=$_GET['zyId'];
	$zyService = new ZuoYeService();
	$zy = $zyService->getZuoYeById($zyId);
	$score = 0;
	if($_POST['score'])
	{
	$score = $_POST['score'];
	}
	$pyState = $_POST['pyState'];
	$py = "none";
	if($pyState==1)
	{
		$py = $_POST['py'];
	}
	$zyService = new ZuoYeService();
	$zy = $zyService->getZuoYeById($zyId);
	$zy->setZy_py($py);
	$zy->setZy_fs($score);
	$zy->setZy_sate(2);
	if($zyService->updateZuoYe($zy))
	{
		$system = unserialize($_SESSION['system']);
	$emailContent = $zy->getXs()->getXs_name()."同学：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;在" . $system->getSiteName () . "！你的作业".$zy->getZy_name()."被".$zy->getLs()->getLs_name()."老师打回！需要重做！请注意查收！点击下面地址进入系统，
				如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
	$fun->sendMail($zy->getXs()->getXs_email(), $emailContent, "作业退回通知",$system->getSiteEmail (), $system->getSiteEmailPassword ());
		$fun->addLog("批改了作业<".$zy->getZy_name().">");
		$fun->closeDB();
		$fun->alertMessage("提交成功！", "controlLs.php?action=getUndoHW");
	}
	else
	{
		$fun->addLog("批改了作业<".$zy->getZy_name().">提交失败");
		$fun->closeDB();
		$fun->alertMessage("提交失败！", "../confirmZY.php?zyId='".$zyId."'");
	}
}
else if($action=='needRecive')
{
	$smarty->assign("action",$action);
	$teacherControl->needRecive($smarty, $fun);
}
else if($action == 'addHF')
{
$teacherControl->addHF($fun);	
}
?>