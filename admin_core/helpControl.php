<?php
session_start ();
include_once 'config.php';
include_once ROOT . '/utils/Function.php';
include_once ROOT . '/utils/email.class.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/XueYuan.php';
include_once ROOT . '/models/BanJi.php';
include_once ROOT . '/models/System.php';
include_once ROOT . '/models/LaoShi.php';
include_once ROOT . '/models/XueSheng.php';
include_once ROOT . '/services/XueYuanService.php';
include_once ROOT . '/services/ZhuanYeService.php';
include_once ROOT . '/services/BanJiService.php';
include_once ROOT . '/services/SystemService.php';
include_once ROOT . '/services/LaoShiService.php';
include_once ROOT . '/services/XueShengService.php';
$action = $_GET['action'];
$fun = new fun();
if($action=='findPWD')
{
	$type = $_POST['userType'];
	
	$system = unserialize($_SESSION['system']);
	$code = $_SESSION['validate'];
	$validata = $_POST['validata'];
	if($type==1)
	{
		
		
		$bj = $_POST['bj'];
		$xh = $_POST['xh'];
		if($validata != $code)
		{
			$fun->alertMessage("验证码错误！", "../findPWD.php");
		}
		else {
			$xsService = new XueShengService();
			$xs_id=$xsService->getXsForFindPWD($bj, $xh);
			if($xs_id==0)
			{
				$fun->closeDB();
				$fun->alertMessage("该用户不存在！请确认你的信息...", "../findPWD.php");
			}
			else {
				$str = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
				$valueLen = rand(10, count($str)-1);
				for($i=0; $i<$valueLen;$i++)
				{
				$value .= $str [rand ( 0, count($str)-1 )];
				}
				$value =mktime().$value;
			    $sql = "insert into findpwd_table (find_key,user_id,user_type) values('$value','$xs_id','student')"; 	
			    if(mysql_query($sql))
			    {
			    	$xs = $xsService->getXueShengById($xs_id);
			    	$fun->closeDB();
			    	$content = $xs->getXs_name()."同学：<br />&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;您好：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				你的验证码为：" . $value. "！你可以点击下面的地址进入：<a href=" . $system->getSiteUrl () . "/resetPWD.php?key=$value>" . $system->getSiteUrl () . "/resetPWD.php?key=$value" . "</a>";
			    	$fun->sendMail($xs->getXs_email(), $content, "找回密码验证",  $system->getSiteEmail (), $system->getSiteEmailPassword ());
			    	$fun->alertRegistMessage("../findResult.html");
			    }
			    else
			    {
			    	$fun->closeDB();
                 $fun->alertMessage("系统出现错误！请与管理员联系...", "../findPWD.php");
			    }
			}
		}
	    
	}
	else
	{
		$rname = $_POST['rname'];
		$tele = $_POST['tele'];
		$xy = $_POST['xyLs'];
			if($validata != $code)
		{
			$fun->alertMessage("验证码错误！", "../findPWD.php");
		}
		else {
			$lsService = new LaoShiService();
			$ls_id=$lsService->getLsForFindPWD($rname, $tele, $xy);
			if($ls_id==0)
			{
				$fun->closeDB();
				$fun->alertMessage("该用户不存在！请确认你的信息...", "../findPWD.php");
			}
			else {
				$str = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
				$valueLen = rand(10, count($str)-1);
				for($i=0; $i<$valueLen;$i++)
				{
				$value .= $str [rand ( 0, count($str)-1 )];
				}
				$value =mktime().$value;
			    $sql = "insert into findpwd_table (find_key,user_id,user_type) values('$value','$ls_id','teacher')"; 	
			    if(mysql_query($sql))
			    {
			    	$ls = $lsService->getLaoShiById($ls_id);
			    	$fun->closeDB();
			    	$content = $ls->getLs_name()."老师：<br />&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;您好：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				你的验证码为：" . $value. "！你可以点击下面的地址进入：<a href=" . $system->getSiteUrl () . "/resetPWD.php?key=$value>" . $system->getSiteUrl () . "/resetPWD.php?key=$value" . "</a>";
			    	$fun->sendMail($ls->getLs_email(), $content, "找回密码验证",  $system->getSiteEmail (), $system->getSiteEmailPassword ());
			    	$fun->alertRegistMessage("../findResult.html");
			    }
			    else
			    {
			     $fun->closeDB();
                 $fun->alertMessage("系统出现错误！请与管理员联系...", "../findPWD.php");
			    }
			}
		}
	}
}
else if($action == 'resetPWD')
{
	$key = $_POST['key'];
	$pwd = $_POST['pwd'];
	$pwd = $fun->StringToMD5($pwd);
	$sql = "select user_id,user_type from findpwd_table where find_key='$key'";
	
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);
	if($row)
	{
		$user_id = $row['user_id'];
		$user_type = $row['user_type'];
	if($user_type=="teacher")
	{
		$sql = "update ls_table set ls_loginpsw='$pwd' where ls_id='$user_id'";
		if(mysql_query($sql))
		{
			$sql = "delete from findpwd_table where find_key='$key'";
			if(mysql_query($sql))
			{
				$fun->alertMessage("密码修改成功！可进行登录！", "../index.php");
			}
			else {
				$fun->alertMessage("密码修改失败！请和管理员联系！", "../index.php");
			}
		}
	}
	else
	{
	   $sql = "update xs_table set xs_loginpwd='$pwd' where xs_id='$user_id'";
		if(mysql_query($sql))
		{
			$sql = "delete from findpwd_table where find_key='$key'";
		if(mysql_query($sql))
			{
				$fun->alertMessage("密码修改成功！可进行登录！", "../index.php");
			}
			else{
				$fun->alertMessage("密码修改失败！请和管理员联系！", "../index.php");
			}
		}
	}
	}else
	{
		$fun->alertMessage("非法操作！", "../index.php");
	}
	
}
?>