<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/Log.php';
/**
 * 
 * Enter description 该类是对日志实体进行的各种操作
 * @author wuzhiwei
 *
 */
class LogService {
	/**
	 * 
	 * Enter description 添加日志到数据库中
	 * @param Log $log
	 */
	public function addLog(Log $log) {
		$sql = "insert into log_table (log_content,log_people,log_date,log_ip) values('" . $log->getLog_content () . "','" . $log->getLog_people () . "','" . $log->getLog_date () . "','" . $log->getLog_ip () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description获取总的记录数
	 */
	public function getLabelCount() {
		$sql = "select count(*) from log_table";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getLabelCountByDate($before, $after) {
		$sql = "select count(*) from log_table where log_date>'$before' and log_date <'$after'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	/**
	 * 
	 * Enter description 获得所有的日志
	 */
	public function getAllList() {
		$sql = "select * from log_table order by log_id desc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			date_default_timezone_set ( "Asia/Shanghai" );
			$log = new Log ();
			$log->setLog_content ( $row ['log_content'] );
			$log->setLog_people ( $row ['log_people'] );
			$log->setLog_date ( date ( "Y-m-d H:i:s", ($row ['log_date']) ) );
			$log->setLog_id ( $row ['log_id'] );
			$log->setLog_ip ( $row ['log_ip'] );
			$logList [] = $log;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $logList ) > 0) {
			return $logList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 获得部分的日志
	 */
	public function getListForPage($start, $end) {
		$sql = "select * from log_table order by log_id desc limit $start,$end";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			date_default_timezone_set ( "Asia/Shanghai" );
			$log = new Log ();
			$log->setLog_content ( $row ['log_content'] );
			$log->setLog_people ( $row ['log_people'] );
			$log->setLog_date ( date ( "Y-m-d H:i:s", ($row ['log_date']) ) );
			$log->setLog_id ( $row ['log_id'] );
			$log->setLog_ip ( $row ['log_ip'] );
			$logList [] = $log;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $logList ) > 0) {
			return $logList;
		} else {
			return null;
		}
	}
	public function getAllByDate($before, $after) {
		$sql = "select * from log_table where log_date>'$before' and log_date <'$after' order by log_id desc";
		
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			date_default_timezone_set ( "Asia/Shanghai" );
			$log = new Log ();
			$log->setLog_content ( $row ['log_content'] );
			$log->setLog_people ( $row ['log_people'] );
			$log->setLog_date ( date ( "Y-m-d H:i:s", ($row ['log_date']) ) );
			$log->setLog_id ( $row ['log_id'] );
			$log->setLog_ip ( $row ['log_ip'] );
			$logList [] = $log;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $logList ) > 0) {
			return $logList;
		} else {
			return null;
		}
	}
	public function getListForPageByDate($start, $end, $before, $after) {
		$sql = "select * from log_table where log_date>'$before' and log_date <'$after' order by log_id desc limit $start,$end";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			date_default_timezone_set ( "Asia/Shanghai" );
			$log = new Log ();
			$log->setLog_content ( $row ['log_content'] );
			$log->setLog_people ( $row ['log_people'] );
			$log->setLog_date ( date ( "Y-m-d H:i:s", ($row ['log_date']) ) );
			$log->setLog_id ( $row ['log_id'] );
			$log->setLog_ip ( $row ['log_ip'] );
			$logList [] = $log;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $logList ) > 0) {
			return $logList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据日志的唯一编号删除日志
	 * @param unknown_type $id
	 */
	public function deleteLogById($id) {
		$sql = "delete from log_table where log_id='" . $id . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的日志唯一标识码的数组进行批量删除日志
	 * @param array $idList
	 */
	public function deleteLogBetch($idList) {
		$sql = "delete from log_table where log_id='";
		$count = 0;
		foreach ( $idList as $id ) {
			$sql = $sql . $id . "'";
			mysql_query ( $sql );
			$count ++;
			$sql = "delete from log_table where log_id='";
		}
		if ($count != count ( $idList )) {
			return false;
		} else {
			return true;
		}
	}
}
?>