<?php
include_once 'WenTiService.php';
include_once 'XueShengService.php';
include_once 'BanJiService.php';
class WT2XSRelationService {
	
	public function addRelation($wtId, $xsList, $type) {
		$count = 0;
		for($i = 0; $i < count ( $xsList ); $i ++) {
			$sql = "insert into wt_xs_table (wt_id,xs_id,state,type,bj_id,fs) values('" . $wtId . "','" . $xsList [$i]->getXs_id () . "','0','$type','". $xsList[$i]->getBj()->getBj_id()."','0')";
			$result = mysql_query ( $sql );
			if ($result) {
				$count ++;
			}
		}
		if ($count == count ( $xsList )) {
			return true;
		} else {
			return false;
		}
	}
   public function addRelationForOne($wtId, $xsId, $type,$bjId) {
			$sql = "insert into wt_xs_table (wt_id,xs_id,state,type,bj_id,fs) values('" . $wtId . "','" .$xsId . "','0','$type','". $bjId."','0')";
			$result = mysql_query ( $sql );
			
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function getRelationListByWT($wtId,$obj=null) {
		$sql = "select xs_table.xs_id,xs_table.bj_id,wt_xs_table.state,wt_xs_table.wt_id,wt_xs_table.type,wt_xs_table.fs from xs_table join wt_xs_table on xs_table.xs_id = wt_xs_table.xs_id where wt_xs_table.wt_id='$wtId'  order by fs desc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$wtService = new WenTiService ();
		$xsService = new XueShengService ();
		$bjService = new BanJiService();
		while ( $row ) {
			$wxr = new WT2XSRelation ();
			$wxr->setState ( $row ['state'] );
			if($obj!=null)
			{
			$wxr->setWt ( $obj );
			}
			else 
			{
				$wxr->setWt($wtService->getWenTiById($wtId));
			}
			$wxr->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$wxr->setType ( $row ['type'] );
			$wxr->setBj($bjService->getBanJiById($row['bj_id']));
			$wxr->setFs($row['fs']);
			$wxrList [] = $wxr;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wxrList ) > 0) {
			return $wxrList;
		} else {
			return null;
		}
	}
	//获得一个班的所有学生提交作业的信息
	public function getRelationListByBJandWT($wtId,$bjId,$obj=null)
	{
	$sql = "select xs_table.xs_id,xs_table.bj_id,wt_xs_table.state,wt_xs_table.wt_id,wt_xs_table.type,wt_xs_table.fs from xs_table join wt_xs_table on xs_table.xs_id = wt_xs_table.xs_id where wt_xs_table.wt_id='$wtId' and xs_table.bj_id='$bjId' order by wt_xs_table.fs desc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$wtService = new WenTiService ();
		$xsService = new XueShengService ();
		$bjService = new BanJiService();
		while ( $row ) {
			$wxr = new WT2XSRelation ();
			$wxr->setState ( $row ['state'] );
			if($obj!=null)
			{
			$wxr->setWt ( $obj );
			}
			else 
			{
			$wxr->setWt ( $wtService->getWenTiById ( $row ['wt_id'],$bjId ) );
			}
			$wxr->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$wxr->setType ( $row ['type'] );
			$wxr->setBj($bjService->getBanJiById($row['bj_id']));
			$wxr->setFs($row['fs']);
			$wxrList [] = $wxr;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wxrList ) > 0) {
			return $wxrList;
		} else {
			return null;
		}
	}
	public function updateRelation(WT2XSRelation $wxr) {
		$sql = "update wt_xs_table set state='" . $wxr->getState () . "',fs='".$wxr->getFs()."' where wt_id='" . $wxr->getWt ()->getWt_id () . "' and xs_id='" . $wxr->getXs ()->getXs_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function getRelation($wtId, $xsId) {
		$sql = "select * from wt_xs_table where wt_id='$wtId' and xs_id='$xsId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		
		if ($row) {
			$bjService = new BanJiService();
			$wtService = new WenTiService ();
			$xsService = new XueShengService ();
			$wxr = new WT2XSRelation ();
			$wxr->setState ( $row ['state'] );
			$wxr->setWt ( $wtService->getWenTiById ( $row ['wt_id'] ) );
			$wxr->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$wxr->setBj($bjService->getBanJiById($row['bj_id']));
			$wxr->setType ( $row ['type'] );
			$wxr->setFs($row['fs']);
			return $wxr;
		} else {
			return null;
		}
	}
	public function updateRelationBatch($wtId, $xsList, $type) {
	
			if ($this->addRelation ( $wtId, $xsList, $type )) {
				return true;
			} else {
				return false;
			}
		
	}
	public function deleteByXs($xsId)
	{
		$sql = "delete from wt_xs_table where xs_id='".$xsId."'";
		$result = mysql_query($sql);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * 
	 * 方法名：getRelationsByXS
	 * 说明：该方法是获得一名学生提交作业的情况
	 * @param unknown_type $xsId
	 */
	public function geteRelationsForXS($xsId,$type,$state) {
		$sql = "select count(*) from wt_xs_table where xs_id='$xsId' and type='$type' and state='$state'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getAVGForXs($xsId,$type)
	{
		$sql = "select avg(fs) from wt_xs_table where xs_id='$xsId' and type='$type' and state='1'";
	    $result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getMAXForXs($xsId,$type)
	{
	    $sql = "select max(fs) from wt_xs_table where xs_id='$xsId' and type='$type' and state='1'";
	    $result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
    public function getMINForXs($xsId,$type)
	{
	    $sql = "select min(fs) from wt_xs_table where xs_id='$xsId' and type='$type' and state='1'";
	    $result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	
}
?>