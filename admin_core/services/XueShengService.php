<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/XueSheng.php';
include_once ROOT . '/models/ZhuanYe.php';
include_once 'LiuYanService.php';
include_once 'WT2XSRelationService.php';
include_once 'ZuoYeService.php';
/**
 * 
 * Enter description 该类是对老师实体进行的各种操作
 * @author wuzhiwei
 *
 */
class XueShengService {
	
	public function checkXSLogin(XueSheng $xs)
	{
		$sql = "select * from xs_table where xs_loginname='".$xs->getXs_loginname()."' and xs_loginpwd='".$xs->getXs_loginpwd()."'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$bjService = new BanJiService ();
		if($row)
		{
			$xs->setXs_id ( $row ['xs_id'] );
			$xs->setXs_name ( $row ['xs_name'] );
			$xs->setXs_email ( $row ['xs_email'] );
			$bj = $bjService->getBanJiById ( $row ['bj_id'] );
			$xs->setZhy ( $bj->getZhy() );
			$xs->setXs_tele ( $row ['xs_tele'] );
			$xs->setXs_tx ( $row ['xs_tx'] );
			$xs->setBj ( $bj );
			$xs->setXy ( $bj->getZhy()->getXy());
			$xs->setXs_xb ( $row ['xs_xb'] );
			$xs->setXs_loginname ( $row ['xs_loginname'] );
			$xs->setXs_loginpwd ( $row ['xs_loginpwd'] );
			$xs->setXs_xh ( $row ['xs_xh'] );
			return $xs;
		}
		else
		{
			return null;
		}
		
	}
	/**
	 * 
	 * Enter description 根据老师对象将该老师添加到数据库中
	 * @param LaoShi $ls
	 * @return boolean
	 */
	public function addXueSheng(XueSheng $xs) {
		$sql = "insert into xs_table(xs_name,xs_email,zhy_id,xs_tele,xs_tx,bj_id,xy_id,xs_xb,xs_loginname,xs_loginpwd,xs_xh) 
		values('" . $xs->getXs_name () . "','" . $xs->getXs_email () . "','" . $xs->getZhy ()->getZhy_id () . "','" . $xs->getXs_tele () . "','" . $xs->getXs_tx () . "','" . $xs->getBj ()->getBj_id () . "','" . $xs->getXy ()->getXy_id () . "','" . $xs->getXs_xb () . "','" . $xs->getXs_loginname () . "','" . $xs->getXs_loginpwd () . "','" . $xs->getXs_xh () . "')";
		$result = mysql_query ( $sql );
	  	if ($result) {
	  		$id = mysql_insert_id ();
		    $wtService = new WenTiService();
			$wtList = $wtService->getWTByBj($xs->getBj()->getBj_id());
			$wxrService = new WT2XSRelationService();
			for($i=0; $i<count($wtList); $i++)
			{
				if(!$wxrService->addRelationForOne($wtList[$i]->getWt_id(), $id, $wtList[$i]->getWt_type(), $xs->getBj()->getBj_id()))
				{
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的老师唯一编号获得对应的老师对象
	 * @param unknown_type $id
	 */
	public function getXueShengById($id) {
		$sql = "select * from xs_table where xs_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$bjService = new BanJiService ();
		if ($row) {
			$xs = new XueSheng ();
			$xs->setXs_id ( $row ['xs_id'] );
			$xs->setXs_name ( $row ['xs_name'] );
			$xs->setXs_email ( $row ['xs_email'] );
			$bj = $bjService->getBanJiById ( $row ['bj_id'] );
			$xs->setZhy ( $bj->getZhy() );
			$xs->setXs_tele ( $row ['xs_tele'] );
			$xs->setXs_tx ( $row ['xs_tx'] );
			$xs->setBj ( $bj );
			$xs->setXy ( $bj->getZhy()->getXy());
			$xs->setXs_xb ( $row ['xs_xb'] );
			$xs->setXs_loginname ( $row ['xs_loginname'] );
			$xs->setXs_loginpwd ( $row ['xs_loginpwd'] );
			$xs->setXs_xh ( $row ['xs_xh'] );
			return $xs;
		} else {
			return null;
		}
	}
	public function getAllListById($zhyId = 0, $bjId = 0, $xyId = 0,$xsName = null) {
		if($xsName == null)
		{
		if ($bjId != 0) {
			$sql = "select * from xs_table where bj_id='" . $bjId . "' order by xs_xh asc" ;
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select * from xs_table where zhy_id='" . $zhyId . "'";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select * from xs_table where xy_id='" . $xyId . "' ";
		} else if ($xyId == 0) {
			$sql = "select * from xs_table";
		}
		}
		else
		{
		if ($bjId != 0) {
			$sql = "select * from xs_table where bj_id='" . $bjId . "' and xs_name like '%$xsName%' order by xs_xh desc";
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select * from xs_table where zhy_id='" . $zhyId . "' and xs_name like '%$xsName%'";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select * from xs_table where xy_id='" . $xyId . "' and xs_name like '%$xsName%'";
		} else if ($xyId == 0) {
			$sql = "select * from xs_table where xs_name like '%$xsName%'";
		}
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$bjService = new BanJiService ();
		while ( $row ) {
			$xs = new XueSheng ();
			$xs->setXs_id ( $row ['xs_id'] );
			$xs->setXs_name ( $row ['xs_name'] );
			$xs->setXs_email ( $row ['xs_email'] );
			$bj = $bjService->getBanJiById ( $row ['bj_id'] );
			$xs->setZhy ( $bj->getZhy() );
			$xs->setXs_tele ( $row ['xs_tele'] );
			$xs->setXs_tx ( $row ['xs_tx'] );
			$xs->setBj ( $bj );
			$xs->setXy ( $bj->getZhy()->getXy());
			$xs->setXs_xb ( $row ['xs_xb'] );
			$xs->setXs_loginname ( $row ['xs_loginname'] );
			$xs->setXs_loginpwd ( $row ['xs_loginpwd'] );
			$xs->setXs_xh ( $row ['xs_xh'] );
			$xsList [] = $xs;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $xsList ) > 0) {
			return $xsList;
		} else {
			return null;
		}
	}
	public function getLabelCount($zhyId = 0, $bjId = 0, $xyId = 0,$xsName = null) {
		if($xsName == null)
		{
		if ($bjId != 0) {
			$sql = "select count(*) from xs_table where bj_id='" . $bjId . "'";
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select count(*) from xs_table where zhy_id='" . $zhyId . "'";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select count(*)from xs_table where xy_id='" . $xyId . "'";
		} else if ($xyId == 0) {
			$sql = "select count(*)from xs_table";
		}
		}
		else
		{
		if ($bjId != 0) {
			$sql = "select count(*) from xs_table where bj_id='" . $bjId . "' and xs_name like '%$xsName%'";
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select count(*) from xs_table where zhy_id='" . $zhyId . "' and xs_name like '%$xsName%'";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select count(*)from xs_table where xy_id='" . $xyId . "' and xs_name like '%$xsName%'";
		} else if ($xyId == 0) {
			$sql = "select count(*)from  xs_table where xs_name like '%$xsName%'";
		}
		}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	public function getListForPageById($start, $end, $zhyId = 0, $bjId = 0, $xyId = 0,$xsName = null) {
		if($xsName == null)
		{
		if ($bjId != 0) {
			$sql = "select * from xs_table where bj_id='" . $bjId . "' order by xs_id desc limit $start,$end";
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select * from xs_table where zhy_id='" . $zhyId . "' order by xs_id desc limit $start,$end";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select * from xs_table where xy_id='" . $xyId . "' order by xs_id desc limit $start,$end";
		} else if ($xyId == 0) {
			
			$sql = "select * from xs_table order by xs_id desc limit $start,$end";
		}
		}
		else {
		if ($bjId != 0) {
			$sql = "select * from xs_table where bj_id='" . $bjId . "' and xs_name like '%$xsName%' order by xs_id desc limit $start,$end";
		} else if ($zhyId != 0 && $bjId == 0) {
			$sql = "select * from xs_table where zhy_id='" . $zhyId . "' and xs_name like '%$xsName%' order by xs_id desc limit $start,$end";
		} else if ($xyId != 0 && $bjId == 0 && $zhyId == 0) {
			$sql = "select * from xs_table where xy_id='" . $xyId . "' and xs_name like '%$xsName%' order by xs_id desc limit $start,$end";
		} else if ($xyId == 0) {
			$sql = "select * from xs_table where xs_name like '%$xsName%' order by xs_id desc limit $start,$end";
		}
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
	$bjService = new BanJiService ();
		while ( $row ) {
			$xs = new XueSheng ();
			$xs->setXs_id ( $row ['xs_id'] );
			$xs->setXs_name ( $row ['xs_name'] );
			$xs->setXs_email ( $row ['xs_email'] );
			$bj = $bjService->getBanJiById ( $row ['bj_id'] );
			$xs->setZhy ( $bj->getZhy() );
			$xs->setXs_tele ( $row ['xs_tele'] );
			$xs->setXs_tx ( $row ['xs_tx'] );
			$xs->setBj ( $bj );
			$xs->setXy ( $bj->getZhy()->getXy());
			$xs->setXs_xb ( $row ['xs_xb'] );
			$xs->setXs_loginname ( $row ['xs_loginname'] );
			$xs->setXs_loginpwd ( $row ['xs_loginpwd'] );
			$xs->setXs_xh ( $row ['xs_xh'] );
			$xsList [] = $xs;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $xsList ) > 0) {
			return $xsList;
		} else {
			return null;
		}
	}
	
	public function deleteXueShengBatch($idList) {
		$count = 0;
		$lyService = new LiuYanServie();
		$wxr = new WT2XSRelationService();
		$zyService =new ZuoYeService();
		for($i=0; $i<count($idList);$i++)
		{
		if($lyService->deleteLYByXs($idList[$i])&&$wxr->deleteByXs($idList[$i])&&$zyService->deleteZuoYeByXs($idList[$i]))
		{
		$sql = "delete from xs_table where xs_id='" . $idList[$i] . "'";
		$result = mysql_query ( $sql );
		$count++;
		}else
		{
			return false;
		}
		}
		if ($count == count($idList)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据学院的唯一编号删除在该学院下的所有老师
	 * @param unknown_type $xyId
	 */
	public function deleteXueShengByXy($xyId) {
		
		$sql = "select * from xs_table where xy_id='".$xyId."'";
	    
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$lyService = new LiuYanServie();
		$wxr = new WT2XSRelationService();
		$zyService =new ZuoYeService();
		while($row)
		{
			if(!$lyService->deleteLYByXs($row['xs_id'])||!$wxr->deleteByXs($row['xs_id'])||!$zyService->deleteZuoYeByXs($row['xs_id']))
			{
				return false;
			}
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from xs_table where xy_id='" . $xyId . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 
	 * @param LaoShi $ls
	 */
	public function deleteXueShengByBj($bjId) {
	$sql = "select * from xs_table where bj_id='".$bjId."'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$lyService = new LiuYanServie();
		$wxr = new WT2XSRelationService();
		$zyService =new ZuoYeService();
		while($row)
		{
			if(!$lyService->deleteLYByXs($row['xs_id'])||!$wxr->deleteByXs($row['xs_id'])||!$zyService->deleteZuoYeByXs($row['xs_id']))
			{
				return false;
			}
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from xs_table where bj_id='" . $bjId . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function deleteXueShengByZhy($zhyId) {
		$sql = "select * from xs_table where zhy_id='".$zhyId."'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$lyService = new LiuYanServie();
		$wxr = new WT2XSRelationService();
		$zyService =new ZuoYeService();
		while($row)
		{
			if(!$lyService->deleteLYByXs($row['xs_id'])||!$wxr->deleteByXs($row['xs_id'])||!$zyService->deleteZuoYeByXs($row['xs_id']))
			{
				return false;
			}
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from xs_table where zhy_id='" . $zhyId . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function updateXueSheng(XueSheng $xs) {
		$sql = "update xs_table set xs_name='" . $xs->getXs_name () . "',xs_email='" . $xs->getXs_email() . "',xs_tele='" . $xs->getXs_tele () . "', xy_id='" . $xs->getXy ()->getXy_id () . "',xs_tx='" . $xs->getXs_tx () . "',zhy_id='" . $xs->getZhy ()->getZhy_id () . "',bj_id='" . $xs->getBj ()->getBj_id () . "', xs_xh='" . $xs->getXs_xh () . "',xs_loginname='" . $xs->getXs_loginname () . "',xs_loginpwd='" . $xs->getXs_loginpwd () . "',xs_xb='" . $xs->getXs_xb () . "' where xs_id='" . $xs->getXs_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function checkXueSheng($name) {
		$sql = "select * from xs_table where xs_loginname='" . $name . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}
	
	public function checkXSByXH($xy, $zhy, $bj, $xh) {
		$sql = "select * from xs_table where xy_id='$xy' and zhy_id='$zhy' and bj_id='$bj' and xs_xh='$xh'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}

    public function getXsForFindPWD($bj_id,$xh)
    {
    	$sql = "select xs_id from xs_table where bj_id='$bj_id' and xs_xh='$xh'";
    	$result =  mysql_query($sql);
    	$row = mysql_fetch_array($result);
    	if($row)
    	{
    		return $row[0];
    	}
    	else
    	{
    		return 0;
    	}
    }
}
?>