<?php
/**
 * description 该类是对管理员实体的操作进行封装
 * @author wuzhiwei
 *
 */
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/Admin.php';
include_once ROOT . '/utils/Function.php';

class AdminService {
	/**
	 *检测是否存在该管理员
	 * 
	 */
	public function checkAdmin(Admin $admin) {
		$func = new fun ();
		$query = mysql_query ( "select * from admin where admin_name='" . $admin->getAdmin_name () . "' and admin_password ='" . $func->StringToMD5 ( $admin->getAdmin_password () ) . "'" ) or die ( "鏌ヨ澶辫触.....<br />" );
		$row = mysql_fetch_array ( $query );
		$admin_return = new Admin ();
		if ($row) {
			$admin_return->setAdmin_id ( $row ["admin_id"] );
			$admin_return->setAdmin_name ( $row ["admin_name"] );
			$admin_return->setAdmin_password ( $row ["admin_password"] );
			$admin_return->setAdmin_purview ( $row ["admin_purview"] );
			$admin_return->setAdmin_email ( $row ["admin_email"] );
			$admin_return->setLast_login ( $func->getDate () );
			$admin_return->setLogin_ip ( $func->getClientIp () );
			return $admin_return;
		} else {
			return null;
		}
	
	}
	/**
	 * 该方法是检测该管理员名是否存在
	 */
	public function checkAdminByName($name) {
		
		$sql = "select * from admin where admin_name='" . $name . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 添加管理员
	 */
	public function addAdmin(Admin $admin) {
		$func = new fun ();
		
		$sql = "insert into admin (admin_name,admin_purview,admin_email,last_login,login_ip,admin_password) values('" . $admin->getAdmin_name () . "','" . $admin->getAdmin_purview () . "','" . $admin->getAdmin_email () . "','" . $func->getDate () . "','" . $func->getClientIp () . "','" . $func->StringToMD5 ( $admin->getAdmin_password () ) . "')";
		$query = mysql_query ( $sql );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据管理员唯一编号删除管理员
	 * @param unknown_type $admin_id
	 */
	public function deletAdmin($admin_id) {
		
		$sql = "delete from admin where admin_id='" . $admin_id . "'";
		$query = mysql_query ( $sql );
		if ($query) {
			return true;
		} else {
			return false;
		}
	
	}
	/**
	 * 
	 * Enter description 根据管理员编号获得该管理员
	 * @param unknown_type $admin_id
	 */
	public function getAdminById($admin_id) {
		$admin = new Admin ();
		$sql = "select * from admin where admin_id='" . $admin_id . "'";
		$query = mysql_query ( $sql );
		$row = mysql_fetch_array ( $query );
		if ($row) {
			$admin->setAdmin_id ( $row ["admin_id"] );
			$admin->setAdmin_name ( $row ["admin_name"] );
			$admin->setAdmin_password ( $row ["admin_password"] );
			$admin->setAdmin_purview ( $row ["admin_purview"] );
			$admin->setAdmin_email ( $row ["admin_email"] );
			$admin->setLast_login ( $row ["last_login"] );
			$admin->setLogin_ip ( $row ["login_ip"] );
		}
		return $admin;
	}
	/**
	 * 
	 * Enter description 获得所有的管理员 
	 */
	public function getAllAdmin() {
		
		$sql = "select * from admin";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			$admin = new Admin ();
			$admin->setAdmin_id ( $row ["admin_id"] );
			$admin->setAdmin_name ( $row ["admin_name"] );
			$admin->setAdmin_password ( $row ["admin_password"] );
			$admin->setAdmin_purview ( $row ["admin_purview"] );
			$admin->setAdmin_email ( $row ["admin_email"] );
			$admin->setLast_login ( $row ["last_login"] );
			$admin->setLogin_ip ( $row ["login_ip"] );
			$adminList [] = $admin;
			$row = mysql_fetch_array ( $result );
		}
		return $adminList;
	}
	/**
	 * 
	 * Enter description 更新该管理员
	 * @param Admin $admin
	 */
	public function updateAdmin(Admin $admin) {
		$fun = new fun ();
		$sql = "update admin set admin_name='" . $admin->getAdmin_name () . "',admin_purview='" . $admin->getAdmin_purview () . "',admin_email='" . $admin->getAdmin_email () . "',last_login='" . $admin->getLast_login () . "',login_ip='" . $admin->getLogin_ip () . "',admin_password='" . $admin->getAdmin_password () . "' where admin_id='" . $admin->getAdmin_id () . "'";
		$query = mysql_query ( $sql );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	
	
}
?>