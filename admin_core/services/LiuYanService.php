<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/LiuYan.php';
include_once 'LaoShiService.php';
include_once 'BanJiService.php';
include_once 'XueShengService.php';
class LiuYanServie {
	public function addLiuYan(LiuYan $ly) {
		$sql = "insert into ly_table (ly_title,ly_date,ly_content,xs_id,ls_id,ly_hf) values('" . $ly->getLy_title () . "','" . $ly->getLy_date () . "','" . $ly->getLy_content () . "','" . $ly->getXs ()->getXs_id () . "','" . $ly->getLs ()->getLs_id () . "','".$ly->getLy_hf()."')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function addHuiFu(LiuYan $ly) {
		$sql = "update ly_table set ly_hf='1',hf_content='" . $ly->getHf_content () . "',hf_date='" . $ly->getHf_date () . "' where ly_id='" . $ly->getLy_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function getLiuYanById($lyId) {
		$sql = "select * from ly_table where ly_id='$lyId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$xsService = new XueShengService ();
			$lsService = new LaoShiService();
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLs($lsService->getLaoShiById($row['ls_id']));
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			return $ly;
		} else {
			return null;
		}
	}
	public function getHuiFuById($lyId) {
		$sql = "select * from ly_table where ly_id='$lyId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$xsService = new XueShengService ();
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			$ly->setHf_content ( $row ['hf_content'] );
			$ly->setLy_hf($row['ly_hf']);
			$lsService = new LaoShiService ();
			$ly->setLs ( $lsService->getLaoShiById ( $row ['ls_id'] ) );
			$ly->setHf_date ( $row ['hf_date'] );
			return $ly;
		} else {
			return null;
		}
	}
	public function getAllListForLs($lsId, $state,$before=0,$after=0) {
		if($before==0&&$after==0)
		{
			$sql = "select * from ly_table where ls_id='$lsId' and ly_hf='$state' order by ly_date asc";
		}
		else
		{
			$sql = "select * from ly_table where ls_id='$lsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' order by ly_date asc";
		}
		
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xsService = new XueShengService ();
		$lsService = new LaoShiService ();
		while ( $row ) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			$ly->setHf_content ( $row ['hf_content'] );
			$ly->setLs ( $lsService->getLaoShiById ( $row ['ls_id'] ) );
			$ly->setHf_date ( $row ['hf_date'] );
			$lyList [] = $ly;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lyList ) > 0) {
			return $lyList;
		} else {
			return null;
		}
	}
	public function getAllListForLsByPage($lsId, $state, $start, $size,$before=0,$after=0) {
		if($before==0&&$after==0)
		{
			$sql = "select * from ly_table where ls_id='$lsId' and ly_hf='$state' order ly_date asc limit $start,$size";
			
		}
		else {
			$sql = "select * from ly_table where ls_id='$lsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' order ly_date desc limit $start,$size";
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xsService = new XueShengService ();
		$lsService = new LaoShiService ();
		while ( $row ) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			$ly->setHf_content ( $row ['hf_content'] );
			$ly->setLs ( $lsService->getLaoShiById ( $row ['ls_id'] ) );
			$ly->setHf_date ( $row ['hf_date'] );
			$lyList [] = $ly;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lyList ) > 0) {
			return $lyList;
		} else {
			return null;
		}
	}
	public function getCountForLs($lsId, $state,$before=0,$after=0) {
		if($before==0&&$after==0)
		{
			$sql = "select count(*) from ly_table where ls_id='$lsId' and ly_hf='$state' ";
		}
		else
		{
			$sql = "select count(*) from ly_table where ls_id='$lsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' ";
		}
			$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getAllListForXs($xsId, $state,$type=0,$before=0,$after=0) {
		if($type==0)
		{
		$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' order by hf_date desc";
		}
		else
		{
			if($type==1)
			{
				$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' order by ly_date desc";
			}
			else
			{
				$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' and hf_date>'$before' and hf_date<'$after' order by hf_date desc";
			}
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xsService = new XueShengService ();
		$lsService = new LaoShiService ();
		while ( $row ) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			$ly->setHf_content ( $row ['hf_content'] );
			$ly->setLs ( $lsService->getLaoShiById ( $row ['ls_id'] ) );
			$ly->setHf_date ( $row ['hf_date'] );
			$lyList [] = $ly;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lyList ) > 0) {
			return $lyList;
		} else {
			return null;
		}
	}
	public function getCountForXs($xsId, $state,$type=0,$before=0,$after=0) {
		if($type==0)
		{
		$sql = "select  count(*) from ly_table where xs_id='$xsId' and ly_hf='$state' ";
		}
		else
		{
			if($type==1)
			{
				$sql = "select count(*) from ly_table where xs_id='$xsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' ";
			}
			else
			{
				$sql = "select  count(*) from ly_table where xs_id='$xsId' and ly_hf='$state' and hf_date>'$before' and hf_date<'$after'";
			}
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getAllListForXsByPage($xsId, $state, $start, $size,$type=0,$before=0,$after=0) {
			if($type==0)
		{
		$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' order hf_date desc limit $start,$size";
		}
		else
		{
			if($type==1)
			{
				$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' and ly_date>'$before' and ly_date<'$after' order ly_date desc limit $start,$size";
			}
			else
			{
				$sql = "select * from ly_table where xs_id='$xsId' and ly_hf='$state' and hf_date>'$before' and hf_date<'$after' order hf_date desc limit $start,$size";
			}
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xsService = new XueShengService ();
		$lsService = new LaoShiService ();
		while ( $row ) {
			$ly = new LiuYan ();
			$ly->setLy_content ( $row ['ly_content'] );
			$ly->setLy_date ( $row ['ly_date'] );
			$ly->setXs ( $xsService->getXueShengById ( $row ['xs_id'] ) );
			$ly->setLy_id ( $row ['ly_id'] );
			$ly->setLy_title ( $row ['ly_title'] );
			$ly->setHf_content ( $row ['hf_content'] );
			$ly->setLs ( $lsService->getLaoShiById ( $row ['ls_id'] ) );
			$ly->setHf_date ( $row ['hf_date'] );
			$lyList [] = $ly;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lyList ) > 0) {
			return $lyList;
		} else {
			return null;
		}
	}
    public function deleteLYByXs($xsId)
    {
    	$sql="delete from ly_table where xs_id='".$xsId."'";
    	$result = mysql_query($sql);
    	if($result)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
   public function deleteLYByLs($lsId)
    {
    	$sql="delete from ly_table where ls_id='".$lsId."'";
    	$result = mysql_query($sql);
    	if($result)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
	public function deleteLYBatch($lyIdList)
	{
		$count=0;
		for($i=0; $i<count($lyIdList); $i++)
		{
			$sql = "delete from ly_table where ly_id='".$lyIdList[$i]."'";
			$result = mysql_query($sql);
			if($result)
			{
				$count++;
			}
		}
		if($count==count($lyIdList))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>