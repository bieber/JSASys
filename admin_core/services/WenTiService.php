<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/WenTi.php';
include_once 'LaoShiService.php';
include_once 'BanJiService.php';
include_once 'XueShengService.php';
include_once 'ZuoYeService.php';
class WenTiService {
	
	public function addWenTi(WenTi $wt) {
		$sql = "insert into wt_table (wt_name,wt_describe,ls_id,start_time,end_time,wt_type,xy_id) values('" . $wt->getWt_name () . "','" . $wt->getWt_describe () . "','" . $wt->getLs ()->getLs_id () . "','" . $wt->getStart_time () . "','" . $wt->getEnd_time () . "','" . $wt->getWt_type () . "','" . $wt->getXy ()->getXy_id () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			$id = mysql_insert_id ();
			$bjList = $wt->getBjList ();
			$count = 0;
			$xsService = new XueShengService ();
			$wxrService = new WT2XSRelationService ();
			for($i = 0; $i < count ( $bjList ); $i ++) {
				$bjSql = "insert into wt_bj_table (wt_id,bj_id) values('" . $id . "','" . $bjList [$i]->getBj_id () . "')";
				mysql_query ( $bjSql );
				$xsList = $xsService->getAllListById ( 0, $bjList [$i]->getBj_id () );
				if ($wxrService->addRelation ( $id, $xsList, $wt->getWt_type () )) {
					$count ++;
				}
			}
			if ($count == count ( $bjList )) {
				return true;
			} else {
				return false;
			}
		
		} else {
			return false;
		}
	}
	public function deleteWenTiById($wtId) {
		$sql = "delete from wt_table where wt_id='$wtId'";
		$result = mysql_query ( $sql );
		$zySetvice = new ZuoYeService();
		if ($result&&$zySetvice->deleteZuoYeByWt($wtId)) {
			return true;
		} else {
			return false;
		}
	}
	public function deleteWenTiByLs($lsId)
	{
		$sql = "select * from wt_table where ls_id='".$lsId."'";
		$result = mysql_query($sql);
		$zySetvice = new ZuoYeService();
		$row = mysql_fetch_array($result);
		while($row)
		{
			if(!$zySetvice->deleteZuoYeByWt($row['wt_id']))
			{
				return false;
			}
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from wt_table where ls_id='$lsId'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	public function deleteWenTiBatch($wtList) {
		$count = 0;
		$zyService = new ZuoYeService();
		for($i = 0; $i < count ( $wtList ); $i ++) {
			$sql = "delete from wt_table where wt_id='$wtList[$i]'";
			$result = mysql_query ( $sql );
			
			if ($result) {
				$zyService->deleteZuoYeByWt($wtList[$i]);
				$this->deleteWBByWt ( $wtList [$i] );
				$count ++;
			}
		
		}
		if ($count == count ( $wtList )) {
			return true;
		} else {
			return false;
		}
	}
	public function deleteWBByWt($wt_id) {
		$sql = "delete from wt_bj_table where wt_id='$wt_id'";
		if (mysql_query ( $sql )) {
			return true;
		} else {
			return false;
		}
	}
	public function getWenTiById($wtId,$bjId=0) {
		$sql = "select * from wt_table where wt_id='$wtId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			$wt = new WenTi ();
			$wt->setEnd_time ( date ( "Y-m-d H:i:s", $row ['end_time'] ) );
			$wt->setStart_time ( date ( "Y-m-d H:i:s", $row ['start_time'] ) );
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$lsService = new LaoShiService ();
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$zyService = new ZuoYeService();
			$wt->setAvg($zyService->getZuoYeByWTandBJAVG($wt->getWt_id(),$bjId));
			$wt->setMax($zyService->getZuoYeByWTandBJMAX($wt->getWt_id(),$bjId));
			$wt->setMin($zyService->getZuoYeByWTandBJMIN($wt->getWt_id(),$bjId));
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setWt_type ( $row ['wt_type'] );
			$wt->setBjList ( $this->getBjByWt ( $wtId ) );
			return $wt;
		} else {
			return null;
		}
	}
	public function getAllList() {
		$sql = "select * from wt_table";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$lsService = new LaoShiService ();
		while ( $row ) {
			$wt = new WenTi ();
			$wt->setEnd_time ( date ( "Y-m-d H:i:s", $row ['end_time'] ) );
			$wt->setStart_time ( date ( "Y-m-d H:i:s", $row ['start_time'] ) );
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$wt->setWt_type ( $row ['wt_type'] );
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setBjList ( $this->getBjByWt ( $row ['wt_id'] ) );
			$wtList [] = $wt;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function getAllListById($lsId) {
		$sql = "select * from wt_table where ls_id='$lsId' order by wt_id desc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$lsService = new LaoShiService ();
		while ( $row ) {
			$wt = new WenTi ();
			$wt->setEnd_time ( date ( "Y-m-d H:i:s", $row ['end_time'] ) );
			$wt->setStart_time ( date ( "Y-m-d H:i:s", $row ['start_time'] ) );
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$wt->setWt_type ( $row ['wt_type'] );
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setBjList ( $this->getBjByWt ( $row ['wt_id'] ) );
			$wtList [] = $wt;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function getLabelCount($id) {
		$sql = "select count(*) from wt_table where ls_id='$id'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	public function getListForPageById($start, $size, $id) {
		$sql = "select * from wt_table where ls_id='$id' order by wt_id desc limit $start,$size";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$lsService = new LaoShiService ();
		while ( $row ) {
			$wt = new WenTi ();
			$wt->setEnd_time ( date ( "Y-m-d H:i:s", $row ['end_time'] ) );
			$wt->setStart_time ( date ( "Y-m-d H:i:s", $row ['start_time'] ) );
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$wt->setWt_type ( $row ['wt_type'] );
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setBjList ( $this->getBjByWt ( $row ['wt_id'] ) );
			$wtList [] = $wt;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function searchWenTi($start_time = 0, $end_time = 0, $wt_name = null, $lsId = 0, $xyId = 0, $type = 0, $wt_type = 0) {
		if($wt_type>1)//不更具题目类型搜索
	{	
		if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' order by start_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' order by start_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' order by start_time desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%' order by start_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' order by start_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' order by start_time desc ";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' order by end_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' order by end_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' order by end_time desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%' order by end_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' order by end_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' order by end_time desc ";
				}
			}
		} else {
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table order by wt_id desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' order by wt_id desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' order by wt_id desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where wt_name like '%$wt_name%' order by wt_id desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' order by wt_id desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' order by wt_id desc ";
				}
			}
		}
	}
	else //更具题目类型搜索
	{
	if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_type='$wt_type' order by start_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type'  order by start_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'  order by start_time desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'   order by start_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by start_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by start_time desc ";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
	
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_type='$wt_type'  order by end_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type'  order by end_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'  order by end_time desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc ";
				}
			}
		} else {//不更具时间搜索
		
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from  wt_table where wt_type='$wt_type'  order by wt_id desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_type='$wt_type'  order by wt_id desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_type='$wt_type'  order by wt_id desc ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc ";
				}
			}
		}
	}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$lsService = new LaoShiService ();
		while ( $row ) {
			$wt = new WenTi ();
			$wt->setEnd_time ( $row ['end_time'] );
			$wt->setStart_time ($row ['start_time']);
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$wt->setWt_type ( $row ['wt_type'] );
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setBjList ( $this->getBjByWt ( $row ['wt_id'] ) );
			$wtList [] = $wt;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function searchWenTiForPage($start, $size, $start_time = 0, $end_time = 0, $wt_name = null, $lsId = 0, $xyId = 0, $type = 0, $wt_type = 0) {
	if($wt_type>1)//不更具题目类型搜索
	{	if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' order by start_time desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%' order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' order by start_time desc limit $start,$size";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' order by end_time desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%' order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' order by end_time desc limit $start,$size";
				}
			}
		} else {
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' order by wt_id desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where wt_name like '%$wt_name%' order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' order by wt_id desc limit $start,$size";
				}
			}
		}
	}
	else //更具题目类型搜索
	{
	if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_type='$wt_type' order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type'  order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'  order by start_time desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'   order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by start_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by start_time desc limit $start,$size";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by end_time desc limit $start,$size";
				}
			}
		} else {
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where wt_type='$wt_type' order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_type='$wt_type'  order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_type='$wt_type'  order by wt_id desc limit $start,$size";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select * from wt_table where wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select * from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc limit $start,$size";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select * from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'  order by wt_id desc limit $start,$size";
				}
			}
		}
	}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$lsService = new LaoShiService ();
		while ( $row ) {
			$wt = new WenTi ();
			$wt->setEnd_time (  $row ['end_time']  );
			$wt->setStart_time ( $row ['start_time']  );
			$wt->setWt_describe ( $row ['wt_describe'] );
			$wt->setWt_name ( $row ['wt_name'] );
			$wt->setWt_id ( $row ['wt_id'] );
			$wt->setWt_type ( $row ['wt_type'] );
			$ls = $lsService->getLaoShiById ( $row ['ls_id'] );
			$wt->setLs ( $ls );
			$wt->setXy ( $ls->getXy () );
			$wt->setBjList ( $this->getBjByWt ( $row ['wt_id'] ) );
			$wtList [] = $wt;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
    public function getLabelCountForSearch($start_time = 0, $end_time = 0, $wt_name = null, $lsId = 0, $xyId = 0, $type = 0, $wt_type = 0)
    {
    	if($wt_type>1)//不更具题目类型搜索
	{	if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId'";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%'  ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%'  ";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%'  ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%'  ";
				}
			}
		} else {
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where xy_id='$xyId'  ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where ls_id='$lsId'  ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where wt_name like '%$wt_name%'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' ";
				}
			}
		}
	}
	else //更具题目类型搜索
	{
	if ($type == 0 && $start_time != 0 && $end_time != 0) //按照开始提交时间查找
{
			if (is_null ( $wt_name )) //用户为数据作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_type='$wt_type'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'   ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'   ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where start_time>='$start_time' and start_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'   ";
				}
			}
		} else if ($type != 0 && $start_time != 0 && $end_time != 0) //按照作业的结束提交时间查找
{
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_type='$wt_type'   ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_type='$wt_type'  ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and wt_name like '%$wt_name%' and wt_type='$wt_type'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where end_time>='$start_time' and end_time<='$end_time' and ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type'   ";
				}
			}
		} else {
			if (is_null ( $wt_name )) //用户未输入作业名的关键词
{
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where xy_id='$xyId' and wt_type='$wt_type'  ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where ls_id='$lsId' and wt_type='$wt_type'  ";
				}
			} //用户输入了作业名的关键词
else {
				if ($lsId == 0 && $xyId == 0) //此时想全校的老师布置的作业集合进行查找符合要求的作业
{
					$sql = "select count(*) from wt_table where wt_name like '%$wt_name%' and wt_type='$wt_type'  ";
				} else if ($xyId != 0 && $lsId == 0) //在指定的学院内查找作业
{
					$sql = "select count(*) from wt_table where xy_id='$xyId' and wt_name like '%$wt_name%' and wt_type='$wt_type' ";
				} else if ($xyId != 0 && $lsId != 0) {
					$sql = "select count(*) from wt_table where ls_id='$lsId' and wt_name like '%$wt_name%' and wt_type='$wt_type' ";
				}
			}
		}
	}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
    }
	public function getBjByWt($wtId) {
		$sql = "select * from wt_bj_table where wt_id='$wtId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$bjService = new BanJiService ();
		while ( $row ) {
			$bjList [] = $bjService->getBanJiById ( $row ['bj_id'] );
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $bjList ) > 0) {
			return $bjList;
		} else {
			return null;
		}
	}
	public function getUndoWtByXs($bjId,$xsId) {
		$sql = "select wt_table.wt_id from wt_table join wt_xs_table on wt_xs_table.wt_id=wt_table.wt_id where wt_xs_table.bj_id='$bjId' and wt_xs_table.xs_id='$xsId' and wt_xs_table.state='0' order by start_time asc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			$wtList [] = $this->getWenTiById ( $row ['wt_id'] );
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function getWTByBj($bjId)
	{
		$sql = "select wt_id from wt_bj_table where bj_id='$bjId'";
	    $result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			$wtList [] = $this->getWenTiById ( $row ['wt_id'] );
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
    public function getDoneWtByXs($bjId,$xsId) {
		$sql = "select * from wt_bj_table join wt_xs_table on wt_bj_table.wt_id=wt_xs_table.wt_id where wt_bj_table.bj_id='$bjId' and wt_xs_table.bj_id='$bjId' and wt_xs_table.state='1' and wt_xs_table.xs_id='$xsId' ";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		while ( $row ) {
			$wtList [] = $this->getWenTiById ( $row ['wt_id'] );
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $wtList ) > 0) {
			return $wtList;
		} else {
			return null;
		}
	}
	public function updateWenTi(WenTi $wt,$state) {
		$sql = "update wt_table set wt_name='" . $wt->getWt_name () . "',wt_describe='" . $wt->getWt_describe () . "',ls_id='" . $wt->getLs ()->getLs_id () . "',start_time='" . $wt->getStart_time () . "',end_time='" . $wt->getEnd_time () . "',wt_type='" . $wt->getWt_type () . "',xy_id='" . $wt->getXy ()->getXy_id () . "' where wt_id='" . $wt->getWt_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			$bjList = $wt->getBjList ();
			$count = 0;
			$xsService = new XueShengService ();
			$wxrService = new WT2XSRelationService ();
			if($state!=0)
			{
			if( count ( $bjList )>0)
			{
			$this->deleteWBByWt($wt->getWt_id ());
			$sqlWS = "delete from wt_xs_table where wt_id='" . $wt->getWt_id () . "'";
		    mysql_query ( $sqlWS );
			}
			for($i = 0; $i < count ( $bjList ); $i ++) {
				$bjSql = "insert into wt_bj_table (wt_id,bj_id) values('".$wt->getWt_id ()."','".$bjList [$i]->getBj_id ()."')";
				mysql_query($bjSql);
				$xsList = $xsService->getAllListById ( 0, $bjList [$i]->getBj_id () );
				if ($wxrService->updateRelationBatch ( $wt->getWt_id (), $xsList, $wt->getWt_type () )) {
					$count ++;
				}
			}
			
			if ($count == count ( $bjList )) {
				return true;
			} else {
				return false;
			}
			}
			else {
				return true;
			}
		
		} else {
			return false;
		}
	}
}
?>