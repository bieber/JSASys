<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/BanJi.php';
include_once 'ZhuanYeService.php';
include_once 'XueShengService.php';
/**
 * 
 * Enter description 该类是封装了对班级实体的各种操作
 * @author wuzhiwei
 *
 */
class BanJiService {
	
	public function checkBanJi($bjName, $zhyId) {
		$sql = "select * from bj_table where bj_name='" . $bjName . "' and zhy_id='$zhyId'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 将该班级对象添加到数据库中
	 * @param BanJi $bj
	 */
	
	public function addBanJi(BanJi $bj) {
		$sql = "insert into bj_table (zhy_id,bj_name) values('" . $bj->getZhy ()->getZhy_id () . "','" . $bj->getBj_name () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据班级的唯一编号获取对应的班级对象
	 * @param unknown_type $id
	 */
	public function getBanJiById($id) {
		$sql = "select * from bj_table where bj_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$zhyService = new ZhuanYeService ();
		$xsService = new XueShengService();
		if ($row) {
			$bj = new BanJi ();
			$bj->setBj_id ( $row ['bj_id'] );
			$bj->setBj_name ( $row ['bj_name'] );
			$bj->setZhy ( $zhyService->getZhuanYeById ( $row ['zhy_id'] ) );
			$bj->setXsCount($xsService->getLabelCount(0,$row['bj_id']));
			return $bj;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据专业获取其包含的班级集合
	 * @param unknown_type $zhyId
	 */
	public function getBanJiByZhy($zhyId) {
		$sql = "select * from bj_table where zhy_id='" . $zhyId . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$zhyService = new ZhuanYeService ();
		$xsService = new XueShengService();
		while ( $row ) {
			$bj = new BanJi ();
			$bj->setBj_id ( $row ['bj_id'] );
			$bj->setBj_name ( $row ['bj_name'] );
			$bj->setZhy ( $zhyService->getZhuanYeById ( $row ['zhy_id'] ) );
			$bj->setXsCount($xsService->getLabelCount(0,$row['bj_id']));
			$bjList [] = $bj;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $bjList ) > 0) {
			return $bjList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据班级的唯一编号删除班级
	 * @param unknown_type $id
	 */
	public function deleteBanJiById($id) {
        $sql = "delete from wt_bj_table where bj_id='".$id."'";
        $result = mysql_query ( $sql );
        if($result)
        {
		$sql = "delete from bj_table where bj_id='" . $id . "'";
		
		$xsService = new XueShengService();
		$result = mysql_query ( $sql );
		if ($result&&$xsService->deleteXueShengByBj($id)) {
			return true;
		} else {
			return false;
		}
	}
	else 
	{
		return false;
	}
	}
	/**
	 * 
	 * 更新班级信息
	 * @param BanJi $bj
	 */
	public function updateBanJi(BanJi $bj) {
		$sql = "update bj_table set bj_name = '" . $bj->getBj_name () . "',zhy_id='" . $bj->getZhy ()->getZhy_id () . "' where bj_id='" . $bj->getBj_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据专业删除其包含的所有班级
	 * @param unknown_type $zhyId
	 */
	public function deleteBanJiByZhY($zhyId) {
		$sql = "select bj_id from bj_table where zhy_id='".$zhyId."'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array($result);
		while($row)
		{
			$sqlWB = "delete from wt_bj_table where bj_id='".$row['bj_id']."'";

			mysql_query($sqlWB);
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from bj_table where zhy_id='" . $zhyId . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
}
?>