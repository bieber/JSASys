<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/models/LaoShi.php';
include_once 'LiuYanService.php';
include_once 'WenTiService.php';
/**
 * 
 * Enter description 该类是对老师实体进行的各种操作
 * @author wuzhiwei
 *
 */
class LaoShiService {
	
	private $action = "";
	public function setAction($action) {
		$this->action = $action;
	}
	/**
	 * 
	 * Enter description 根据老师对象将该老师添加到数据库中
	 * @param LaoShi $ls
	 * @return boolean
	 */
	public function addLaoShi(LaoShi $ls) {
		$sql = "insert into ls_table (ls_name,ls_email,ls_tele,xy_id,ls_tx,ls_yz,ls_describe,ls_xb,ls_loginname,ls_loginpsw) 
		values('" . $ls->getLs_name () . "','" . $ls->getLs_email () . "','" . $ls->getLs_tele () . "','" . $ls->getXy ()->getXy_id () . "','" . $ls->getLs_tx () . "','" . $ls->getLs_yz () . "','" . $ls->getLs_describe () . "','" . $ls->getLs_xb () . "','" . $ls->getLs_loginname () . "','" . $ls->getLs_loginpwd () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 用于检测老师的登录名是否已经存在
	 * @param unknown_type $name
	 */
	public function checkLaoShiByName($name) {
		$sql = "select * from ls_table where ls_loginname = '$name'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		//echo $sql;
		if ($row) {
			return retrun;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的老师唯一编号获得对应的老师对象
	 * @param unknown_type $id
	 */
	public function getLaoShiById($id) {
		$sql = "select * from ls_table where ls_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		if ($row) {
			$ls = new LaoShi ();
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			return $ls;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据学院的唯一编号获取包含在该学院的所有老师对象集合
	 * @param unknown_type $xyId
	 */
	public function getAllListById($xyId) {
		if ($this->action == "waitValidateTeachers") {
			$sql = "select * from ls_table where xy_id='" . $xyId . "' and ls_yz='0'";
		} else if ($this->action == "validatedTeachers") {
			$sql = "select * from ls_table where xy_id='" . $xyId . "' and ls_yz='1'";
		} else {
			$sql = "select * from ls_table where xy_id='" . $xyId . "' and ls_yz='2'";
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		while ( $row ) {
			$ls = new LaoShi ();
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			$lsList [] = $ls;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lsList ) > 0) {
			return $lsList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据老师的唯一编号删除对应的老师
	 * @param unknown_type $id
	 */
	public function deleteLaoShi($id) {
		$sql = "delete from ls_table where ls_id='" . $id . "'";
		
	   $lyService = new LiuYanServie();
	   $wtService = new WenTiService();
	   if($lyService->deleteLYByLs($id)&&$wtService->deleteWenTiByLs($id))
	   {
	   	$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	   }
	   else
	   {
	   	return false;
	   }
	}
	/**
	 * 
	 * Enter description 根据学院的唯一编号删除在该学院下的所有老师
	 * @param unknown_type $xyId
	 */
	public function deleteLaoShiByXy($xyId) {
		$sql = "select * from ls_table where xy_id='".$xyId."'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$lyService = new LiuYanServie();
		$wtService = new WenTiService();
		while($row)
		{
			if(!$lyService->deleteLYByLs($row['ls_id'])||!$wtService->deleteWenTiByLs($row['ls_id'])){
				return false;
			}
			$row = mysql_fetch_array($result);
		}
		$sql = "delete from ls_table where xy_id='" . $xyId . "' order by ls_id desc";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 更新传递过来的老师
	 * @param LaoShi $ls
	 */
	public function updateLaoShi(LaoShi $ls) {
		$sql = "update ls_table set ls_name='" . $ls->getLs_name () . "',ls_email='" . $ls->getLs_email () . "',ls_tele='" . $ls->getLs_tele () . "', xy_id='" . $ls->getXy ()->getXy_id () . "',ls_tx='" . $ls->getLs_tx () . "',ls_yz='" . $ls->getLs_yz () . "',ls_describe='" . $ls->getLs_describe () . "',ls_loginname='" . $ls->getLs_loginname () . "',ls_loginpsw='" . $ls->getLs_loginpwd () . "',ls_xb='" . $ls->getLs_xb () . "' where ls_id='" . $ls->getLs_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * Enter description获取老师的记录条数
	 * @param unknown_type $xyid
	 */
	public function getLabelCount($xyid = 0) {
		if ($xyid == 0) {
			if ($this->action == "waitValidateTeachers") {
				$sql = "select count(*) from ls_table where ls_yz='0' ";
			} else if ($this->action == "validatedTeachers") {
				$sql = "select count(*) from ls_table where ls_yz='1' ";
			} else {
				$sql = "select count(*) from ls_table where ls_yz='2' ";
			}
			
			$result = mysql_query ( $sql );
			$row = mysql_fetch_array ( $result );
			if ($row) {
				return $row [0];
			} else {
				return 0;
			}
		} else {
			if ($this->action == "waitValidateTeachers") {
				$sql = "select count(*) from ls_table where  xy_id='$xyid' and  ls_yz='0' ";
			} else if ($this->action == "validatedTeachers") {
				$sql = "select count(*) from ls_table where  xy_id='$xyid' and  ls_yz='1' ";
			} else {
				$sql = "select count(*) from ls_table where  xy_id='$xyid' and ls_yz='2' ";
			}
			$result = mysql_query ( $sql );
			$row = mysql_fetch_array ( $result );
			if ($row) {
				return $row [0];
			} else {
				return 0;
			}
		}
	}
	
	public function getListForPageById($start, $end, $id) {
		if ($this->action == "waitValidateTeachers") {
			$sql = "select * from ls_table where xy_id='" . $id . "' and ls_yz='0' order by ls_id desc  limit $start,$end";
		} else if ($this->action == "validatedTeachers") {
			$sql = "select * from ls_table where xy_id='" . $id . "' and ls_yz='1' order by ls_id desc  limit $start,$end";
		} else {
			$sql = "select * from ls_table where xy_id='" . $id . "' and ls_yz='2' order by ls_id desc  limit $start,$end";
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		while ( $row ) {
			$ls = new LaoShi ();
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			$lsList [] = $ls;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lsList ) > 0) {
			return $lsList;
		} else {
			return null;
		}
	}
	public function getAllList() {
		if ($this->action == "waitValidateTeachers") {
			$sql = "select * from ls_table where  ls_yz='0' order by ls_id desc";
		} else if ($this->action == "validatedTeachers") {
			$sql = "select * from ls_table where ls_yz='1' order by ls_id desc";
		} else {
			$sql = "select * from ls_table where ls_yz='2' order by ls_id desc ";
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		while ( $row ) {
			$ls = new LaoShi ();
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			$lsList [] = $ls;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lsList ) > 0) {
			return $lsList;
		} else {
			return null;
		}
	}
	public function getListForPage($start, $end) {
		if ($this->action == "waitValidateTeachers") {
			$sql = "select * from ls_table where  ls_yz='0' order by ls_id desc limit $start,$end";
		} else if ($this->action == "validatedTeachers") {
			$sql = "select * from ls_table where ls_yz='1' order by ls_id desc limit $start,$end";
		} else {
			$sql = "select * from ls_table where ls_yz='2' order by ls_id desc limit $start,$end";
		}
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		while ( $row ) {
			$ls = new LaoShi ();
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			$lsList [] = $ls;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $lsList ) > 0) {
			return $lsList;
		} else {
			return null;
		}
	}
	public function checkLaoShiLogin(Laoshi $ls) {
		$sql = "select * from ls_table where ls_loginname='" . $ls->getLs_loginname () . "' and ls_loginpsw='" . $ls->getLs_loginpwd () . "' and ls_yz='1'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$ls = new LaoShi ();
		$xyService = new XueYuanService ();
		if ($row) {
			$ls->setLs_id ( $row ['ls_id'] );
			$ls->setLs_name ( $row ['ls_name'] );
			$ls->setLs_email ( $row ['ls_email'] );
			$ls->setLs_tele ( $row ['ls_tele'] );
			$ls->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$ls->setLs_tx ( $row ['ls_tx'] );
			$ls->setLs_yz ( $row ['ls_yz'] );
			$ls->setLs_describe ( $row ['ls_describe'] );
			$ls->setLs_xb ( $row ['ls_xb'] );
			$ls->setLs_loginname ( $row ['ls_loginname'] );
			$ls->setLs_loginpwd ( $row ['ls_loginpsw'] );
			return $ls;
		} else {
			return null;
		}
	}
	public function getLsForFindPWD($rname,$tele,$xy)
	{
		$sql = "select ls_id from ls_table where ls_name='$rname' and ls_tele='$tele' and xy_id='$xy'";
	 	$result =  mysql_query($sql);
	 	$row = mysql_fetch_array($result);
	 	if($row)
	 	{
	 		return $row[0];
	 	}
	 	else
	 	{
	 		return 0;
	 	}
	}
}
?>