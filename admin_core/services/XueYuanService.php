<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/models/XueYuan.php';
include_once ROOT . '/models/ZhuanYe.php';
include_once ROOT . '/models/BanJi.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT . '/utils/Function.php';
include_once 'LaoShiService.php';
include_once 'ZhuanYeService.php';
/**
 * 
 * Enter description 该类是对学院实体进行的各种操作
 * @author wuzhiwei
 *
 */
class XueYuanService {
	/**
	 * 
	 * Enter description 检测传递过来的学院名是否存在
	 * @param unknown_type $name
	 */
	public function checkXYByName($name) {
		
		$sql = "select * from xy_table where xy_name='" . $name . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 将传递过来的学院对象添加到数据库中
	 * @param XueYuan $xy
	 */
	public function addXueYuan(XueYuan $xy) {
		$sql = "insert into xy_table (xy_name,xy_describe) values('" . $xy->getXy_name () . "','" . $xy->getXy_describe () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description获取总的记录数
	 */
	public function getLabelCount() {
		$sql = "select count(*) from xy_table";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	/**
	 * 
	 * Enter description 获取所有学院对象集合
	 */
	public function getAllList() {
		$sql = "select * from xy_table order by xy_id desc";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$zhyService = new ZhuanYeService();
		$zhyService = new ZhuanYeService ();
		while ( $row ) {
			$xueYuan = new XueYuan ();
			$xueYuan->setXy_id ( $row ['xy_id'] );
			$xueYuan->setXy_name ( $row ['xy_name'] );
			$xueYuan->setXy_describe ( $row ['xy_describe'] );
			$zhyList = $zhyService->getAllListById ( $row ['xy_id'] );
			if ($zhyList) {
				$xueYuan->setZhyCount ( count ( $zhyList ) );
				$xueYuan->setZhyList ( $zhyList );
			} else {
				$xueYuan->setZhyList ( null );
				$xueYuan->setZhyCount ( 0 );
			}
			$xueYuanList [] = $xueYuan;
			$row = mysql_fetch_array ( $result );
		}
		if (!isset($xueYuanList)||count ( $xueYuanList ) == 0) {
			return null;
		} else {
			return $xueYuanList;
		}
	}
	/**
	 * 
	 * Enter description 将学院集合进行分页显示
	 * @param unknown_type $start
	 * @param unknown_type $end
	 */
	public function getListForPage($start, $end) {
		$sql = "select * from xy_table order by xy_id desc limit $start,$end";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$zhyService = new ZhuanYeService ();
		while ( $row ) {
			$xueYuan = new XueYuan ();
			$xueYuan->setXy_id ( $row ['xy_id'] );
			$xueYuan->setXy_name ( $row ['xy_name'] );
			$xueYuan->setXy_describe ( $row ['xy_describe'] );
			$zhyList = $zhyService->getAllListById ( $row ['xy_id'] );
			if ($zhyList) {
				$xueYuan->setZhyCount ( count ( $zhyList ) );
				$xueYuan->setZhyList ( $zhyList );
			} else {
				$xueYuan->setZhyList ( null );
				$xueYuan->setZhyCount ( 0 );
			}
			$xueYuanList [] = $xueYuan;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $xueYuanList ) == 0) {
			return null;
		} else {
			return $xueYuanList;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的学院唯一编码得到一个学院对象
	 * @param unknown_type $id
	 */
	public function getXueYuanById($id) {
		$sql = "select * from xy_table where xy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			$xueYuan = new XueYuan ();
			$xueYuan->setXy_id ( $row ['xy_id'] );
			$xueYuan->setXy_name ( $row ['xy_name'] );
			$xueYuan->setXy_describe ( $row ['xy_describe'] );
			return $xueYuan;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的学院唯一标识符删除对应的学院信息
	 * @param unknown_type $id
	 */
	public function deleteXueYuanById($id) {
		$sql = "delete from xy_table where xy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$lsService = new LaoShiService ();
		$zhyService = new ZhuanYeService ();
		$xsService = new XueShengService();
		$xsService->deleteXueShengByXy($id);
		$lsService->deleteLaoShiByXy ( $id );
		$zhyService->deleteZhuanYeByXYId ( $id );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 更新学院的信息
	 * @param XueYuan $xy
	 */
	public function updateXueYuan(XueYuan $xy) {
		$sql = "update xy_table set xy_name='" . $xy->getXy_name () . "',xy_describe='" . $xy->getXy_describe () . "' where xy_id='" . $xy->getXy_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
}

?>