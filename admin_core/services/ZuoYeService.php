<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/utils/mysql_class.php';
include_once ROOT.'/models/ZuoYe.php';
include_once 'LaoShiService.php';
class ZuoYeService{
	public function deleteZuoYeByWt($wtId)
	{
	    $sql = "delete from zy_table where wt_id='$wtId'";
		$result = mysql_query($sql);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function submitZuoYe(ZuoYe $zy)
	{
		$sql = "insert into zy_table (zy_name,xs_id,wt_id,zy_file,zy_content,zy_py,zy_fs,ls_id,bj_id,tj_date,zy_state) value('".$zy->getZy_name()."','".$zy->getXs()->getXs_id()."'
		,'".$zy->getWt()->getWt_id()."','".$zy->getZy_file()."','".$zy->getZy_content()."','".$zy->getZy_py()."','".$zy->getZy_fs()."','".$zy->getLs()->getLs_id()."',
		'".$zy->getBj()->getBj_id()."','".$zy->getTj_date()."','".$zy->getZy_sate()."')";
		$result = mysql_query($sql);
		
		if($result)
		{
			$wxrService = new WT2XSRelationService();
			$wxr = $wxrService->getRelation($zy->getWt()->getWt_id(), $zy->getXs()->getXs_id());
			if($wxr==null)
			{
				$wxrService->addRelationForOne($zy->getWt()->getWt_id(), $zy->getXs()->getXs_id(), $zy->getWt()->getWt_type(), $zy->getXs()->getXs_id());
				$wxr = $wxrService->getRelation($zy->getWt()->getWt_id(), $zy->getXs()->getXs_id());
			}
			$wxr->setState(1);
			$wxrService->updateRelation($wxr);
			return true;
		}
		else
		{
			return false;
		}
	}
	public function reDoZuoYe(ZuoYe $zy)
	{
		
		$sql ="update zy_table set zy_py='".$zy->getZy_py()."',zy_fs='".$zy->getZy_fs()."',zy_state='".$zy->getZy_sate()."',zy_file='".$zy->getZy_file()."',zy_content='".$zy->getZy_content()."' ,zy_py='".$zy->getZy_py()."',zy_fs='0',tj_date='".$zy->getTj_date()."' where zy_id='".$zy->getZy_id()."'";
		$result = mysql_query($sql);
		if($result)
		{
			$wxrService = new WT2XSRelationService();
			$wxr = $wxrService->getRelation($zy->getWt()->getWt_id(), $zy->getXs()->getXs_id());
			$wxr->setFs(0);
			$wxr->setState(1);
			$wxrService->updateRelation($wxr);
			return true;
		}
		else {
			return false;
		}
	}
	public function updateZuoYe(ZuoYe $zy)
	{
		$sql ="update zy_table set zy_py='".$zy->getZy_py()."',zy_fs='".$zy->getZy_fs()."',zy_state='".$zy->getZy_sate()."' where zy_id='".$zy->getZy_id()."'";
		$result = mysql_query($sql);
		if($result)
		{
			$wxrService = new WT2XSRelationService();
			$wxr = $wxrService->getRelation($zy->getWt()->getWt_id(), $zy->getXs()->getXs_id());
			$wxr->setFs($zy->getZy_fs());
			if($zy->getZy_sate()==2)
			{
				$wxr->setState(2);
			}
			$wxrService->updateRelation($wxr);
			return true;
		}
		else {
			return false;
		}
	}
	public function deleteZuoYeById($zyId)
	{
		$sql = "delete from zy_table where zy_id='$zyId'";
		$result = mysql_query($sql);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function deleteZuoYeByXs($xsId)
	{
		$sql = "delete from zy_table where xs_id='$xsId'";
		$result = mysql_query($sql);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * 
	 * 方法名：getCountByXS
	 * 说明：该方法是获得一位学生已经做过的作业总和
	 * @param unknown_type $xsId
	 */
	public function getCountByXS($xsId)
	{
		$sql="select count(*) from zy_table where xs_id='$xsId'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	public function getZuoYeById($zyId)
	{
		$sql = "select * from zy_table where zy_id='$zyId'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy_content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xsService = new XueShengService();
			$wtService = new WenTiService();
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			return $zy;
		}
		else
		{
			return null;
		}
	}
	//获取某个学生所有提交过的作业
	public function getZuoYeByXSORWT($xsId,$wtId=0)
	{
		if($wtId==0)
		{
		$sql = "select * from zy_table where xs_id='$xsId'";
		}
		else
		{
			$sql = "select * from zy_table where xs_id='$xsId' and wt_id='$wtId'";
		}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$xsService = new XueShengService();
		$wtService = new WenTiService();
		while($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy-content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			$zyList[] = $zy;
			$row = mysql_fetch_array($result);
		}
		if(count($zyList)>0)
		{
			return $zyList;
		}
		else {
			return null;
		}
	}
	//获取某次作业所有提交过来的学生作业
	public function getZuoYeByWT($wtId)
	{
		$sql = "select * from zy_table where wt_id='$wtId' order by zy_fs desc";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$xsService = new XueShengService();
		$wtService = new WenTiService();
		while($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy-content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			$zyList[] = $zy;
			$row = mysql_fetch_array($result);
		}
		if(count($zyList)>0)
		{
			return $zyList;
		}
		else {
			return null;
		}
	}
/**
 * 
 * Enter description 根据用户输入不同的搜索方案进行搜索
 * @param unknown_type $wt_id 问题的编号
 * @param unknown_type $zy_name 作业名
 * @param unknown_type $xs_id 提交作业的学生
 * @param unknown_type $ls_id 提交给的老师
 * @param unknown_type $bj_id 提交该作业的学生所属班级
 * @param unknown_type $zy_state 作业是否被老师审核
 */
	public function searchZuoYeForUser($wt_id=0,$zy_name=null,$xs_id=0,$ls_id=0,$bj_id=0,$zy_state=0,$tj_date=0,$start=0,$end=0)
	{
		$sql = $this->getSql($wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end);
        $sql = $sql." order by tj_date desc";
		//echo $zy_state;
	    $result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$xsService = new XueShengService();
		$wtService = new WenTiService();
		while($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy-content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			$zyList[] = $zy;
			$row = mysql_fetch_array($result);
		}
		if(count($zyList)>0)
		{
			return $zyList;
		}
		else {
			return null;
		}
	}
	public function searchZuoYeForUserByPage($wt_id=0,$zy_name=null,$xs_id=0,$ls_id=0,$bj_id=0,$zy_state=0,$tj_date=0,$start=0,$end=0,$start_index,$size)
	{
		
		$sql = $this->getSql($wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end);
		$sql = $sql." order by tj_date desc limit $start_index,$size";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$xsService = new XueShengService();
		$wtService = new WenTiService();
		while($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy-content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			$zyList[] = $zy;
			$row = mysql_fetch_array($result);
		}
		if(count($zyList)>0)
		{
			return $zyList;
		}
		else {
			return null;
		}
	}

	public function  searchZuoYeCountForUser($wt_id=0,$zy_name=null,$xs_id=0,$ls_id=0,$bj_id=0,$zy_state=0,$tj_date=0,$start=0,$end=0)
	{
		$sql = explode("*", $this->getSql($wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end));
		$sql = $sql[0]." count(*) ".$sql[1];
        $result = mysql_query($sql);
	    $row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	//获取某次作业平均分
	public function getZuoYeByWTAVG($wtId)
	{
		$sql = "select avg(zy_fs) from zy_table where wt_id='$wtId'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	//获取某次作业最高分
	public function getZuoYeByWTMAX($wtId)
	{
		$sql = "select MAX(zy_fs) from zy_table where wt_id='$wtId'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
    //获取某次作业最低分
	public function getZuoYeByWTMIN($wtId)
	{
		$sql = "select MIN(zy_fs) from zy_table where wt_id='$wtId'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	//获取某个班提交的某次作业
	public function getZuoYeByWTandBJ($wtId,$bjId)
	{
		$sql = "select * from zy_table where wt_id='$wtId' and bj_id='$bjId' order by zy_fs desc";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		$xsService = new XueShengService();
		$wtService = new WenTiService();
		while($row)
		{
			$zy = new ZuoYe();
			$zy->setZy_name($row['zy_name']);
			$zy->setZy_content($row['zy-content']);
			$zy->setZy_file($row['zy_file']);
			$zy->setZy_fs($row['zy_fs']);
			$zy->setZy_id($row['zy_id']);
			$zy->setZy_py($row['zy_py']);
			$zy->setZy_sate($row['zy_state']);
			$xs = $xsService->getXueShengById($row['xs_id']);
			$wt = $wtService->getWenTiById($row['wt_id']);
			$zy->setBj($xs->getBj());
			$zy->setWt($wt);
			$zy->setLs($wt->getLs());
			$zy->setTj_date($row['tj_date']);
			$zy->setXs($xs);
			$zyList[] = $zy;
			$row = mysql_fetch_array($result);
		}
		if(count($zyList)>0)
		{
			return $zyList;
		}
		else {
			return null;
		}
	}
    //获取某次作业某班最低分
	public function getZuoYeByWTandBJMIN($wtId,$bjId=0)
	{
		if($bjId!=0)
		{
		$sql = "select MIN(zy_fs) from zy_table where wt_id='$wtId' and bj_id='$bjId'";
		}else
		{
			$sql = "select MIN(zy_fs) from zy_table where wt_id='$wtId'";
		}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row[0]!=null)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
    //获取某次作业某班最高分
	public function getZuoYeByWTandBJMAX($wtId,$bjId=0)
	{
		if($bjId!=0)
		{
		$sql = "select MAX(zy_fs) from zy_table where wt_id='$wtId' and bj_id='$bjId'";
		}else
		{
			$sql = "select MAX(zy_fs) from zy_table where wt_id='$wtId'";
		}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row[0]!=null)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
    //获取某次作业某班平均分
	public function getZuoYeByWTandBJAVG($wtId,$bjId=0)
	{
		if($bjId!=0)
		{
		$sql = "select AVG(zy_fs) from zy_table where wt_id='$wtId' and bj_id='$bjId'";
		}
		else {
			$sql = "select AVG(zy_fs) from zy_table where wt_id='$wtId'";
		}
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		if($row[0]!=null)
		{
			return $row[0];
		}
		else
		{
			return 0;
		}
	}
	public function getSql($wt_id=0,$zy_name=null,$xs_id=0,$ls_id=0,$bj_id=0,$zy_state=0,$tj_date=0,$start=0,$end=0)
	{
		if($tj_date==0)//不指定提交时间
		{
	if($zy_name==null)//用户未输入作业名的关键字
		{
			if($zy_state>=1)//搜索未被或被老师审核的作业
			{
				if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and wt_id='$wt_id'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and wt_id='$wt_id'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and wt_id='$wt_id'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}else//不区分作业是否被审核
			 {
			 if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where wt_id='$wt_id'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  ls_id='$ls_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and wt_id='$wt_id'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and wt_id='$wt_id'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}
		}
		else//指定搜索作业名的关键字 
		{
		if($zy_state>=1)//搜索未被或被老师审核的作业
			{
				if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and zy_name like '%$zy_name%'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and zy_name like '%$zy_name%'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and zy_name like '%$zy_name%'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}else//不区分作业是否被审核
			 {
			 if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  zy_name like '%$zy_name%'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and zy_name like '%$zy_name%'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and zy_name like '%$zy_name%'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and zy_name like '%$zy_name%'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  ls_id='$ls_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}
		}
		}
		else //指定提交时间
		{
		if($zy_name==null)//用户未输入作业名的关键字
		{
			if($zy_state>=1)//搜索未被或被老师审核的作业
			{
				if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and tj_date<='$end' and tj_date>='$start'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}else//不区分作业是否被审核
			 {
			 if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where tj_date<='$end' and tj_date>='$start'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  ls_id='$ls_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}
		}
		else//指定搜索作业名的关键字 
		{
		if($zy_state>=1)//搜索未被或被老师审核的作业
			{
				if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where zy_state='".($zy_state-1)."' and ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}else//不区分作业是否被审核
			 {
			 if($wt_id==0)//不针对回答一个问题的作业进行搜索
				{
					if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//只搜索未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个学生未被或被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索某位老师未被或被审核某学生的作业
						}
					}
				}
				else//搜索回答某一个问题的作业
				 {
				 if($bj_id==0&&$xs_id==0)//不指定搜索某个班级提交的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业未被或被被老师审核的
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where  ls_id='$ls_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核的作业
						}
					}
					else if($bj_id!=0&&$xs_id==0)//搜索指定班级的作业
					{
						if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索某个班级对于某一个问题提交的作业未被或被被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and bj_id='$bj_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未被或被审核某班的作业
						}
					}
					else if($xs_id!=0)//搜索指定学生提交的作业
					{
					   if($ls_id==0)//不指定搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//搜索对于某一个问题提交的作业某个学生未被老师审核的作业其他情况不考虑
						}
						else //搜索提交给某个老师的作业
						{
							$sql="select * from zy_table where ls_id='$ls_id' and xs_id='$xs_id' and wt_id='$wt_id' and zy_name like '%$zy_name%' and tj_date<='$end' and tj_date>='$start'";//指定搜索对于某一个问题提交的作业某位老师未审核某学生的作业
						}
					}
				}
			}
		}
		}
		return $sql;
	}
	
}

?>