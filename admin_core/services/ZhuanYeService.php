<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT . '/models/ZhuanYe.php';
include_once ROOT . '/utils/mysql_class.php';
include_once 'XueYuanService.php';
include_once 'BanJiService.php';
include_once 'XueShengService.php';
/**
 * 
 * Enter description 该类是对专业实体进行的各种操作
 * @author wuzhiwei
 *
 */
class ZhuanYeService {
	
	/**
	 * 
	 * Enter description  将传递过来的专业对象添加到数据库中(对象的持久化)
	 * @param ZhuanYe $zhy
	 */
	public function addZhuanYe(ZhuanYe $zhy) {
		$sql = "insert into zhy_table (zhy_name,zhy_describe,xy_id) values('" . $zhy->getZhy_name () . "','" . $zhy->getZhy_describe () . "','" . $zhy->getXy ()->getXy_id () . "')";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description获取总的记录数
	 */
	public function getLabelCount($id) {
		$sql = "select count(*) from zhy_table where xy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return $row [0];
		} else {
			return 0;
		}
	}
	/**
	 * 
	 * Enter description 检测在对应的学院下是否 存在同名的专业
	 * @param unknown_type $zhyName
	 * @param unknown_type $xyid
	 */
	public function checkZhuanYe($zhyName, $xyid) {
		$sql = "select * from zhy_table where zhy_name='$zhyName' and xy_id='$xyid'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的专业唯一标识符得到一个专业对象
	 * @param unknown_type $id
	 */
	public function getZhuanYeById($id) {
		$sql = "select * from zhy_table where zhy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		if ($row) {
			$xyService = new XueYuanService ();
			$zhy = new ZhuanYe ();
			$zhy->setZhy_describe ( $row ['zhy_describe'] );
			$zhy->setZhy_name ( $row ['zhy_name'] );
			$zhy->setZhy_id ( $row ['zhy_id'] );
			$zhy->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			return $zhy;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 分页显示在一个学院下的专业列表
	 * @param unknown_type $start
	 * @param unknown_type $end
	 * @param unknown_type $id
	 */
	public function getListForPageById($start, $end, $id) {
		$sql = "select * from zhy_table where xy_id='" . $id . "'" . "order by xy_id desc  limit $start,$end";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
		$bjService = new BanJiService ();
		while ( $row ) {
			$zhy = new ZhuanYe ();
			$zhy->setZhy_describe ( $row ['zhy_describe'] );
			$zhy->setZhy_name ( $row ['zhy_name'] );
			$zhy->setZhy_id ( $row ['zhy_id'] );
			$zhy->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$bjList = $bjService->getBanJiByZhy ( $row ['zhy_id'] );
			if ($bjList) {
				$zhy->setBjCount ( count ( $bjList ) );
				$zhy->setBjList ( $bjList );
			
			} else {
				$zhy->setBjCount ( 0 );
				$zhy->setBjList ( null );
			}
			$zhyList [] = $zhy;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $zhyList ) > 0) {
			return $zhyList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 获取所有的在一个学院下的所有专业对象集合
	 * @param unknown_type $id
	 */
	public function getAllListById($id) {
		$sql = "select * from zhy_table where xy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$xyService = new XueYuanService ();
	   $bjService = new BanJiService ();
		while ( $row ) {
			$zhy = new ZhuanYe ();
			$zhy->setZhy_describe ( $row ['zhy_describe'] );
			$zhy->setZhy_name ( $row ['zhy_name'] );
			$zhy->setZhy_id ( $row ['zhy_id'] );
			$zhy->setXy ( $xyService->getXueYuanById ( $row ['xy_id'] ) );
			$bjList = $bjService->getBanJiByZhy ( $row ['zhy_id'] );
			if ($bjList) {
				$zhy->setBjCount ( count ( $bjList ) );
				$zhy->setBjList ( $bjList );
			
			} else {
				$zhy->setBjCount ( 0 );
				$zhy->setBjList ( null );
			}
			$zhyList [] = $zhy;
			$row = mysql_fetch_array ( $result );
		}
		if (count ( $zhyList ) > 0) {
			return $zhyList;
		} else {
			return null;
		}
	}
	/**
	 * 
	 * Enter description 根据专业的唯一标号删除该专业的信息
	 * @param unknown_type $id
	 */
	public function deleteZhuanYeById($id) {
		$sql = "delete from zhy_table where zhy_id='" . $id . "'";
		$result = mysql_query ( $sql );
		$bjService = new BanJiService ();
		$xsService = new XueShengService();
		if ($result && $bjService->deleteBanJiByZhY ( $id )&&$xsService->deleteXueShengByZhy($id)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据学院的唯一标号删除其下的所有专业的信息
	 * @param unknown_type $xyId
	 */
	public function deleteZhuanYeByXYId($xyId) {
		$sql = "select zhy_id from zhy_table where xy_id='" . $xyId . "'";
		$result = mysql_query ( $sql );
		$row = mysql_fetch_array ( $result );
		$bjService = new BanJiService ();
		while ( $row ) {
			$bjService->deleteBanJiByZhY ( $row ['zhy_id'] );
			$row = mysql_fetch_array ( $result );
		}
		$sql = "delete from zhy_table where xy_id='" . $xyId . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 更新一个专业
	 * @param ZhuanYe $zhy
	 */
	public function updateZhuanYe(ZhuanYe $zhy) {
		$sql = "update zhy_table set zhy_name='" . $zhy->getZhy_name () . "',zhy_describe='" . $zhy->getZhy_describe () . "',xy_id='" . $zhy->getXy ()->getXy_id () . "' where zhy_id='" . $zhy->getZhy_id () . "'";
		$result = mysql_query ( $sql );
		if ($result) {
			return $sql;
		} else {
			return $sql;
		}
		return $sql;
	}

}

?>