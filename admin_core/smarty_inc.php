<?php
include 'smarty/Smarty.class.php';
$smarty = new Smarty();
$smarty->config_dir="smarty/config_file.class.php";
$smarty->caching=false;
$smarty->template_dir="./templates";
$smarty->compile_dir="./templates_c";
$smarty->cache_dir="./smarty_cache";
$smarty->cache_lifetime=60;
$smarty->left_delimiter="#{";
$smarty->right_delimiter="}#";
?>