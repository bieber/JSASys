<?php

class BanJiControl{
	
	public function  checkBJ(fun $fun)
	{
		header ( "content-type:text/html; charset=utf-8" );
		$zhyId = $_GET['zhyId'];
		$bjName = $_GET['bjName'];
		$oldName = $_GET['oldName'];
		$bjName = iconv("utf-8", "gbk",$bjName);
		$oldName = iconv("utf-8", "gbk",$oldName);
		$bjService = new BanJiService();
	
		if($oldName == "")
		{
			if($bjService->checkBanJi($bjName,$zhyId))
			{
				$fun->closeDB();
				return true;
			}
			else
			{
				$fun->closeDB();
				return false;
			}
		}
		else
		{
		if($oldName == $bjName)
		{
			$fun->closeDB();
			return false;
		}	
		else
		{
			if($bjService->checkBanJi($bjName, $zhyId))
			{
				$fun->closeDB();
				return true;
			}
			else {
				$fun->closeDB();
				return false;
			}
		}
		}
	}

	public function addBJ(fun $fun)
	{
		$bjName = $_POST['bjName'];
		$zhyId = $_POST['zhyId'];
		$bj = new BanJi();
		$zhyService = new ZhuanYeService();
		$zhy = $zhyService->getZhuanYeById($zhyId);
		$bj->setZhy($zhy);
		$bj->setBj_name($bjName);
		$bjService = new BanJiService();
		if($bjService->addBanJi($bj))
		{
			$fun->addLog("在<".$zhy->getXy()->getXy_name().">学院的<".$zhy->getZhy_name().">专业添加了班级<".$bj->getBj_name().">");
			$fun->closeDB();
			$fun->alertMessage("班级添加成功！", "control.php?action=xyList");
		}
		else
		{
			$fun->addLog("在<".$zhy->getXy()->getXy_name().">学院的<".$zhy->getZhy_name().">专业添加了班级<".$bj->getBj_name().">失败");
			$fun->closeDB();
			$fun->alertMessage("班级添加失败！", "control.php?action=xyList");
		}
	}
	public function updateBJ(fun $fun)
	{
		$bjName = $_POST['bjName'];
		$zhyId = $_POST['zhyId'];
		$bjId = $_POST['bjId'];
		$bjService = new BanJiService();
		$bj = $bjService->getBanJiById($bjId);
		$bj->setBj_name($bjName);
		if($bjService->updateBanJi($bj))
		{
		    $fun->addLog("更新了<".$bj->getZhy()->getXy()->getXy_name().">学院的<".$bj->getZhy()->getZhy_name().">专业的<".$bj->getBj_name().">班级信息");
			$fun->closeDB();
			$fun->alertMessage("信息修改成功!", "control.php?action=xyList");
		}
		else
		{
			 $fun->addLog("更新了<".$bj->getZhy()->getXy()->getXy_name().">学院的<".$bj->getZhy()->getZhy_name().">专业的<".$bj->getBj_name().">班级信息失败");
			$fun->closeDB();
			$fun->alertMessage("信息修改失败!", "control.php?action=xyList");
		}
	}
	public function deleteBJ(fun $fun)
	{
		$bjId = $_GET['bjId'];
		$bjService = new BanJiService();
		if($bjService->deleteBanJiById($bjId))
		{
			 $fun->addLog("删除了一个班级");
			$fun->closeDB();
			$fun->alertMessage("删除成功！", "control.php?action=xyList");
		}
		else
		{
			 $fun->addLog("删除了一个班级失败");
			$fun->closeDB();
			$fun->alertMessage("删除失败！", "control.php?action=xyList");
		}
	}
	
}
?>