<?php
include_once 'LaoShi.php';
include_once 'BanJi.php';
include_once 'XueYuan.php';
class WenTi{
	/**
	 * 
	 * 类名：WenTi
	 * 功能：老师发布的问题，它可以对用多个学生提交的作业
	 * @var unknown_type
	 */
	private $wt_id;//作业的唯一标签
	private $wt_name;//作业名
	private $wt_describe;//作业说明
	private $ls;//发布改作业的老师
	private $start_time;//改作业的开始提交时间
	private $end_time;//结束提交时间
	private $bjList;//需要做改作业的班级集合
	private $wt_type;//问题是必做还是选做
	private $xy;//发布改作业的学院
	private $avg;//该作业的平均分
	private $min;//该作业的最低分
	private $max;//该作业的最高分
	/**
	 * @return the $avg
	 */
	public function getAvg() {
		return $this->avg;
	}

	/**
	 * @return the $min
	 */
	public function getMin() {
		return $this->min;
	}

	/**
	 * @return the $max
	 */
	public function getMax() {
		return $this->max;
	}

	/**
	 * @param field_type $avg
	 */
	public function setAvg($avg) {
		$this->avg = $avg;
	}

	/**
	 * @param field_type $min
	 */
	public function setMin($min) {
		$this->min = $min;
	}

	/**
	 * @param field_type $max
	 */
	public function setMax($max) {
		$this->max = $max;
	}

	/**
	 * @return the $xy
	 */
	public function getXy() {
		return $this->xy;
	}

	/**
	 * @param field_type $xy
	 */
	public function setXy($xy) {
		$this->xy = $xy;
	}

	/**
	 * @return the $wt_type
	 */
	public function getWt_type() {
		return $this->wt_type;
	}

	/**
	 * @param field_type $wt_type
	 */
	public function setWt_type($wt_type) {
		$this->wt_type = $wt_type;
	}

	/**
	 * @return the $bjList
	 */
	public function getBjList() {
		return $this->bjList;
	}

	/**
	 * @param field_type $bjList
	 */
	public function setBjList($bjList) {
		$this->bjList = $bjList;
	}

	/**
	 * @return the $wt_id
	 */
	public function getWt_id() {
		return $this->wt_id;
	}

	/**
	 * @return the $wt_name
	 */
	public function getWt_name() {
		return $this->wt_name;
	}

	/**
	 * @return the $wt_describe
	 */
	public function getWt_describe() {
		return $this->wt_describe;
	}

	/**
	 * @return the $ls
	 */
	public function getLs() {
		return $this->ls;
	}

	/**
	 * @return the $start_time
	 */
	public function getStart_time() {
		return $this->start_time;
	}

	/**
	 * @return the $end_time
	 */
	public function getEnd_time() {
		return $this->end_time;
	}

	/**
	 * @param unknown_type $wt_id
	 */
	public function setWt_id($wt_id) {
		$this->wt_id = $wt_id;
	}

	/**
	 * @param field_type $wt_name
	 */
	public function setWt_name($wt_name) {
		$this->wt_name = $wt_name;
	}

	/**
	 * @param field_type $wt_describe
	 */
	public function setWt_describe($wt_describe) {
		$this->wt_describe = $wt_describe;
	}

	/**
	 * @param field_type $ls
	 */
	public function setLs($ls) {
		$this->ls = $ls;
	}

	/**
	 * @param field_type $start_time
	 */
	public function setStart_time($start_time) {
		$this->start_time = $start_time;
	}

	/**
	 * @param field_type $end_time
	 */
	public function setEnd_time($end_time) {
		$this->end_time = $end_time;
	}

    
}
?>