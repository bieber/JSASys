<?php
/**
 * description 该类是对老师实体进行封装，对其属性进行方法的封装
 * @author hebibo
 *
 */
include_once 'XueYuan.php';
class LaoShi{
	private $ls_id;          /*老师的唯一编码*/
	private $ls_name;        /*老师的姓名*/
	private $ls_email;       /*老师的邮箱*/
	private $ls_tele;        /*老师的电话*/
	private $ls_describe;    /*老师的简述*/
	private $ls_tx;          /*老师的个人头像*/
	private $ls_xb;          /*老师性别*/
	private $ls_yz;          /*表示老师是否通过管理员验证*/
	private $xy;             /*老师所属的学院*/
	private $ls_loginname;   /*老师登录名*/
	private $ls_loginpwd;    /*老师登录密码*/
	
	/**
	 * @return the $ls_loginname
	 */
	public function getLs_loginname() {
		return $this->ls_loginname;
	}

	/**
	 * @return the $ls_loginpwd
	 */
	public function getLs_loginpwd() {
		return $this->ls_loginpwd;
	}

	/**
	 * @param field_type $ls_loginname
	 */
	public function setLs_loginname($ls_loginname) {
		$this->ls_loginname = $ls_loginname;
	}

	/**
	 * @param field_type $ls_loginpwd
	 */
	public function setLs_loginpwd($ls_loginpwd) {
		$this->ls_loginpwd = $ls_loginpwd;
	}

	/**
	 * @return the $ls_id
	 */
	public function getLs_id() {
		return $this->ls_id;
	}

	/**
	 * @return the $ls_name
	 */
	public function getLs_name() {
		return $this->ls_name;
	}

	/**
	 * @return the $ls_email
	 */
	public function getLs_email() {
		return $this->ls_email;
	}

	/**
	 * @return the $ls_tele
	 */
	public function getLs_tele() {
		return $this->ls_tele;
	}

	/**
	 * @return the $ls_describe
	 */
	public function getLs_describe() {
		return $this->ls_describe;
	}

	/**
	 * @return the $ls_tx
	 */
	public function getLs_tx() {
		return $this->ls_tx;
	}

	/**
	 * @return the $ls_xb
	 */
	public function getLs_xb() {
		return $this->ls_xb;
	}

	/**
	 * @return the $ls_yz
	 */
	public function getLs_yz() {
		return $this->ls_yz;
	}

	/**
	 * @return the $xy
	 */
	public function getXy() {
		return $this->xy;
	}

	/**
	 * @param field_type $ls_id
	 */
	public function setLs_id($ls_id) {
		$this->ls_id = $ls_id;
	}

	/**
	 * @param field_type $ls_name
	 */
	public function setLs_name($ls_name) {
		$this->ls_name = $ls_name;
	}

	/**
	 * @param field_type $ls_email
	 */
	public function setLs_email($ls_email) {
		$this->ls_email = $ls_email;
	}

	/**
	 * @param field_type $ls_tele
	 */
	public function setLs_tele($ls_tele) {
		$this->ls_tele = $ls_tele;
	}

	/**
	 * @param field_type $ls_describe
	 */
	public function setLs_describe($ls_describe) {
		$this->ls_describe = $ls_describe;
	}

	/**
	 * @param field_type $ls_tx
	 */
	public function setLs_tx($ls_tx) {
		$this->ls_tx = $ls_tx;
	}

	/**
	 * @param field_type $ls_xb
	 */
	public function setLs_xb($ls_xb) {
		$this->ls_xb = $ls_xb;
	}

	/**
	 * @param field_type $ls_yz
	 */
	public function setLs_yz($ls_yz) {
		$this->ls_yz = $ls_yz;
	}

	/**
	 * @param field_type $xy
	 */
	public function setXy(XueYuan $xy) {
		$this->xy = $xy;
	}

	
}
?>