<?php
class LiuYan{
	private $ly_id;//留言编号
	private $ly_title;//留言标题/主题
	private $ly_date;//留言时间
	private $ly_content;//留言内容
	private $xs;//留言的学生
	private $ls;//给留言的老师
	private $hf_content;//回复内容
	private $hf_date;//回复时间
	private $ly_hf;//标记该条留言是否已回复
	/**
	 * @return the $ly_id
	 */
	public function getLy_id() {
		return $this->ly_id;
	}

	/**
	 * @return the $ly_title
	 */
	public function getLy_title() {
		return $this->ly_title;
	}

	/**
	 * @return the $ly_date
	 */
	public function getLy_date() {
		return $this->ly_date;
	}

	/**
	 * @return the $ly_content
	 */
	public function getLy_content() {
		return $this->ly_content;
	}

	/**
	 * @return the $xs
	 */
	public function getXs() {
		return $this->xs;
	}

	/**
	 * @return the $ls
	 */
	public function getLs() {
		return $this->ls;
	}

	/**
	 * @return the $hf_content
	 */
	public function getHf_content() {
		return $this->hf_content;
	}

	/**
	 * @return the $hf_date
	 */
	public function getHf_date() {
		return $this->hf_date;
	}

	/**
	 * @return the $ly_hf
	 */
	public function getLy_hf() {
		return $this->ly_hf;
	}

	/**
	 * @param field_type $ly_id
	 */
	public function setLy_id($ly_id) {
		$this->ly_id = $ly_id;
	}

	/**
	 * @param field_type $ly_title
	 */
	public function setLy_title($ly_title) {
		$this->ly_title = $ly_title;
	}

	/**
	 * @param field_type $ly_date
	 */
	public function setLy_date($ly_date) {
		$this->ly_date = $ly_date;
	}

	/**
	 * @param field_type $ly_content
	 */
	public function setLy_content($ly_content) {
		$this->ly_content = $ly_content;
	}

	/**
	 * @param field_type $xs
	 */
	public function setXs($xs) {
		$this->xs = $xs;
	}

	/**
	 * @param field_type $ls
	 */
	public function setLs($ls) {
		$this->ls = $ls;
	}

	/**
	 * @param field_type $hf_content
	 */
	public function setHf_content($hf_content) {
		$this->hf_content = $hf_content;
	}

	/**
	 * @param field_type $hf_date
	 */
	public function setHf_date($hf_date) {
		$this->hf_date = $hf_date;
	}

	/**
	 * @param field_type $ly_hf
	 */
	public function setLy_hf($ly_hf) {
		$this->ly_hf = $ly_hf;
	}

	
}
?>