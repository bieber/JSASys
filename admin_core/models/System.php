<?php

class System{
	private $siteName;
	private $siteDescpribt;
	private $siteEmail;
	private $siteEmailPassword;
	private $pageSize;
	private $id;
	private $siteUrl;
	/**
	 * @return the $siteUrl
	 */
	public function getSiteUrl() {
		return $this->siteUrl;
	}

	/**
	 * @param field_type $siteUrl
	 */
	public function setSiteUrl($siteUrl) {
		$this->siteUrl = $siteUrl;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return the $siteName
	 */
	public function getSiteName() {
		return $this->siteName;
	}

	/**
	 * @return the $siteDescpribt
	 */
	public function getSiteDescpribt() {
		return $this->siteDescpribt;
	}

	/**
	 * @return the $siteEmail
	 */
	public function getSiteEmail() {
		return $this->siteEmail;
	}

	/**
	 * @return the $siteEmailPassword
	 */
	public function getSiteEmailPassword() {
		return $this->siteEmailPassword;
	}

	/**
	 * @return the $pageSize
	 */
	public function getPageSize() {
		return $this->pageSize;
	}

	/**
	 * @param field_type $siteName
	 */
	public function setSiteName($siteName) {
		$this->siteName = $siteName;
	}

	/**
	 * @param field_type $siteDescpribt
	 */
	public function setSiteDescpribt($siteDescpribt) {
		$this->siteDescpribt = $siteDescpribt;
	}

	/**
	 * @param field_type $siteEmail
	 */
	public function setSiteEmail($siteEmail) {
		$this->siteEmail = $siteEmail;
	}

	/**
	 * @param field_type $siteEmailPassword
	 */
	public function setSiteEmailPassword($siteEmailPassword) {
		$this->siteEmailPassword = $siteEmailPassword;
	}

	/**
	 * @param field_type $pageSize
	 */
	public function setPageSize($pageSize) {
		$this->pageSize = $pageSize;
	}

	
	
}
?>