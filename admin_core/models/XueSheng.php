<?php
include_once 'ZhuanYe.php';
include_once 'XueYuan.php';
include_once 'BanJi.php';
class XueSheng{
	private $xs_id;
	private $xs_name;
	private $xs_email;
	private $xs_tele;
	private $xs_xb;
	private $zhy;
	private $bj;
	private $xy;
	private $xs_tx;
	private $xs_loginname;
	private $xs_loginpwd;
	private $xs_xh;
	/**
	 * @return the $xs_loginname
	 */
	public function getXs_loginname() {
		return $this->xs_loginname;
	}

	/**
	 * @return the $xs_loginpwd
	 */
	public function getXs_loginpwd() {
		return $this->xs_loginpwd;
	}

	/**
	 * @return the $xs_xh
	 */
	public function getXs_xh() {
		return $this->xs_xh;
	}

	/**
	 * @param field_type $xs_loginname
	 */
	public function setXs_loginname($xs_loginname) {
		$this->xs_loginname = $xs_loginname;
	}

	/**
	 * @param field_type $xs_loginpwd
	 */
	public function setXs_loginpwd($xs_loginpwd) {
		$this->xs_loginpwd = $xs_loginpwd;
	}

	/**
	 * @param field_type $xs_xh
	 */
	public function setXs_xh($xs_xh) {
		$this->xs_xh = $xs_xh;
	}

	/**
	 * @return the $xs_id
	 */
	public function getXs_id() {
		return $this->xs_id;
	}

	/**
	 * @return the $xs_name
	 */
	public function getXs_name() {
		return $this->xs_name;
	}

	/**
	 * @return the $xs_email
	 */
	public function getXs_email() {
		return $this->xs_email;
	}

	/**
	 * @return the $xs_tele
	 */
	public function getXs_tele() {
		return $this->xs_tele;
	}

	/**
	 * @return the $xs_xb
	 */
	public function getXs_xb() {
		return $this->xs_xb;
	}

	/**
	 * @return the $zhy
	 */
	public function getZhy() {
		return $this->zhy;
	}

	/**
	 * @return the $bj
	 */
	public function getBj() {
		return $this->bj;
	}

	/**
	 * @return the $xy
	 */
	public function getXy() {
		return $this->xy;
	}

	/**
	 * @return the $xs_tx
	 */
	public function getXs_tx() {
		return $this->xs_tx;
	}

	/**
	 * @param field_type $xs_id
	 */
	public function setXs_id($xs_id) {
		$this->xs_id = $xs_id;
	}

	/**
	 * @param field_type $xs_name
	 */
	public function setXs_name($xs_name) {
		$this->xs_name = $xs_name;
	}

	/**
	 * @param field_type $xs_email
	 */
	public function setXs_email($xs_email) {
		$this->xs_email = $xs_email;
	}

	/**
	 * @param field_type $xs_tele
	 */
	public function setXs_tele($xs_tele) {
		$this->xs_tele = $xs_tele;
	}

	/**
	 * @param field_type $xs_xb
	 */
	public function setXs_xb($xs_xb) {
		$this->xs_xb = $xs_xb;
	}

	/**
	 * @param field_type $zhy
	 */
	public function setZhy(ZhuanYe $zhy) {
		$this->zhy = $zhy;
	}

	/**
	 * @param field_type $bj
	 */
	public function setBj(BanJi $bj) {
		$this->bj = $bj;
	}

	/**
	 * @param field_type $xy
	 */
	public function setXy(XueYuan $xy) {
		$this->xy = $xy;
	}

	/**
	 * @param field_type $xs_tx
	 */
	public function setXs_tx($xs_tx) {
		$this->xs_tx = $xs_tx;
	}

}
?>