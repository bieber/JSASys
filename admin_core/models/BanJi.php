<?php
include_once 'ZhuanYe.php';
/**
 * description 该类是对班级实体进行封装，对其属性进行方法的封装
 * @author hebibo
 *
 */
class BanJi{
	private $bj_id;    /*编辑编号*/
	private $bj_name;  /*班级名*/
	private $zhy;      /*所属专业（对象）*/
    private $xsCount;

	/**
	 * @return the $xsCount
	 */
	public function getXsCount() {
		return $this->xsCount;
	}

	/**
	 * @param field_type $xsCount
	 */
	public function setXsCount($xsCount) {
		$this->xsCount = $xsCount;
	}

	/**
	 * @param $bj_id
	 * @param $bj_name
	 * @param $zhy
	 */

	/**
	 * 返回编辑的唯一编号
	 */
	public function getBj_id() {
		return $this->bj_id;
	}

	/**
	 * 返回班级名
	 */
	public function getBj_name() {
		return $this->bj_name;
	}

	/**
	 *返回班级所属的专业对象
	 */
	public function getZhy() {
		return $this->zhy;
	}

	/**
	 *设置班级的唯一编码
	 */
	public function setBj_id($bj_id) {
		$this->bj_id = $bj_id;
	}

	/**
	 * 设置班级名
	 */
	public function setBj_name($bj_name) {
		$this->bj_name = $bj_name;
	}

	/**
	 * 设置班级所属专业的对象
	 */
	public function setZhy(ZhuanYe $zhy) {
		$this->zhy = $zhy;
	}

}
?>