<?php
include_once 'XueYuan.php';
class ZhuanYe{
	private $zhy_id;
	private $zhy_name;
	private $zhy_describe;
	private $xy;
	private $bjList;
	private $bjCount;
	/**
	 * @return the $bjCount
	 */
	public function getBjCount() {
		return $this->bjCount;
	}

	/**
	 * @param field_type $bjCount
	 */
	public function setBjCount($bjCount) {
		$this->bjCount = $bjCount;
	}

	/**
	 * @return the $bjList
	 */
	public function getBjList() {
		return $this->bjList;
	}

	/**
	 * @param field_type $bjList
	 */
	public function setBjList( $bjList) {
		$this->bjList = $bjList;
	}

	/**
	 * @return the $zhy_id
	 */
	public function getZhy_id() {
		return $this->zhy_id;
	}

	/**
	 * @return the $zhy_name
	 */
	public function getZhy_name() {
		return $this->zhy_name;
	}

	/**
	 * @return the $zhy_describe
	 */
	public function getZhy_describe() {
		return $this->zhy_describe;
	}

	/**
	 * @return the $xy
	 */
	public function getXy() {
		return $this->xy;
	}

	/**
	 * @param field_type $zhy_id
	 */
	public function setZhy_id($zhy_id) {
		$this->zhy_id = $zhy_id;
	}

	/**
	 * @param field_type $zhy_name
	 */
	public function setZhy_name($zhy_name) {
		$this->zhy_name = $zhy_name;
	}

	/**
	 * @param field_type $zhy_describe
	 */
	public function setZhy_describe($zhy_describe) {
		$this->zhy_describe = $zhy_describe;
	}

	/**
	 * @param field_type $xy
	 */
	public function setXy(XueYuan $xy) {
		$this->xy = $xy;
	}

}
?>