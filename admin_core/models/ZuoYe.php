<?php
include_once 'LaoShi.php';
include_once 'XueSheng.php';
include_once 'WenTi.php';
class ZuoYe{
	private $zy_id;//作业编号
	private $zy_name;//作业名
	private $zy_file;//作业的附件
	private $zy_py;//作业评语
	private $zy_fs;//作业评分
	private $xs;//该作业所属的学生
	private $wt;//该作业所回答的问题
	private $ls;//评改该作业的老师
	private $tj_date;//提交该作业的时间
	private $zy_content;//作业内容
	private $zy_sate;//标识该作业是否已被老师审核
	private $bj;//发表该作业的学生所属的班级
	/**
	 * @return the $bj
	 */
	public function getBj() {
		return $this->bj;
	}

	/**
	 * @param field_type $bj
	 */
	public function setBj($bj) {
		$this->bj = $bj;
	}

	/**
	 * @return the $zy_sate
	 */
	public function getZy_sate() {
		return $this->zy_sate;
	}

	/**
	 * @param field_type $zy_sate
	 */
	public function setZy_sate($zy_sate) {
		$this->zy_sate = $zy_sate;
	}

	/**
	 * @return the $tj_date
	 */
	public function getTj_date() {
		return $this->tj_date;
	}

	/**
	 * @param field_type $tj_date
	 */
	public function setTj_date($tj_date) {
		$this->tj_date = $tj_date;
	}

	/**
//	 * @return the $wt
	 */
	public function getWt() {
		return $this->wt;
	}

	/**
	 * @param field_type $wt
	 */
	public function setWt($wt) {
		$this->wt = $wt;
	}

	/**
	 * @return the $zy_id
	 */
	public function getZy_id() {
		return $this->zy_id;
	}

	/**
	 * @return the $zy_name
	 */
	public function getZy_name() {
		return $this->zy_name;
	}

	/**
	 * @return the $zy_file
	 */
	public function getZy_file() {
		return $this->zy_file;
	}

	/**
	 * @return the $zy_py
	 */
	public function getZy_py() {
		return $this->zy_py;
	}

	/**
	 * @return the $zy_fs
	 */
	public function getZy_fs() {
		return $this->zy_fs;
	}

	/**
	 * @return the $xs
	 */
	public function getXs() {
		return $this->xs;
	}

	
	/**
	 * @return the $ls
	 */
	public function getLs() {
		return $this->ls;
	}

	/**
	 * @return the $zy_content
	 */
	public function getZy_content() {
		return $this->zy_content;
	}

	/**
	 * @param field_type $zy_id
	 */
	public function setZy_id($zy_id) {
		$this->zy_id = $zy_id;
	}

	/**
	 * @param field_type $zy_name
	 */
	public function setZy_name($zy_name) {
		$this->zy_name = $zy_name;
	}

	/**
	 * @param field_type $zy_file
	 */
	public function setZy_file($zy_file) {
		$this->zy_file = $zy_file;
	}

	/**
	 * @param field_type $zy_py
	 */
	public function setZy_py($zy_py) {
		$this->zy_py = $zy_py;
	}

	/**
	 * @param field_type $zy_fs
	 */
	public function setZy_fs($zy_fs) {
		$this->zy_fs = $zy_fs;
	}

	/**
	 * @param field_type $xs
	 */
	public function setXs(XueSheng $xs) {
		$this->xs = $xs;
	}

	

	/**
	 * @param field_type $ls
	 */
	public function setLs(LaoShi $ls) {
		$this->ls = $ls;
	}

	/**
	 * @param field_type $zy_content
	 */
	public function setZy_content($zy_content) {
		$this->zy_content = $zy_content;
	}

}
?>