<?php
class XueYuan{
	private $xy_id;
	private $xy_name;
	private $xy_describe;
	private $zhyList;
	private $zhyCount;

	/**
	 * @return the $zhyList
	 */
	public function getZhyList() {
		return $this->zhyList;
	}

	/**
	 * @return the $zhyCount
	 */
	public function getZhyCount() {
		return $this->zhyCount;
	}

	/**
	 * @param field_type $zhyCount
	 */
	public function setZhyCount($zhyCount) {
		$this->zhyCount = $zhyCount;
	}

	/**
	 * @param field_type $zhyList
	 */
	public function setZhyList($zhyList) {
		$this->zhyList = $zhyList;
	}

	/**
	 * @return the $xy_id
	 */
	public function getXy_id() {
		return $this->xy_id;
	}

	/**
	 * @return the $xy_name
	 */
	public function getXy_name() {
		return $this->xy_name;
	}

	/**
	 * @return the $xy_describe
	 */
	public function getXy_describe() {
		return $this->xy_describe;
	}

	/**
	 * @param field_type $xy_id
	 */
	public function setXy_id($xy_id) {
		$this->xy_id = $xy_id;
	}

	/**
	 * @param field_type $xy_name
	 */
	public function setXy_name($xy_name) {
		$this->xy_name = $xy_name;
	}

	/**
	 * @param field_type $xy_describe
	 */
	public function setXy_describe($xy_describe) {
		$this->xy_describe = $xy_describe;
	}

}
?>