<?php
class News{
	
	private $news_id;
	private $news_title;
	private $news_content;
	private $news_date;
	private $news_department;
	/**
	 * @return the $news_id
	 */
	public function getNews_id() {
		return $this->news_id;
	}

	/**
	 * @return the $news_title
	 */
	public function getNews_title() {
		return $this->news_title;
	}

	/**
	 * @return the $news_content
	 */
	public function getNews_content() {
		return $this->news_content;
	}

	/**
	 * @return the $news_date
	 */
	public function getNews_date() {
		return $this->news_date;
	}

	/**
	 * @return the $news_department
	 */
	public function getNews_department() {
		return $this->news_department;
	}

	/**
	 * @param field_type $news_id
	 */
	public function setNews_id($news_id) {
		$this->news_id = $news_id;
	}

	/**
	 * @param field_type $news_title
	 */
	public function setNews_title($news_title) {
		$this->news_title = $news_title;
	}

	/**
	 * @param field_type $news_content
	 */
	public function setNews_content($news_content) {
		$this->news_content = $news_content;
	}

	/**
	 * @param field_type $news_date
	 */
	public function setNews_date($news_date) {
		$this->news_date = $news_date;
	}

	/**
	 * @param field_type $news_department
	 */
	public function setNews_department($news_department) {
		$this->news_department = $news_department;
	}

}
?>