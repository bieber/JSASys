<?php
/**
 * 
 * description 该类是管理员类，对管理员信息的封装 ，并对其属性进行方法的封装
 * @author hebibo
 *
 */
class Admin{
    private $admin_id;       /*管理员编号*/
	private $admin_name;     /*管理员登录名*/
	private $admin_password; /*管理员登录密码*/
	private $admin_purview;  /*管理员权限*/
	private $admin_email;    /*管理员邮箱*/
	private $last_login;     /*最后登录*/
	private $login_ip;       /*最后登录Ip*/
	
	/**
	 * @param $admin_id
	 * @param $admin_name
	 * @param $admin_password
	 * @param $admin_purview
	 * @param $admin_email
	 * @param $last_login
	 * @param @login_ip
	 */
	/**
	 * 返回管理员编号
	 */
	public function getAdmin_id() {
		return $this->admin_id;
	}
	
	/**
	 * 返回管理员登录名
	 */
	public function getAdmin_name() {
		return $this->admin_name;
	}
	
	/**
	 * 返回管理员登录密码
	 */
	public function getAdmin_password() {
		return $this->admin_password;
	}
	
	/**
	 * 返回管理员权限值
	 */
	public function getAdmin_purview() {
		return $this->admin_purview;
	}
	
	/**
	 * 返回管理员邮箱
	 */
	public function getAdmin_email() {
		return $this->admin_email;
	}
	
	/**
	 * 返回最后登录时间
	 */
	public function getLast_login() {
		return $this->last_login;
	}
	
	/**
	 * 返回最后登录IP
	 */
	public function getLogin_ip() {
		return $this->login_ip;
	}
	
	/**
	 *设置管理员编号
	 */
	public function setAdmin_id($admin_id) {
		$this->admin_id = $admin_id;
	}
	
	/**
	 * 设置管理员登录名
	 */
	public function setAdmin_name($admin_name) {
		$this->admin_name = $admin_name;
	}
	
	/**
	 * 设置管理员登录密码
	 */
	public function setAdmin_password($admin_password) {
		$this->admin_password = $admin_password;
	}
	
	/**
	 *设置管理员权限
	 */
	public function setAdmin_purview($admin_purview) {
		$this->admin_purview = $admin_purview;
	}
	
	/**
	 * 设置管理员邮箱
	 */
	public function setAdmin_email($admin_email) {
		$this->admin_email = $admin_email;
	}
	
	/**
	 * 设置管理员最后登录时间
	 */
	public function setLast_login($last_login) {
		$this->last_login = $last_login;
	}
	
	/**
	 * 设置管理员最后登录IP
	 */
	public function setLogin_ip($login_ip) {
		$this->login_ip = $login_ip;
	}

}
?>