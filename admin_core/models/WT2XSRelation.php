<?php
/**
 * 
 * 类名：WT2XSRelation
 * 功能：描述老师发布的问题对应学生的关系(及指明该作业有哪些学生做)
 * @author Administrator
 *
 */
include_once 'WenTi.php';
include_once 'XueSheng.php';
class WT2XSRelation{
	
	private $xs;
	private $wt;
	private $state;
	private $type;
	private $bj;
	private $fs;
	/**
	 * @return the $fs
	 */
	public function getFs() {
		return $this->fs;
	}

	/**
	 * @param field_type $fs
	 */
	public function setFs($fs) {
		$this->fs = $fs;
	}

	/**
	 * @return the $bj
	 */
	public function getBj() {
		return $this->bj;
	}

	/**
	 * @param field_type $bj
	 */
	public function setBj($bj) {
		$this->bj = $bj;
	}

	/**
	 * @return the $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param field_type $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * @return the $state
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * @param field_type $state
	 */
	public function setState($state) {
		$this->state = $state;
	}

	/**
	 * @return the $xs
	 */
	public function getXs() {
		return $this->xs;
	}

	/**
	 * @return the $wt
	 */
	public function getWt() {
		return $this->wt;
	}

	/**
	 * @param field_type $xs
	 */
	public function setXs(XueSheng $xs) {
		$this->xs = $xs;
	}

	/**
	 * @param field_type $wt
	 */
	public function setWt(WenTi $wt) {
		$this->wt = $wt;
	}

}
?>