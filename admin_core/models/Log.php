<?php
class Log{
private $log_id;
private $log_content;
private $log_people;
private $log_date;
private $log_ip;
/**
	 * @return the $log_ip
	 */
	public function getLog_ip() {
		return $this->log_ip;
	}

/**
	 * @param field_type $log_ip
	 */
	public function setLog_ip($log_ip) {
		$this->log_ip = $log_ip;
	}

	/**
	 * @return the $log_id
	 */
	public function getLog_id() {
		return $this->log_id;
	}

/**
	 * @return the $log_content
	 */
	public function getLog_content() {
		return $this->log_content;
	}

/**
	 * @return the $log_people
	 */
	public function getLog_people() {
		return $this->log_people;
	}

/**
	 * @return the $log_date
	 */
	public function getLog_date() {
		return $this->log_date;
	}

/**
	 * @param field_type $log_id
	 */
	public function setLog_id($log_id) {
		$this->log_id = $log_id;
	}

/**
	 * @param field_type $log_content
	 */
	public function setLog_content($log_content) {
		$this->log_content = $log_content;
	}

/**
	 * @param field_type $log_people
	 */
	public function setLog_people($log_people) {
		$this->log_people = $log_people;
	}

/**
	 * @param field_type $log_date
	 */
	public function setLog_date($log_date) {
		$this->log_date = $log_date;
	}


}
?>