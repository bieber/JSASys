<?php
include_once 'parentPathConfig.php';
include_once $path . '/config.php';
include_once ROOT.'/models/System.php';
/**
 * 
 * Enter description 该类是工具类
 * @author Administrator
 *
 */
/*$patharr = explode('\\',dirname(__FILE__));
$path = $patharr[0].'/'.$patharr[1].'/'.$patharr[2].'/'.$patharr[3];
include_once $path.'/config.php';
include_once ROOT.'/models/Log.php';
include_once ROOT.'/services/LogService.php';*/
class fun {
	
	public $pageSize;
	function __construct() {
		if (isset ( $_SESSION ['system'] )) {
			$system = unserialize ( $_SESSION ['system'] );
			$this->pageSize = $system->getPageSize ();
		}
	}
	
	/**
	 * 
	 * Enter description MD5加密方法
	 * @param unknown_type $str
	 */
	public function StringToMD5($str) {
		return md5 ( md5 ( $str ) );
	}
	/**
	 * 
	 * Enter description 获得设置格式的时间
	 */
	public function getDate() {
		date_default_timezone_set ( "Asia/Shanghai" );
		return date ( "Y-m-d H:i:s" );
	
	}
	/**
	 * 
	 * Enter description 比较两个时间
	 * @param unknown_type $nowTime
	 * @param unknown_type $outTime
	 */
	public function compareTime($nowTime, $outTime) {
		if (strtotime ( $nowTime ) >= strtotime ( $outTime )) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * Enter description 根据总数和每页显示的条数得到页数
	 * @param unknown_type $count
	 * @param unknown_type $pageSize
	 */
	
	public function getPagees($count, $pageSize) {
		$pagees = $count / $pageSize;
		if (ereg ( "\\.", $pagees )) {
			return ( int ) $pagees + 1;
		} else {
			return $pagees;
		}
	
	}
	/**
	 * 
	 * Enter description 返回和当前时间一定间隔的时间
	 * @param unknown_type $hadd
	 * @param unknown_type $madd
	 * @param unknown_type $madd
	 * @param unknown_type $dadd
	 * @param unknown_type $yadd
	 */
	public function addUseTime($hadd, $madd, $madd, $dadd, $yadd) {
		date_default_timezone_set ( "Asia/Shanghai" );
		$y = date ( "Y" );
		$m = date ( "m" );
		$d = date ( "d" );
		$h = date ( "H" );
		$m = date ( "m" );
		$s = date ( "s" );
		return date ( "Y-m-d H:i:s", mktime ( $h + $hadd, $m + $madd, $s, $m + $madd, $d + $dadd, $y + $yadd ) );
	}
	/**
	 * 
	 * Enter description 返回客户端访问的IP地址
	 */
	public function getClientIp() {
		return $_SERVER ["REMOTE_ADDR"];
	}
	/**
	 * 
	 * Enter description 关闭数据库
	 */
	public function closeDB() {
		return mysql_close ();
	}
	/**
	 * 
	 * Enter description 弹出消息，并跳转制定页面
	 * @param unknown_type $content
	 * @param unknown_type $url
	 */
	public function alertMessage($content, $url) {
		
		echo "<html><head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gbk\">
<title>提示</title><script language=\"javascript\">alert('" . $content . "');location.href='" . $url . "';</script> </head></html>";
	}
	public function alertRegistMessage($url) {
		echo "<script language=\"javascript\">location.href='" . $url . "';</script>";
	}
	/**
	 * 
	 * Enter description 检测用户是否登录
	 */
	public function checkLogin() {
		session_start ();
		if (! session_is_registered ( "user" )) {
			echo "<script language=\"javascript\">alert('请先登录！');parent.location.href='../index.php';</script>";
			return ;
		} else if (mktime () - $_SESSION ['time'] > 3600) {
			session_destroy ();
			echo "<script language=\"javascript\">alert('登录超时！请重新登录！');parent.parent.location.href='../index.php';</script>";
			return ;
		} else {
			$_SESSION ['time'] = mktime ();
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的服务对象进行对结果集进行分页
	 * @param unknown_type $obj
	 * @param unknown_type $smarty
	 */
	public function listPage($obj, $smarty) {
		if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getListForPage ( $start, $this->pageSize );
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getLabelCount ();
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllList ();
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getListForPage ( 0, $this->pageSize );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
	}
	/**
	 * 
	 * Enter description 根据传递过来的服务对象进行对结果集进行分页并所有的结果都在一点的时间范围内
	 * @param unknown_type $obj
	 * @param unknown_type $smarty
	 */
	public function listPageByDate($obj, $smarty) {
		
		if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$before = $_GET ['before'];
			$after = $_GET ['after'];
			$smarty->assign('beforeTime',date("Y-m-d H:i:s",$before));
			$smarty->assign('afterTime',date("Y-m-d H:i:s",$after));
			$smarty->assign ( "before", $before );
			$smarty->assign ( "after", $after );
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getListForPageByDate ( $start, $this->pageSize, $before, $after );
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$before = strtotime ( $_GET ['before'] );
			$after = strtotime ( $_GET ['after'] );
			$smarty->assign('beforeTime',date("Y-m-d H:i:s",$before));
			$smarty->assign('afterTime',date("Y-m-d H:i:s",$after));
			$smarty->assign ( "before", $before );
			$smarty->assign ( "after", $after );
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getLabelCountByDate ( $before, $after );
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllByDate ( $before, $after );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getListForPageByDate ( 0, $this->pageSize, $before, $after );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
	}
	/**
	 * 
	 * Enter description 根据某一约束进行分页显示
	 * @param unknown_type $obj
	 * @param unknown_type $smarty
	 * @param unknown_type $id
	 */
	public function listPageById($obj, $smarty, $id=0) {
		if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getListForPageById ( $start, $this->pageSize, $id );
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getLabelCount ( $id );
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllListById ( $id );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getListForPageById ( 0, $this->pageSize, $id );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
	}
	public function listPageByIdForStudent($obj,$smarty)
	{
		$xyId = 0;
		$zhyId =0;
		$bjId = 0;
		$xsName = null;
		if($_GET['xyId'])
		{
			$xyId=$_GET['xyId'];
		}
		if($_GET['zhyId'])
		{
			$zhyId = $_GET['zhyId'];
		}
		if($_GET['bjId'])
		{
			$bjId = $_GET['bjId'];
		}
		if($_GET['xsName'])
		{
			$xsName = $_GET['xsName'];
		}
		$smarty->assign('xyId',$xyId);
		$smarty->assign('zhyId',$zhyId);
		$smarty->assign('bjId',$bjId);
		$smarty->assign('xsName',$xsName);
		if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getListForPageById ( $start, $this->pageSize ,$zhyId,$bjId,$xyId,$xsName);
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getLabelCount ($zhyId,$bjId,$xyId,$xsName );
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllListById ($zhyId,$bjId,$xyId,$xsName );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getListForPageById ( 0, $this->pageSize,$zhyId,$bjId,$xyId,$xsName);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
	}
	/**
	 * 
	 * Enter description 获取当前登录的用户名
	 */
	public function getCurrentAdminName() {
		session_start ();
		$type = $_SESSION['logintype'];
		if($type=='admin')
		{
		if (isset ( $_SESSION ['user'] )) {
			$admin = unserialize ( $_SESSION ['user'] );
			return $admin->getAdmin_name ().'【管理员】';
		} else {
			return "未知用户";
		}
		}
		else if($type == 'ls')
		{
		if (isset ( $_SESSION ['user'] )) {
			$ls = unserialize ( $_SESSION ['user'] );
			return $ls->getLs_name().'【老师】';
		} else {
			return "未知用户";
		}
		}
		else
		{
		if (isset ( $_SESSION ['user'] )) {
			$xs = unserialize ( $_SESSION ['user'] );
			return $xs->getXs_name().'【学生】';
		} else {
			return "未知用户";
		}
		}
	}
	/**
	 * 
	 * Enter description 添加当前用户操作的日志
	 * @param unknown_type $content
	 */
	public function addLog($content) {
		$log = new Log ();
		$log->setLog_content ( $content );
		$log->setLog_ip ( $this->getClientIp () );
		$log->setLog_people ( $this->getCurrentAdminName () );
		date_default_timezone_set ( "Asia/Shanghai" );
		$log->setLog_date ( strtotime ( $this->getDate () ) );
		$logService = new LogService ();
		if ($logService->addLog ( $log )) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * Enter description here 发送邮件的方法
	 * @param unknown_type $smtpemailto 接收方的邮箱
	 * @param unknown_type $content  发送邮件内容
	 * @param unknown_type $mailsubject 发送邮件的主题
	 * @param unknown_type $sendemail 发件人邮箱
	 * @param unknown_type $password 发件人邮箱密码
	 */
	public function sendMail($smtpemailto, $content, $mailsubject, $sendemail, $password) {
		$mailtype = "HTML";
		$email = $sendemail;
		$email = explode ( '@', $email );
		$smtpuser = $email [0]; //登录smtp服务器的用户名
		$email = "smtp." . $email [1]; //获取站长邮箱的smtp服务器
		$smtppassword = $password; //登录smtp服务的密码
		$smtpport = 25; //登录smtp服务的端口
		$smtp = new smtp ( $email, $smtpport, true, $smtpuser, $smtppassword ); //这里面的一个true是表示使用身份验证,否则不使用身份验证.
		$smtp->debug = FALSE; //是否显示发送的调试信息
		if ($smtp->sendmail ( $smtpemailto, $sendemail, $mailsubject, $content, $mailtype )) {
			return true; //发送成功
		} else {
			return false; //发送失败
		}
	}
	
	public function uploadImage($destination_folder, $fileNanme, $max_file_size, $errorURL) {
		$uptypes = array ('image/jpg', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/gif', 'image/bmp', 'image/x-png' ); //允许上传文件格式
		$file = $_FILES [$fileNanme];
		if ($max_file_size < $file ["size"]) //检查文件大小
{
			$this->alertMessage ( "你选择的头像图片太大！不能超过10MB", $errorURL );
		}
		if (! in_array ( $file ["type"], $uptypes )) //检查文件类型
{
			$this->alertMessage ( "你选择的头像图片格式不正确！", $errorURL );
		}
		if (! file_exists ( $destination_folder )) {
			mkdir ( $destination_folder );
		}
		$tmp_name = $file ["tmp_name"];
		$name = $file ["name"];
	    $type = explode(".", $name);
	    $name= mktime().".".$type[count($type)-1];
		move_uploaded_file ( $tmp_name, $destination_folder .$name);
		return $name;
	}
	public function uploadFile($fileNanme,$destination_folder,$errorURL)
	{
		$file = $_FILES [$fileNanme];
		$max_file_size = 25000000;
echo $file ["size"];
		if ($max_file_size < $file ["size"]) //检查文件大小
{
			$this->alertMessage ( "你选择的文件太大！不能超过20MB", $errorURL );
			return ;
		}
	
		if (! file_exists ( $destination_folder )) {
			mkdir ( $destination_folder );
		}
		$tmp_name = $file ["tmp_name"];
		$name = $file ["name"];
	    $type = explode(".", $name);
	    $name= mktime().".".$type[count($type)-1];
	move_uploaded_file ( $tmp_name, $destination_folder .$name);
		return $name;
	}
	public function deleteFile($dir,$fileName)
	{
		$fileName = $dir.$fileName;
		if(unlink($fileName))
		{
			return true;
		}
		else {
			return false;
		}
	}

    public function listPageForHW($obj,$smarty,$start_time = 0, $end_time = 0, $wt_name = null, $lsId = 0, $xyId = 0, $type = 0, $wt_type = 0)
    {
    if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->searchWenTiForPage ( $start, $this->pageSize, $start_time, $end_time, $wt_name , $lsId, $xyId, $type, $wt_type );
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getLabelCountForSearch ( $start_time, $end_time, $wt_name , $lsId, $xyId, $type, $wt_type );
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->searchWenTi ( $start_time, $end_time, $wt_name , $lsId, $xyId, $type, $wt_type );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->searchWenTiForPage ( 0, $this->pageSize, $start_time, $end_time, $wt_name , $lsId, $xyId, $type, $wt_type );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
    }
    public function getListForZY($obj,$smarty,$wt_id=0,$zy_name=null,$xs_id=0,$ls_id=0,$bj_id=0,$zy_state=0,$tj_date=0,$start=0,$end=0)
    {
    	//echo $zy_state;
    if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->searchZuoYeForUserByPage ($wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end, $start, $this->pageSize);
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->searchZuoYeCountForUser ( $wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end );
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->searchZuoYeForUser ($wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end );
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->searchZuoYeForUserByPage ( $wt_id,$zy_name,$xs_id,$ls_id,$bj_id,$zy_state,$tj_date,$start,$end ,0, $this->pageSize);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
    }

    public function getListForLY($obj,$smarty,$xsId, $state,$type=0,$before=0,$after=0)
    {
    if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getAllListForXsByPage ($xsId, $state,  $start, $this->pageSize,$type,$before,$after);
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getCountForXs($xsId, $state,$type,$before,$after);
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllListForXs($xsId, $state,$type,$before,$after);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getAllListForXsByPage ( $xsId, $state,  $start, $this->pageSize,$type,$before,$after);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
    }
    public function getLYListForLs($obj,$smarty,$lsId,$before=0,$after=0)
    {
       if ($_GET ['page']) {
			$page = $_GET ['page'];
			$countPage = $_GET ['countPage'];
			$smarty->assign ( "countPage", $countPage );
			$end = ($page - 1) * $this->pageSize + $this->pageSize;
			$start = ($page - 1) * $this->pageSize;
			$list = $obj->getAllListForLsByPage($lsId, 0, $start, $this->pageSize,$before,$after);
			$smarty->assign ( "list", $list );
			$smarty->assign ( "start", $start + 1 );
			$smarty->assign ( "end", $end );
			$smarty->assign ( "nowPage", $page );
			$smarty->assign ( "count", $_GET ['count'] );
		} else {
			$smarty->assign ( "nowPage", 1 );
			$count = $obj->getCountForLs($lsId, 0,$before,$after);
			$countPage = $this->getPagees ( $count, $this->pageSize );
			$smarty->assign ( "count", $count );
			$smarty->assign ( "countPage", $countPage );
			if ($count < $this->pageSize) {
				$list = $obj->getAllListForLs($lsId, 0,$before,$after);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", count ( $list ) );
			} else {
				$list = $obj->getAllListForLsByPage($lsId, 0, $start, $this->pageSize,$before,$after);
				$smarty->assign ( "list", $list );
				$smarty->assign ( "start", 1 );
				$smarty->assign ( "end", $this->pageSize );
			}
		}
    }
}

?>