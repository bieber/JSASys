<?php
include_once 'TableBak.php';
class DbBak {
var $_mysql_link_id;
var $_dataDir;
var $_tableList;
var $_TableBak;
function DbBak($_mysql_link_id,$dataDir)
{
( (!is_string($dataDir)) || strlen($dataDir)==0) && die('error:$datadir is not a string');
!is_dir($dataDir) && mkdir($dataDir);
$this->_dataDir = $dataDir;
$this->_mysql_link_id = $_mysql_link_id;
}
function backupDb($dbName,$tableName=null)
{
( (!is_string($dbName)) || strlen($dbName)==0 ) && die('$dbName must be a string value');
//step1:选择数据库：
mysql_select_db($dbName);
//step2:创建数据库备份目录
$dbDir = $this->_dataDir.DIRECTORY_SEPARATOR.$dbName;
!is_dir($dbDir) && mkdir($dbDir);
//step3:得到数据库所有表名 并开始备份表
$this->_TableBak = new TableBak($this->_mysql_link_id,$dbDir);
if(is_null($tableName)){//backup all table in the db
$this->_backupAllTable($dbName);
return;
}

}
function restoreDb($dbName,$tableName=null){
( (!is_string($dbName)) || strlen($dbName)==0 ) && die('$dbName must be a string value');
//step1:检查是否存在数据库 并连接：
@mysql_select_db($dbName) || die("the database <b>$dbName</b> dose not exists");
//step2:检查是否存在数据库备份目录
$dbDir = $this->_dataDir.DIRECTORY_SEPARATOR.$dbName;
!is_dir($dbDir) && die("$dbDir not exists");
//step3:start restore
$this->_TableBak = new TableBak($this->_mysql_link_id,$dbDir);
if(is_null($tableName)){//backup all table in the db
$this->_restoreAllTable($dbName);
return;
}
}
function _getTableList($dbName)
{
$tableList = array();
$result=mysql_list_tables($dbName,$this->_mysql_link_id);
for ($i = 0; $i < mysql_num_rows($result); $i++){
array_push($tableList,mysql_tablename($result, $i));
}
mysql_free_result($result);
return $tableList;
}
function _backupAllTable($dbName)
{
foreach ($this->_getTableList($dbName) as $tableName){
$this->_TableBak->backupTable($tableName);
}
}


function _restoreAllTable($dbName)
{
//step1:检查是否存在所有数据表的备份文件 以及是否可写：
foreach ($this->_getTableList($dbName) as $tableName){
$tableBakFile = $this->_dataDir.DIRECTORY_SEPARATOR
. $dbName.DIRECTORY_SEPARATOR
. $tableName.DIRECTORY_SEPARATOR
. $tableName.'.sql';
!is_writeable ($tableBakFile) && die("$tableBakFile not exists or unwirteable");
}
//step2:start restore
foreach ($this->_getTableList($dbName) as $tableName){
$tableBakFile = $this->_dataDir.DIRECTORY_SEPARATOR
. $dbName.DIRECTORY_SEPARATOR
. $tableName.DIRECTORY_SEPARATOR
. $tableName.'.sql';
$this->_TableBak->restoreTable($tableName,$tableBakFile);
}
}


}

?>