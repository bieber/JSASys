<?php
class TeacherControl {
	public function registerTeacher(fun $fun) {
		$validata = $_POST ['validata'];
		if ($validata != $_SESSION ['validate']) {
			$fun->alertMessage ( "验证码错误！", "../teacherRegist.php" );
		} else {
			$loginName = $_POST ['loginName'];
			$pwd = $_POST ['pwd'];
			$rname = $_POST ['rname'];
			$xb = $_POST ['xb'];
			$email = $_POST ['email'];
			$tele = $_POST ['tele'];
			$xy = $_POST ['xy'];
			$tx = $fun->uploadImage ( "../teacherTX/", "tx", 10000000, "../teacherRegist.php" );
			$xyService = new XueYuanService ();
			$xy = $xyService->getXueYuanById ( $xy );
			$ls = new LaoShi ();
			$ls->setLs_name ( $rname );
			$ls->setLs_loginname ( $loginName );
			$ls->setLs_loginpwd ( $fun->StringToMD5 ( $pwd ) );
			$ls->setLs_yz ( 0 );
			$ls->setLs_tele ( $tele );
			$ls->setLs_tx ( $tx );
			$ls->setLs_xb ( $xb );
			$ls->setXy ( $xy );
			$ls->setLs_describe ( "" );
			$ls->setLs_email ( $email );
			$ls->setLs_email ( $email );
			$lsService = new LaoShiService ();
			if ($lsService->addLaoShi ( $ls )) {
				$fun->closeDB ();
				$fun->alertRegistMessage ( "../teacherRegist.html" );
			} else {
				$fun->closeDB ();
				$fun->alertMessage ( "注册失败！请重新注册！", "../teacherRegist.php" );
			}
		}
	}
	
	public function checkTeacher(fun $fun) {
		header ( "content-type:text/html; charset=utf-8" );
		$name = $_GET ['lsName'];
		$oldName = $_GET ['oldName'];
		$name = iconv ( "utf-8", "gbk", $name );
		$oldName = iconv ( "utf-8", "gbk", $oldName );
		if ($oldName == "") {
			$lsService = new LaoShiService ();
			if ($lsService->checkLaoShiByName ( $name )) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($oldName == $name) {
				return false;
			} else {
				$lsService = new LaoShiService ();
				if ($lsService->checkLaoShiByName ( $name )) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
	
	public function getTeachers(Smarty $smarty, fun $fun, $action) {
		$lsService = new LaoShiService ();
		$lsService->setAction ( $action );
		$xyService = new XueYuanService ();
		$smarty->assign ( "xyList", $xyService->getAllList () );
		$smarty->assign ( "action", $action );
		$fun->listPage ( $lsService, $smarty );
		$fun->closeDB ();
		$smarty->display ( "admin/teacherList.html" );
	}
	public function getTeachersById(Smarty $smarty, fun $fun, $action) {
		$lsService = new LaoShiService ();
		$lsService->setAction ( $action );
		$xyService = new XueYuanService ();
		$smarty->assign ( "xyList", $xyService->getAllList () );
		$smarty->assign ( "xyId", $_GET ['xyId'] );
		$smarty->assign ( "action", $action );
		$fun->listPageById ( $lsService, $smarty, $_GET ['xyId'] );
		$fun->closeDB ();
		$smarty->display ( "admin/teacherList.html" );
	}
	public function teacherPassing(fun $fun) {
		
		$lsId = $_GET ['lsId'];
		$lsService = new LaoShiService ();
		$ls = $lsService->getLaoShiById ( $lsId );
		$ls->setLs_yz ( 1 );
		$lsService->updateLaoShi ( $ls );
		session_start ();
		$system = unserialize ( $_SESSION ['system'] );
		$fun->addLog ( "批准老师<" . $ls->getLs_name () . ">加入该系统" );
		$content = "尊敬的" . $ls->getLs_name () . "老师:<br />" . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;恭喜你！你已经通过管理员审核。欢迎加入" . $system->getSiteName () . "！点击下面地址进入系统，如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
		$mailsubject = $system->getSiteName () . "审核通过通知";
		$fun->closeDB ();
		$fun->sendMail ( $ls->getLs_email (), $content, $mailsubject, $system->getSiteEmail (), $system->getSiteEmailPassword () );
		
		$fun->alertMessage ( "操作成功！", "control.php?action=validatedTeachers" );
	
	}
	
	public function deletePassed(fun $fun) {
		$lsId = $_GET ['lsId'];
		$lsService = new LaoShiService ();
		$ls = $lsService->getLaoShiById ( $lsId );
		$ls->setLs_yz ( 2 );
		$lsService->updateLaoShi ( $ls );
		session_start ();
		$system = unserialize ( $_SESSION ['system'] );
		$fun->addLog ( "拒绝老师<" . $ls->getLs_name () . ">加入该系统" );
		$content = "尊敬的" . $ls->getLs_name () . "老师:<br />" . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;对不起！你未通过管理员审核。可能你的信息不正确！请确认后再注册！同时也谢谢你对本系统的支持！点击下面地址重新注册，如果不能进入请将地址复制粘贴到浏览器地址栏中！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
		$mailsubject = $system->getSiteName () . "审核通知";
		$fun->closeDB ();
		$fun->sendMail ( $ls->getLs_email (), $content, $mailsubject, $system->getSiteEmail (), $system->getSiteEmailPassword () );
		$fun->alertMessage ( "操作成功！", "control.php?action=reValidatedTeachers" );
	}
	public function deleteTeacher(fun $fun) {
		$lsId = $_GET ['lsId'];
		$lsService = new LaoShiService ();
		if ($lsService->deleteLaoShi ( $lsId )) {
			$fun->addLog ( "删除一位老师信息" );
			$fun->alertMessage ( "操作成功！", "control.php?action=reValidatedTeachers" );
		} else {
			$fun->alertMessage ( "操作失败！", "control.php?action=reValidatedTeachers" );
		}
	}
	
	public function login(fun $fun) {
		$user_name = $_POST ["username"];
		$user_psw = $_POST ["password"];
		$validate = $_POST ["chknumber"];
		$user_name = addslashes ( $user_name );
		$user_psw = addslashes ( $user_psw );
		$user_psw = $fun->stringToMD5 ( $user_psw );
		$ls = new LaoShi ();
		$ls->setLs_loginname ( $user_name );
		$ls->setLs_loginpwd ( $user_psw );
		$lsService = new LaoShiService ();
		$chineseSpell = new ChineseSpell ();
		$ls = $lsService->checkLaoShiLogin ( $ls );
		if ($validate != $_SESSION ["validate"] && $validate != $chineseSpell->getFullSpell ( $_SESSION ['validate'] )) {
			$fun->closeDB ();
			$fun->alertMessage ( "你输入的验证码错误！请重新出入...", "../index.php" );
			return;
		} else if ($ls) {
			$_SESSION ["user"] = serialize ( $ls );
			$userId = strtotime ( date ( "Y-m-d H:i:s" ) );
			$_SESSION ['userId'] = $userId;
			$_SESSION ['time'] = mktime ();
			$fun->addLog ( "登录成功" );
			$fun->closeDB ();
			$fun->alertMessage ( "欢迎" . $ls->getLs_name () . "使用该系统！", "../main.php" );
		} else {
			$fun->closeDB ();
			$fun->alertMessage ( "输入的登录名、密码错误或者还未通过管理员审核！请重新输入...", "../index.php" );
		}
	}
	
	public function updateLs(fun $fun) {
		$loginName = $_POST ['loginName'];
		$pwd = $_POST ['pwd'];
		$lsService = new LaoShiService ();
		$ls = $lsService->getLaoShiById ( $_POST ['userId'] );
		$ls->setLs_loginname ( $loginName );
		$ls->setLs_loginpwd ( $fun->StringToMD5 ( $pwd ) );
		if ($lsService->updateLaoShi ( $ls )) {
			$_SESSION ["user"] = serialize ( $ls );
			$fun->addLog ( "用户修改密码" );
			$fun->closeDB ();
			$fun->alertMessage ( "修改成功！", "../right.php" );
		} else {
			$fun->addLog ( "用户修改密码失败" );
			$fun->closeDB ();
			$fun->alertMessage ( "修改失败！", "../right.php" );
		}
	}
	public function updateLsInfo(fun $fun) {
		$pwd = $_POST ['pwd'];
		$rname = $_POST ['rname'];
		$xb = $_POST ['xb'];
		$email = $_POST ['email'];
		$tele = $_POST ['tele'];
		$xy = $_POST ['xy'];
		$xyService = new XueYuanService ();
		$xy = $xyService->getXueYuanById ( $xy );
		$ls = unserialize ( $_SESSION ['user'] );
		$ls->setLs_name ( $rname );
		$ls->setLs_tele ( $tele );
		$ls->setLs_describe ( $_POST ['descript'] );
		$file = $_FILES ["tx"];
		if ($file ["name"]) {
			$fun->deleteFile ( "../teacherTX/", $ls->getLs_tx () );
			$ls->setLs_tx ( $fun->uploadImage ( "../teacherTX/", "tx", 10000000, "../modifyLsInfo.php" ) );
		}
		$ls->setLs_xb ( $xb );
		$ls->setXy ( $xy );
		$ls->setLs_email ( $email );
		$lsService = new LaoShiService ();
		if ($lsService->updateLaoShi ( $ls )) {
			$_SESSION ["user"] = serialize ( $ls );
			$fun->addLog ( "用户修改信息" );
			$fun->closeDB ();
			$fun->alertMessage ( "修改成功！", "../right.php" );
		} else {
			$fun->addLog ( "用户修改信息失败" );
			$fun->closeDB ();
			$fun->alertMessage ( "修改失败！", "../right.php" );
		}
	
	}
	public function getZhy(fun $fun) {
		$xyList = $_GET ['xyList'];
		$xyList = explode ( "//", $xyList );
		$zhyService = new ZhuanYeService ();
		$zhyList = array ();
		for($i = 0; $i < count ( $xyList ) - 1; $i ++) {
			$zhyList = array_merge ( $zhyList, $zhyService->getAllListById ( $xyList [$i] ) );
		}
		$fun->closeDB ();
		return $zhyList;
	}
	public function getBj(fun $fun) {
		$zhyList = $_GET ['zhyList'];
		$zhyList = explode ( "//", $zhyList );
		$bjService = new BanJiService ();
		$bjList = array ();
		for($i = 0; $i < count ( $zhyList ) - 1; $i ++) {
			$bjList = array_merge ( $bjList, $bjService->getBanJiByZhy ( $zhyList [$i] ) );
		}
		$fun->closeDB ();
		return $bjList;
	
	}
	public function getHW(Smarty $smarty, fun $fun, $state) {
		$wt_id = 0;
		$zy_name = null;
		$xs_id = 0;
		$ls_id = 0;
		$bj_id = 0;
		$zy_state = 0;
		$tj_date = 0;
		$start_time = 0;
		$end_time = 0;
		if ($_GET ['wt_id']) {
			$wt_id = $_GET ['wt_id'];
		}
		if ($_GET ['zy_name'] && $_GET ['zy_name'] != "null") {
			$zy_name = $_GET ['zy_name'];
		}
		if ($_GET ['xs_id']) {
			$xs_id = $_GET ['xs_id'];
		}
		if ($_GET ['ls_id']) {
			$ls_id = $_GET ['ls_id'];
		}
		if ($_GET ['bj_id']) {
			$bj_id = $_GET ['bj_id'];
		}
		if ($_GET ['zy_state']) {
			$zy_state = $_GET ['zy_state'];
		}
		if ($_GET ['tj_date']) {
			$tj_date = $_GET ['tj_date'];
			$start_time = strtotime ( $_GET ['start_time'] );
			$end_time = strtotime ( $_GET ['end_time'] );
		}
		$ls = unserialize ( $_SESSION ['user'] );
		$ls_id = $ls->getLs_id ();
		$zy_state = $state;
		$zyService = new ZuoYeService ();
		$fun->getListForZY ( $zyService, $smarty, $wt_id, $zy_name, $xs_id, $ls_id, $bj_id, $zy_state, $tj_date, $start_time, $end_time );
		$xyService = new XueYuanService ();
		$wtService = new WenTiService ();
		$smarty->assign ( "wtList", $wtService->searchWenTi ( 0, 0, null, $ls->getLs_id (), $ls->getXy ()->getXy_id (), 0, 2 ) );
		$smarty->assign ( "xyList", $xyService->getAllList () );
		$smarty->assign ( "wt_id", $wt_id );
		$smarty->assign ( "zy_name", $zy_name );
		$smarty->assign ( "xs_id", $xs_id );
		$smarty->assign ( "ls_id", $ls_id );
		$smarty->assign ( "bj_id", $bj_id );
		$smarty->assign ( "zy_sate", $zy_state );
		$smarty->assign ( "tj_date", $tj_date );
		$smarty->assign ( "start_time", $start_time );
		$smarty->assign ( "end_time", $end_time );
		$fun->closeDB ();
		$smarty->display ( "teacher/zyList.html" );
	
	}
	public function needRecive(Smarty $smarty, fun $fun) {
		$before = 0;
		if ($_GET ['before']) {
			$before = $_GET ['before'];
		}
		$after = 0;
		if ($_GET ['after']) {
			$after = $_GET ['after'];
		}
		$lyService = new LiuYanServie ();
		$ls = unserialize ( $_SESSION ['user'] );
		$fun->getlyListForLs ( $lyService, $smarty, $ls->getLs_id (), strtotime ( $before ), strtotime ( $after ) );
		$smarty->assign ( "before", $before );
		$smarty->assign ( "after", $after );
		$fun->closeDB ();
		$smarty->display ( "teacher/leaveWordList.html" );
	}
	public function addHF(fun $fun) {
		$lyId = $_POST ['lyId'];
		$hf = $_POST ['hf'];
		$lyService = new LiuYanServie ();
		$ly = $lyService->getLiuYanById ( $lyId );
		$ly->setHf_content ( $hf );
		$ly->setHf_date ( mktime () );
		$ly->setLy_hf ( 1 );
		if ($lyService->addHuiFu ( $ly )) {
			$system = unserialize ( $_SESSION ['system'] );
			$emailContent = $ly->getXs ()->getXs_name () . "同学：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好：<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;在" . $system->getSiteName () . "！你的留言已得到" . $ly->getLs()->getLs_name () . "老师的回复！请注意查收！点击下面地址进入系统，
				如果不能进入请将地址复制粘贴到浏览器地址栏中登录！<br /><a href=" . $system->getSiteUrl () . ">" . $system->getSiteUrl () . "</a>";
			$fun->sendMail ( $ly->getLs ()->getLs_email (), $emailContent, "留言回复通知", $system->getSiteEmail (), $system->getSiteEmailPassword () );
			$fun->closeDB ();
			$fun->alertMessage ( "回复成功！", "controlLs.php?action=needRecive" );
		} else {
			$fun->closeDB ();
			$fun->alertMessage ( "回复失败！", "controlLs.php?action=needRecive" );
		}
	}
}

?>