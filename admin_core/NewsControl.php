<?php

class NewsControl{
	public function addNews(fun $fun)
	{
		$newsTitle = $_POST['newsTitle'];
		$newsContent = $_POST['newsContent'];
		$newsDepartment = $_POST['newsDepartment'];
		$news = new News();
		$news->setNews_content($newsContent);
		$news->setNews_department($newsDepartment);
		$news->setNews_title($newsTitle);
		date_default_timezone_set ( "Asia/Shanghai" );
		$news->setNews_date(date ( "Y年m月d日" ));
		$newsService = new NewService();
		if($newsService->addNews($news))
		{
			$fun->addLog("添加新闻<".$news->getNews_title().">");
			$fun->closeDB();
			$fun->alertMessage("添加成功！", "control.php?action=newsList");
		}
		else
		{
			$fun->addLog("添加新闻<".$news->getNews_title().">失败");
			$fun->closeDB();
			$fun->alertMessage("添加失败，请重新添加！", "../addNews.php");
		}
	}
	public function newsList(Smarty $smarty,fun $fun)
	{
		
		$newService = new NewService();
		$fun->listPage($newService, $smarty);
		$smarty->assign("logintype",$_SESSION['logintype']);
		$fun->closeDB();
		$smarty->display("admin/newsList.html");
	}
	public function deletNewsBatch(fun $fun)
	{
		$newsList = $_POST['newsId'];
		$newsService = new NewService();
		if($newsService->deleteNewses($newsList))
		{
			$fun->addLog("删除新闻");
			$fun->closeDB();
			$fun->alertMessage("删除成功！", "control.php?action=newsList");
		}
		else{
			$fun->addLog("删除新闻失败");
			$fun->closeDB();
			$fun->alertMessage("删除失败！", "control.php?action=newsList");
		}
	}
    public function updateNews(fun $fun)
    {
    	$newsService = new NewService();
    	$newsTitle = $_POST['newsTitle'];
		$newsContent = $_POST['newsContent'];
		$newsDepartment = $_POST['newsDepartment'];
		$news = new News();
		$news->setNews_content($newsContent);
		$news->setNews_department($newsDepartment);
		$news->setNews_title($newsTitle);
		date_default_timezone_set ( "Asia/Shanghai" );
		$news->setNews_date(date ( "Y年m月d日" ));
		$news->setNews_id($_POST['newsId']);
    	if($newsService->updateNews($news))
    	{
    		$fun->addLog("修改新闻<".$news->getNews_title().">");
    		$fun->closeDB();
    		$fun->alertMessage("修改成功！", "control.php?action=newsList");
    	}
    	else
    	{
    			$fun->addLog("修改新闻<".$news->getNews_title().">失败");
    		$fun->closeDB();
    		$fun->alertMessage("修改失败！", "control.php?action=newsList");
    	}
    }
	}
?>