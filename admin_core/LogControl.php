<?php
class LogControl{
	public function logList(Smarty $smarty,fun $fun)
	{
		$logService = new LogService();
		$fun->listPage($logService, $smarty);
		$fun->closeDB();
		$smarty->display("admin/logList.html");
	}
	public function deleteLogBetch(fun $fun)
	{
		$logList = $_POST['logId'];
		$logService = new LogService();
		if($logService->deleteLogBetch($logList))
		{
			$fun->addLog("批量删除系统日志");
			$fun->closeDB();
			$fun->alertMessage("日志删除成功！", "control.php?action=logList");
		}
		else
		{
			$fun->addLog("批量删除系统日志失败");
			$fun->closeDB();
			$fun->alertMessage("日志删除失败！", "control.php?action=logList");
		}
	}
	public function searchLogByDate(Smarty $smarty,fun $fun)
	{
		$logService = new LogService();
		$fun->listPageByDate($logService, $smarty);
		$fun->closeDB();
		$smarty->display("admin/logList.html");
	}
}
?>