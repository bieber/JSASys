<?php 
include_once 'admin_core/models/BanJi.php';
include_once 'admin_core/services/BanJiService.php';
include_once 'admin_core/models/XueSheng.php';
include_once 'admin_core/services/XueShengService.php';
include_once 'admin_core/utils/Function.php';
$bjId = $_GET['bjId'];
$bjService = new BanJiService();
$bj = $bjService->getBanJiById($bjId);
$xsService = new XueShengService();
$xsList = $xsService->getAllListById(0,$bjId,0,0);
$fun = new fun();
$fun->closeDB();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>[<?php echo $bj->getZhy()->getZhy_name();?>]<?php echo $bj->getBj_name();?>所有学生情况</title>
<script type="text/javascript">
function checkStudentDetail(xsId)
	{
		window.open('readStudentDetail.php?xsId='+xsId,'学生信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');
		}
			function changeBackColor(obj)
{
obj.style.backgroundColor="#F3F3F3";

	}
	function removeBackColor(obj)
	{
		obj.style.backgroundColor="#FFFFFF";
		}
			function preview(oper){
	if (oper < 10){
bdhtml=window.document.body.innerHTML;//获取当前页的html代码
sprnstr="<!--startprint-->";//设置打印开始区域
eprnstr="<!--endprint-->";//设置打印结束区域
prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html

prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html
window.document.body.innerHTML=prnhtml;
window.print();
window.document.body.innerHTML=bdhtml;


} else{
window.print();
}

}
</script>
<style type="text/css">
a{
	text-decoration:none;}
	img{
		border:0px;}
</style>
</head>

<body>
<center>
<br />
<!--startprint-->
<table  width="60%" style="border:1px #333 solid; border-collapse:collapse;">
<caption style="font-size:15px; font-weight:bold; color:#000;">[<?php echo $bj->getZhy()->getZhy_name();?>]<?php echo $bj->getBj_name();?>学生列表信息<br />
该班总共有个<font color=red><?php echo $bj->getXsCount();?></font>人
</caption>
<tr style="background-color:#999;">
<th style=" font-size:14px; text-align:center; border:1px #333 solid; height:25px; line-height:25px;" width="20%">
学生姓名
</th>
<th  style=" font-size:14px; text-align:center; border:1px #333 solid; height:25px; line-height:25px;" width="10%">
性别
</th>
<th  style=" font-size:14px; text-align:center; border:1px #333 solid; height:25px; line-height:25px;" width="10%">
学号
</th>
<th  style=" font-size:14px; text-align:center; border:1px #333 solid; height:25px; line-height:25px;">
查看详细信息
</th>
</tr>
<?php 
for($i=0; $i<count($xsList); $i++)
{
?>
<tr  onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:13px; text-align:center; border:1px #333 solid; color:#666; line-height:20px; height:20px;">
<?php 
echo $xsList[$i]->getXs_name();
?>
</td>
<td style=" font-size:13px; text-align:center; border:1px #333 solid; color:#666; line-height:20px; height:20px;">
<?php 
if($xsList[$i]->getXs_xb()==1)
{
?>
男
<?php 
}else 
{
?>
女
<?php 
}
?>
</td>
<td style=" font-size:13px; text-align:center; border:1px #333 solid;  color:#666;line-height:20px; height:20px;">
<?php 
echo $xsList[$i]->getXs_xh();
?>
</td>
<td style=" font-size:13px; text-align:center; border:1px #333 solid; color:#666; line-height:20px; height:20px;">
<a href="#" onclick="checkStudentDetail(<?php echo $xsList[$i]->getXs_id();?>)"> <img src="images/user-comment-green.gif" width="14" height="14" />&nbsp;查看其详细信息</a>
</td>
</tr>
<?php 
}
?>
</table>
<!--endprint-->
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]&nbsp;<img src="images/print_16x16.gif" width="16" height="16" onclick="preview(0)" style="cursor:pointer; border:0px;" title="打印学生名单" alt="打印学生名单"  />
</center>
</body>
</html>
