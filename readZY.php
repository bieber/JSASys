<?php 
session_start();
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/services/WenTiService.php';
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/models/ZuoYe.php';
include_once 'admin_core/services/ZuoYeService.php';

$zyId =$_GET['zyId'];
$zyService = new ZuoYeService();
$zy = $zyService->getZuoYeById($zyId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提交作业</title>
<style type="text/css">
img{
	border:0px;}
</style>
 <link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="uploadify/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"  src="js/comment.js"></script>
<style type="text/css">
table
{
	border:1px #06C solid;
	border-collapse:collapse;
	}
	td{
		border:1px #06C solid;
		}
</style>
</head>

<body>
<center>

<div style="width:99% ; height:20px; border-top:2px dashed #666; border-bottom:2px dashed #666; font-size:16px; color:#999;font-weight:bold; margin-top:5px; margin-bottom:5px; line-height:20px;">
作业内容
</div>

<table width="90%">
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业名：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;">
<?php echo $zy->getWt()->getWt_name();?>

</td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业类型：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php 
if($zy->getWt()->getWt_type()==0)
{
	echo "选做";
}
else
{
	echo "必做";
}
?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业描述：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php 
echo $zy->getWt()->getWt_describe();
?></td>
</tr>
<?php 
$fileName = $zy->getZy_file();

$fileName = explode(".", $fileName);
$name = $fileName[count($fileName)-1];
?>
<?php 
if($zy->getZy_file()!='none')
{
?>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C; ">文件格式为：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"><?php echo $name;?></td>
</tr>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业附件：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;"> 

<a href="studentHomework/<?php echo $zy->getZy_file();?>" style="text-decoration: none;" target="_blank"><img src="fileimages/<?php echo $name.".png";?>" alt="附件"  width="80" height="80"/>
【<img src="images/last_16x16.gif" width="14" height="14" />下载】</a>
</td>
</tr>
<?php 
}
?>
<tr>
<td width="15%" style="text-align:right; font-size:14px; color:#09C;">作业内容：</td>
<td align="left" style="font-size:14px; color:#09C;">
<div style="padding:5px; height:200px; width:89%;overflow:auto; ">
<?php 
echo $zy->getZy_content();
?>
</div>
</td>
</tr>

</table>
<div style="width:99% ; height:20px; border-top:2px dashed #666; border-bottom:2px dashed #666; font-size:16px; color:#999;font-weight:bold; margin-top:5px; margin-bottom:5px; line-height:20px;">
批改内容
</div>

<table width="90%" style="border:0px;">

<tr>
<td width="9%" style="text-align:right; font-size:14px; color:#09C; border:0px;">得分：</td>
<td align="left" style="font-size:14px; color:#09C; border:0px;">

<?php 
echo $zy->getZy_fs();
?> 分
</td>
</tr>
<?php 
if($zy->getZy_py()!='none')
{
?>
<tr id="py" style="display:none;">
<td width="7%" style="text-align:right; font-size:14px; color:#09C; border:0px;">评语：</td>
<td align="left" style="font-size:14px; color:#09C; border:0px;">
<div style="padding:5px; height:200px; width:80%;overflow:auto; ">
<?php 
$zy->getZy_py();
?>
</div>

</td>
</tr>
<?php 
}
?>
</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
