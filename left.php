<?php 
  session_start();
include_once 'admin_core/models/Admin.php';

  ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/chili-1.7.pack.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>
<script type="text/javascript" src="js/jquery.dimensions.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<script language="javascript">
	jQuery().ready(function(){
		jQuery('#navigation').accordion({
			header: '.head',
			navigation1: true, 
			event: 'click',
			fillSpace: true,
			animated: 'bounceslide'
		});
	 	$(".ui-accordion li a").bind("click",function(){
		 	var href=$(this).attr('href');
		 	if(href){
		 	window.parent.document.getElementById('rightFrame').src=href;
		 	}
		 });
	});
</script>
<style type="text/css">
<!--
body {
	margin:0px;
	padding:0px;
	font-size: 12px;
}
#navigation {
	margin:0px;
	padding:0px;
	width:147px;
}
#navigation a.head {
	cursor:pointer;
	background:url(images/main_34.gif) no-repeat scroll;
	display:block;
	font-weight:bold;
	margin:0px;
	padding:5px 0 5px;
	text-align:center;
	font-size:12px;
	text-decoration:none;
}
#navigation ul {
	border-width:0px;
	margin:0px;
	padding:0px;
	text-indent:0px;
}
#navigation li {
	list-style:none; display:inline;
}
#navigation li li a {
	display:block;
	font-size:12px;
	text-decoration: none;
	text-align:center;
	padding:3px;
}
#navigation li li a:hover {
	background:url(images/tab_bg.gif) repeat-x;
		border:solid 1px #adb9c2;
}
-->
</style>
</head>
<body>
<div  style="height:100%;">
  <ul id="navigation">
   <li> <a class="head">系统版本信息</a>
      <ul style="text-align: center;">
        <li>TES Beta1.8.0</li>
      </ul>
    </li>
  <?php 

  $logintype = $_SESSION['logintype'];
  if($logintype == "admin")
  {
  	$admin = unserialize($_SESSION['user']);
  ?>
 <?php 
 if($admin->getAdmin_purview()>=2)
 {
 ?>
  <li> <a class="head">学院/部门管理空间</a>
      <ul>
        <li><a href="addXueYuan.php" target="rightFrame">添加学院/部门</a></li>
        <li><a href="admin_core/control.php?action=xyList" target="rightFrame">查看所有学院/部门信息</a></li>
      </ul>
    </li>
    <li>
    <?php 
 }
  if($admin->getAdmin_purview()>2)
 {
    ?>
     <a class="head">管理员空间</a>
      <ul>
        <li><a href="addAdmin.html" target="rightFrame">添加管理员</a></li>
        <li><a href="admin_core/control.php?action=adminList" target="rightFrame">查看所有管理员</a></li>
      </ul>
    </li>
    <?php 
 }
  if($admin->getAdmin_purview()>=2)
 {
    ?>
     <li>
     <a class="head">老师管理空间</a>
      <ul>
        <li><a href="admin_core/control.php?action=waitValidateTeachers" target="rightFrame">待审核老师</a></li>
          <li><a href="admin_core/control.php?action=validatedTeachers" target="rightFrame">已通过审核老师</a></li>
          <li><a href="admin_core/control.php?action=reValidatedTeachers" target="rightFrame">未通过审核老师</a></li>
      </ul>
   </li>
  
    <li>
     <a class="head">学生管理空间</a>
      <ul>
        <li><a href="admin_core/control.php?action=studentList" target="rightFrame">已注册学生</a></li>
      </ul>
   </li>
   <li>
     <a class="head">老师发布作业管理</a>
      <ul>
        <li><a href="admin_core/control.php?action=getHomeworkList" target="rightFrame">老师已发布的作业</a></li>
      </ul>
   </li>
   <li>
     <a class="head">学生提交作业管理</a>
      <ul>
        <li><a href="admin_core/control.php?action=getZyList" target="rightFrame">学生已提交的作业</a></li>
      </ul>
   </li>
    <?php 
 }
 if($admin->getAdmin_purview()>=1)
 {
   ?>
     <li>
     <a class="head">站内公告管理</a>
      <ul>
        <li><a href="addNews.php" target="rightFrame">添加公告</a></li>
        <li><a href="admin_core/control.php?action=newsList" target="rightFrame">公告列表</a></li>
      </ul>
   </li>
   <?php 
 }
 if($admin->getAdmin_purview()>=3)
 {
   ?>
   <li>
     <a class="head">系统管理空间</a>
      <ul>
        <li><a href="setSystem.php" target="rightFrame">修改系统设置</a></li>
          <li><a href="admin_core/control.php?action=logList" target="rightFrame">系统日志</a></li>
          <li><a href="bakup.html" target="rightFrame" onclick="return confirm('确定要备份吗？就会覆盖上次备份数据！');">系统数据备份</a></li>
             <li><a href="restore.html" target="rightFrame" onclick="return confirm('确定要还原吗？会将最后一次备份数据进行还原！');">系统数据还原</a></li>
      </ul>
   </li>
   <?php 
  }
  }
  else if($logintype == "ls")
  {
   ?>
   <li>
     <a class="head">作业管理空间</a>
      <ul>
        <li><a href="addHomework.php" target="rightFrame">发布作业</a></li>
          <li><a href="admin_core/controlLs.php?action=searchHW" target="rightFrame">已发布的作业</a></li>
      </ul>
   </li>
   <li>
     <a class="head">提交作业管理空间</a>
      <ul>
        <li><a href="admin_core/controlLs.php?action=getUndoHW" target="rightFrame">未审核的作业</a></li>
          <li><a href="admin_core/controlLs.php?action=getDoneHW" target="rightFrame">已审核并通过的作业</a></li>
           <li><a href="admin_core/controlLs.php?action=getReDoHW" target="rightFrame">需重做的作业</a></li>
      </ul>
   </li>
   <li>
     <a class="head">我的留言板</a>
      <ul>
          <li><a href="admin_core/controlLs.php?action=needRecive" target="rightFrame">需回复的留言</a></li>
      </ul>
   </li>
   <?php 
  }
  else if($logintype == "xs")
  {
   ?>
    <li>
     <a class="head">待作业管理</a>
      <ul>
        <li><a href="getPositionHW.php" target="rightFrame">提交指定作业</a></li>
        <li><a href="admin_core/controlXs.php?action=searchHw" target="rightFrame">选择作业提交</a></li>
        
      </ul>
   </li>
   <li>
     <a class="head">已做作业管理</a>
      <ul>
        <li><a href="admin_core/controlXs.php?action=getUnread" target="rightFrame">还未审核作业</a></li>
    <li><a href="admin_core/controlXs.php?action=getPassed" target="rightFrame">审核已通过作业</a></li>
      <li><a href="admin_core/controlXs.php?action=getUnPassed" target="rightFrame">审核未通过作业</a></li>
      </ul>
      </li>
         <li>
     <a class="head">老师留言管理</a>
      <ul>
       <li><a href="addLeaveWordForSelectLs.php" target="rightFrame">给老师留言</a></li>
    <li><a href="admin_core/controlXs.php?action=getUnRecive" target="rightFrame">未回复留言</a></li>
      <li><a href="admin_core/controlXs.php?action=getRecived" target="rightFrame">已回复留言</a></li>
      </ul>
      </li>
   <?php 
  }
   ?>
   
  </ul>
</div>
</body>
</html>
