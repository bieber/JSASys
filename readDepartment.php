<?php 
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/services/ZhuanYeService.php';
$xyId = $_GET['xyId'];
$xyService = new XueYuanService();
$xy = $xyService->getXueYuanById($xyId);
$zhyService = new ZhuanYeService();
$zhyList = $zhyService->getAllListById($xyId);
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>查看学院/部门信息</title>
<script type="text/javascript">
function readMajorDetail(zhyId)
{
	window.open('readMajor.php?zhyId='+zhyId,'查看专业信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
	}
</script>
</head>

<body>
<center>
<table width="80%" style=" border:#666 1px solid; border-collapse:collapse; font-size:13px; color:#999;">
<caption>
学院/部门信息
</caption>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">学院/部门名：</td>
<td align="left" style=" border:#666 1px solid; "><?php echo $xy->getXy_name();?></td>

</tr>
<?php 
if(count($zhyList)>0)
{
?>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">学院所含专业：</td>
<td align="left" style=" border:#666 1px solid; ">
<table>
<?php 
for($i=0; $i<count($zhyList); $i++)
{
?>
<tr>
<td style="color:#990; padding-left:5px;cursor:pointer;" onclick="readMajorDetail(<?php echo $zhyList[$i]->getZhy_id();?>)" title="查看该专业详细信息" >
<?php 
echo $zhyList[$i]->getZhy_name();
?>
</td>
</tr>
<?php 
}
?>
</table>
</td>

</tr>
<?php 
}
?>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">学院/部门简介：</td>
<td align="left" style=" border:#666 1px solid; ">
<div style="width:80%; height:200px; overflow:auto;">
<?php 
echo $xy->getXy_describe();
?>
</div>
</td>

</tr>



</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
