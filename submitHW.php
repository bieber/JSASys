<?php 
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/services/WenTiService.php';
include_once 'admin_core/models/WT2XSRelation.php';
include_once 'admin_core/services/WT2XSRelationService.php';
include_once 'admin_core/models/BanJi.php';
include_once 'admin_core/services/BanJiService.php';
include_once 'admin_core/utils/Function.php';
$hwId= $_GET['hwId'];
$wt2Relation = new WT2XSRelationService();
$wtService = new WenTiService();
if($_GET['bjId'])
{
	$bjId = $_GET['bjId'];
	$bjService = new BanJiService();
	$bj = $bjService->getBanJiById($bjId);
	$wt=$wtService->getWenTiById($hwId,$bjId);
	$rList = $wt2Relation->getRelationListByBJandWT($hwId,$bjId,$wt);

}
else {
	$wt=$wtService->getWenTiById($hwId);
	$rList = $wt2Relation->getRelationListByWT($hwId,$wt);
	
}
$bjList = $wt->getBjList();
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>作业《<?php echo $wt->getWt_name();?>》提交情况</title>
<style type="text/css">
a{
	text-decoration:none;}
</style>
<script type="text/javascript">
function checkStudentDetail(xsId)
	{
		window.open('readStudentDetail.php?xsId='+xsId,'学生信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');
		}
		function changeBackColor(obj)
{
obj.style.backgroundColor="#F3F3F3";

	}
	function removeBackColor(obj)
	{
		obj.style.backgroundColor="#FFFFFF";
		}
		function preview(oper){
	if (oper < 10){
bdhtml=window.document.body.innerHTML;//获取当前页的html代码
sprnstr="<!--startprint-->";//设置打印开始区域
eprnstr="<!--endprint-->";//设置打印结束区域
prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html

prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html
window.document.body.innerHTML=prnhtml;
window.print();
window.document.body.innerHTML=bdhtml;


} else{
window.print();
}

}
</script>
</head>

<body>
<center>
<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">作业<?php 
if($_GET['bjId'])
{
echo " 《".$wt->getWt_name()."》 "."[".$bj->getZhy()->getZhy_name()."]".$bj->getBj_name();
}
else 
{
	echo "《".$wt->getWt_name()."》"."所有班级";
}
?>
提交情况</span></legend>
<span style="font-size:14px;color:#9C3;">
<?php 
if($_GET['bjId'])
{
echo "[".$bj->getZhy()->getZhy_name()."]".$bj->getBj_name();
}
?>
本次作业的最高分：<font color="#FF0000";><?php echo $wt->getMax();?>分</font>&nbsp;最低分：<font color="#FF0000";><?php echo $wt->getMin();?>分</font>&nbsp;平均分：<font color="#FF0000";><?php echo $wt->getAvg();?>分</font></span><br />
<span style="font-size:14px; color:#666;">本次作业指定的班级：</span><span style="font-size:14px; color:#9C3;">
<?php 
if(count($bjList)>0)
{
for($i=0; $i<count($bjList); $i++)
{
	echo "<a href='submitHW.php?hwId=$hwId&bjId=".$bjList[$i]->getBj_id()."' title='查看该班的提交情况'>[".$bjList[$i]->getZhy()->getZhy_name().']'.$bjList[$i]->getBj_name()."</a>&nbsp;&nbsp;&nbsp;";
}}
else
{
	echo "你未选择班级！";
}
?>
</span>
<span style="font-size:14px; color:#9C3;">
<a href='submitHW.php?hwId=<?php echo $hwId;?>' title='查看所有提交情况'>查看所有提交情况</a>
<img src="images/print_16x16.gif" width="16" height="16" onclick="preview(0)" style="cursor:pointer; border:0px;" title="打印成绩" alt="打印成绩"  />
</span>
<!--startprint-->
<table width="80%" style="border:1px #333 solid; border-collapse:collapse;">
<tr style="background-color:#999;">
<th style="border:1px #333 solid; width:10%; text-align:center; font-size:16px;">姓名</th>
<th style="border:1px #333 solid; width:30%; text-align:center; font-size:16px;">班级</th>
<th style="border:1px #333 solid; width:20%; text-align:center; font-size:16px;">是否提交作业</th>
<th style="border:1px #333 solid; width:20%; text-align:center; font-size:16px;">分数</th>
<th style="border:1px #333 solid; text-align:center; font-size:16px;">查看学生信息</th>
</tr>
<?php 
for($i=0; $i<count($rList); $i++)
{
?>
<tr onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style="font-size:13px;border:1px #333 solid; text-align:left; padding-left:5px; color:#666;"><?php echo $rList[$i]->getXs()->getXs_name();?></td>
<td style="font-size:13px;border:1px #333 solid; text-align:left; color:#666; padding-left:5px;"><?php echo "[".$rList[$i]->getXs()->getBj()->getZhy()->getZhy_name()."]".$rList[$i]->getXs()->getBj()->getBj_name();?></td>
<td style="font-size:13px;border:1px #333 solid; text-align:center; color:#666;"><?php 
if($rList[$i]->getState()==0)
{
	echo "未提交";
}
else if($rList[$i]->getState()==1) {
	echo "已提交";
}
else 
{
	echo "重做";
	}
?></td>
<td style="font-size:13px;border:1px #333 solid; text-align:center; color:#666;">
<?php echo $rList[$i]->getFs();?>分
</td>
<td style="font-size:13px;border:1px #333 solid; text-align:center; color:#666;">
<a href="#" onclick="checkStudentDetail(<?php echo $rList[$i]->getXs()->getXs_id();?>)"> 
<img src="images/user-comment-green.gif" width="14" height="14" border="0" />&nbsp;点击查看学生信息
</a>
</td>
</tr>
<?php 
}
?>
</table>
<!--endprint-->
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</fieldset>

</center>
</body>
</html>
