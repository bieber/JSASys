<?php
session_start();
include_once 'admin_core/models/XueSheng.php';
$xs = unserialize($_SESSION["user"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
input{
	color:#3CC;
	width:200px;
	height:20px;
	}
-->
</style>
<script type="text/javascript"  src="js/comment.js"></script>
<script src="js/regist.js" charset="utf-8"></script>
<script type="text/javascript"  src="js/ajax.js" charset="utf-8"></script>
<script type="text/javascript">
function submitForm()
{
	if(document.getElementById("loginName").value=="")
	{
		alert("登录名不能为空！");
		document.getElementById("loginName").focus();
		}
		else if(document.getElementById('pwd').value=="")
		{
			alert("密码不能为空！");
		document.getElementById("pwd").focus();
			}
			else if(document.getElementById('pwdAgain').value=="")
			{
				alert("密码不不一致!");
		document.getElementById("pwdAgain").focus();
				}
				else if(document.getElementById('pwd').value!=document.getElementById('pwdAgain').value)
				{
					alert("密码不不一致!");
		document.getElementById("pwdAgain").focus();
					}
		else
		{
			document.getElementById("bjForm").submit();
		}
}

</script>
</head>

<body style="text-align:center;">
<center>

<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">修改个人密码</span></legend>
<form action="admin_core/controlXs.php?action=updateXs" method="post" id="bjForm"> 
<table width="100%">
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">用户登录名：</td>
<td align="left" width="88%">
<input name="loginName" id="loginName" type="text" value="<?php echo $xs->getXs_loginname();?>" onfocus="lightBorder(this);alertMessage(this);" onkeyup="checkValue(this)" onblur="checkValue(this)"/>
<input type="hidden" id="namestate" value="0" />
<input name="userId" id="userId" type="hidden" value="<?php echo $xs->getXs_id();?>" />
<input name="oldName" id="oldName" type="hidden" value="<?php echo $xs->getXs_loginname();?>" />
<input name="checktype" id="checktype" type="hidden" value="0" />
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">新密码：</td>
<td align="left" width="88%">
<input type="password" name="pwd" id="pwd" class="textFile"  onkeyup="checkValue(this)" onblur="checkValue(this)" onfocus="lightBorder(this);alertMessage(this)" />
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">确认密码：</td>
<td align="left" width="88%">
<input type="password" name="pwdAgain" id="pwdAgain" class="textFile" onkeyup="checkValue(this)" onblur="checkValue(this)"  onfocus="lightBorder(this);alertMessage(this)" /> 
</td>
</tr>
<tr>
<td width="12%" style="text-align:right; font-size:14px; color:#09C;">

</td>
<td align="left">
<input type="button" value=" 修 改 "   id="submitid"  onclick="submitForm()"  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px; color:#000;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
<input type="reset" value=" 重 置 "  onmousemove="changeButton(this,'url(images/61.gif)','#fff')" style="background-image:url(images/40.gif); color:#000; width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)','#09C')" />
</td>
</tr>
</table>
</form>
</fieldset>
</center>
</body>
</html>
