<?php 
session_start();
include_once 'admin_core/services/XueShengService.php';
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/services/ZhuanYeService.php';
include_once 'admin_core/services/BanJiService.php';
include_once 'admin_core/services/WT2XSRelationService.php';
include_once 'admin_core/utils/Function.php';
$xsService = new XueShengService();
$xsId = $_GET['xsId'];
$xs = $xsService->getXueShengById($xsId);

$fun = new fun();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>学生信息</title>
<script type="text/javascript">
function readDepartment(xyId)
		{
			window.open('readDepartment.php?xyId='+xyId,'查看学院/部门信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
			}
			function getStudentList(bjId)
	{
		window.open('getStudentListByBJ.php?bjId='+bjId,'班级详细信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
		}
</script>
</head>

<body>
<div style=" height:50px;">
</div>
<center>
<table width="80%" style=" border:#666 1px solid; border-collapse:collapse; font-size:13px; color:#999;">
<caption>
学生信息
</caption>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">学生姓名：</td>
<td align="left" style=" border:#666 1px solid; padding-left:5px;"><?php echo $xs->getXs_name();?></td>

</tr>
<?php 
$logintype = $_SESSION['logintype'];
if($logintype=='admin')
{
?>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">学生登录名：</td>
<td align="left" style=" border:#666 1px solid; "><?php echo $xs->getXs_loginname();?>&nbsp;&nbsp;
</td>
</tr>
<?php 
}
?>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">头像：</td>
<td align="left" style=" border:#666 1px solid; padding-left:5px; ">
<img src="studentTX/<?php echo $xs->getXs_tx();?>" style="width:100px; height:120px; border:0px;" />
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">性别：</td>
<td align="left" style=" border:#666 1px solid;padding-left:5px; ">
<?php 
if($xs->getXs_xb()==1)
{
	echo "男";
}
else {
	echo "女";
}
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">学号：</td>
<td align="left" style=" border:#666 1px solid;padding-left:5px; ">
<?php 
echo $xs->getXs_xh();
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">历史提交作业情况：</td>
<td align="left" style=" border:#666 1px solid;padding-left:5px; ">
<table>
<?php 
$wxrService = new WT2XSRelationService();
echo "<tr><td>必做已提交作业次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 1, 1)."次</td><td>未通过次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 1, 2)."次 </td><td>未提交次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 1, 0)."次</td></tr>";
echo "<tr><td>选做已提交作业次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 0, 1)."次</td><td>未通过次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 0, 2)."次</td><td>未提交次数：".$wxrService->geteRelationsForXS($xs->getXs_id(), 0, 0)."次</td></tr>";
echo "<tr><td>必做作业平均分：".$wxrService->getAVGForXs($xs->getXs_id(), 1)."分</td><td>最高分：".$wxrService->getMAXForXs($xs->getXs_id(), 1)."分</td><td>最低分：".$wxrService->getMINForXs($xs->getXs_id(), 1)."分</td></tr>";
echo "<tr><td>选做作业平均分：".$wxrService->getAVGForXs($xs->getXs_id(), 0)."分</td><td>最高分：".$wxrService->getMAXForXs($xs->getXs_id(), 0)."分</td><td>最低分：".$wxrService->getMINForXs($xs->getXs_id(),0)."分</td></tr>";
$fun->closeDB();
?>
</table>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">联系电话：</td>
<td align="left" style=" border:#666 1px solid; padding-left:5px;">
<?php 
 
echo $xs->getXs_tele();
?>
</td>

</tr>
<tr>

<td width="20%" align="right" style=" border:#666 1px solid; ">Email：</td>
<td align="left" style=" border:#666 1px solid; padding-left:5px; ">
<a href="mailto:<?php 
echo $xs->getXs_email();
?>" title="发送邮件">
<?php 
echo $xs->getXs_email();
?>
</a>
</td>

</tr><tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">所属学院/部门：</td>
<td align="left" style=" border:#666 1px solid; padding-left:5px; color:#990;cursor:pointer;" title="查看学院/部门信息" onclick="readDepartment(<?php echo $xs->getXy()->getXy_id(); ?>)">
<?php 
echo $xs->getXy()->getXy_name();
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">所属专业班级：</td>
<td align="left" style=" border:#666 1px solid;padding-left:5px; color:#990;cursor:pointer;" onclick="getStudentList(<?php echo $xs->getBj()->getBj_id();?>)" title="查看该班详细信息">

<?php 
echo $xs->getZhy()->getZhy_name().$xs->getBj()->getBj_name();
?>
</td>

</tr>

</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
