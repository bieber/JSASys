<?php 
session_start();
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/models/LiuYan.php';
include_once 'admin_core/services/LiuYanService.php';
$lyId =$_GET['lyId'];
$lyService = new LiuYanServie();
$ly = $lyService->getHuiFuById($lyId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提交作业</title>
<style type="text/css">
img{
	border:0px;}
</style>
 <link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="uploadify/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"  src="js/comment.js"></script>
<style type="text/css">
table
{
	border:1px #06C solid;
	border-collapse:collapse;
	}
	td{
		border:1px #06C solid;
		}
</style>

</head>

<body>
<center>
<h3>查看留言信息</h3>
<div style="width:90% ; height:20px;  font-size:16px; color:#999;font-weight:bold; margin:auto; line-height:20px; text-align:left; padding-left:60px;">留言内容：</div>

<table width="850">
<tr>
<td width="20%" style="text-align:right; font-size:14px; color:#09C;">留言主题：</td>
<td align="left" style="font-size:14px; color:#09C; padding-left:5px;">
<?php echo $ly->getLy_title();?>

</td>
</tr>

<tr>
<td width="20%" style="text-align:right; font-size:14px; color:#09C;">留言内容：</td>
<td align="left" style="font-size:14px; color:#09C;">
<div style="padding:5px; height:200px; width:700px;overflow:auto; ">
<?php 
echo $ly->getLy_content();
?>
</div>
</td>
</tr>

</table>
<div style=" height:20px; width:90%; font-size:16px; color:#999;font-weight:bold; margin:auto;line-height:20px; text-align:left; padding-left:60px;">回复：</div>

<table width="850" style="border:0px;">


<?php 
if($ly->getLy_hf()==0)
{
?>
<tr>
<td style="text-align:center; font-size:14px; color:#09C;">还未回复！</td>
</tr>
<?php 
}else
{
?>
<tr>
<td width="20%" style="text-align:right; font-size:14px; color:#09C;">回复内容：</td>
<td align="left" style="font-size:14px; color:#09C;">
<div style="padding:5px; height:200px; width:700px;overflow:auto; ">
<?php 
echo $ly->getHf_content();
?>
</div>
</td>
</tr>
<?php 
}
?>
</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
