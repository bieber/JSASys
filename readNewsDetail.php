<?php 
include_once 'admin_core/services/NewsService.php';
include_once 'admin_core/utils/Function.php';
$newsService = new NewService();
$newsId = $_GET['newsId'];
$news = $newsService->getNewsById($newsId);
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新闻信息</title>
</head>

<body>
<div style=" height:100px;">
</div>
<center >
<span style=" font-size:20px; font-weight:bold; color:#666;"><?php echo $news->getNews_title();?></span><span style="font-size:13px; color:#999;">——[<?php echo $news->getNews_date()?>]</span><br />
<span style="font-size:14px; font-weight:bold; color:#666;">发布部门：<?php echo $news->getNews_department();?></span><br />
<div style="width:80%; margin:auto; text-align:left;">
<?php echo $news->getNews_content();?>
</div>

<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
