<?php 
session_start();
include_once 'admin_core/services/LaoShiService.php';
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
$lsService = new LaoShiService();
$lsId = $_GET['lsId'];
$ls = $lsService->getLaoShiById($lsId);
$fun = new fun();
$fun->closeDB();
$logintype = $_SESSION['logintype'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>老师信息</title>
<script type="text/javascript">
function readDepartment(xyId)
		{
			window.open('readDepartment.php?xyId='+xyId,'查看学院/部门信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
			}
			function addLeaveWord(lsId)
			{
				window.open('addLeaveWord.php?lsId='+lsId,'给老师留言' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
				}
</script>
</head>

<body>
<div style=" height:50px;">
</div>
<center>
<table width="80%" style=" border:#666 1px solid; border-collapse:collapse; font-size:13px; color:#999;">
<caption>
老师信息
</caption>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">老师姓名：</td>
<td align="left" style=" border:#666 1px solid; "><?php echo $ls->getLs_name();?>&nbsp;&nbsp;
<?php 
if($logintype=='xs')
{
?>
<span>[<a href="#" onclick="addLeaveWord(<?php echo $lsId;?>)" style=" text-decoration:none;">留言</a>]</span>
<?php 
}
?>
</td>

</tr>
<?php 
if($logintype=='admin')
{
?>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">老师登录名：</td>
<td align="left" style=" border:#666 1px solid; "><?php echo $ls->getLs_loginname();?>&nbsp;&nbsp;
</td>
</tr>
<?php 
}
?>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">头像：</td>
<td align="left" style=" border:#666 1px solid; ">
<img src="teacherTX/<?php echo $ls->getLs_tx();?>" style="width:100px; height:120px; border:0px;" />
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">性别：</td>
<td align="left" style=" border:#666 1px solid; ">
<?php 
if($ls->getLs_xb()==1)
{
	echo "男";
}
else {
	echo "女";
}
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">联系电话：</td>
<td align="left" style=" border:#666 1px solid; ">
<?php 

echo $ls->getLs_tele();
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">Email：</td>
<td align="left" style=" border:#666 1px solid; ">
<a href="mailto:<?php 
echo $ls->getLs_email();
?>" title="发送邮件">
<?php 
echo $ls->getLs_email();
?>
</a>
</td>

</tr><tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">所属学院/部门：</td>
<td align="left" style=" border:#666 1px solid; color:#990; cursor:pointer;" title="查看学院/部门信息" onclick="readDepartment(<?php echo $ls->getXy()->getXy_id(); ?>)">
<?php 
echo $ls->getXy()->getXy_name();
?>
</td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">简介：</td>
<td align="left" style=" border:#666 1px solid; ">
<div style="width:80%; height:100px; overflow:auto;">
<?php 
echo $ls->getLs_describe();
?>
</div>
</td>

</tr>

</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
