<?php
session_start();
include_once 'admin_core/models/Admin.php';
include_once 'admin_core/models/LaoShi.php';
include_once 'admin_core/models/XueSheng.php';
if(session_is_registered("user"))
{
	
	$myAdmin = unserialize($_SESSION["user"]); 
}
else
{
	echo "<script language=\"javascript\">alert('请先登录！');parent.location.href='index.php';</script>";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>头部页面</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {
	font-size: 12px;
	color: #000000;
}
.STYLE5 {font-size: 12}
.STYLE7 {font-size: 12px; color: #FFFFFF; }
.STYLE7 a{font-size: 12px; color: #FFFFFF; }
a img {
	border:none;
}
-->
</style>
<script type="text/javascript" src="js/comment.js">


</script>
</head>

<body onload="getTime()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="57" background="images/main_03.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="378" height="57" background="images/main_01.gif">&nbsp;</td>
        <td>&nbsp;</td>
        <td width="281" valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="33" height="27"><img src="images/main_05.gif" width="33" height="27" /></td>
            <td width="248" background="images/main_06.gif"><table width="225" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td height="17"><div align="right">
                <?php 
                $type = $_SESSION['logintype'];
                if($type == 'admin')
                {
                ?>
                <a href="admin_core/control.php?action=modifyAdmin" target="rightFrame">
                <?php 
                }
                else if($type == 'ls')
                {
                ?>
                <a href="modifyLs.php" target="rightFrame">
                <?php 
                }else 
                {
                ?>
                 <a href="modifyXs.php" target="rightFrame">
                <?php 
                }?>
                <img src="images/pass.gif" width="69" height="17" /></a></div></td>
                <td><div align="right">
                <?php 
                if($type == 'admin')
                {
                ?>
                <a href="admin_core/control.php?action=adminInfo" target="rightFrame">
                <?php 
                }else if($type == 'ls')
                {
                ?>
                 <a href="modifyLsInfo.php" target="rightFrame">
                <?php 
                }
				else 
                {
            
                ?>
                  <a href="modifyXsInfo.php" target="rightFrame">
                <?php 
                }
                ?>
                <img src="images/user.gif" width="69" height="17" /></a></div></td>
                <td><div align="right"><a href="admin_core/control.php?action=exit" target="_parent" onclick="return confirm('确定要退出吗？');"><img src="images/quit.gif" alt=" " width="69" height="17" /></a></div></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="40" background="images/main_10.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="194" height="40" background="images/main_07.gif">&nbsp;</td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="21"><img src="images/main_13.gif" width="19" height="14" /></td>
            <td width="35" class="STYLE7"><div align="center"><a href="right.php" target="rightFrame">首页</a></div></td>
            <td width="21" class="STYLE7"><img src="images/main_15.gif" width="19" height="14" /></td>
            <td width="35" class="STYLE7"><div align="center"><a href="javascript:history.go(-1);">后退</a></div></td>
            <td width="21" class="STYLE7"><img src="images/main_17.gif" width="19" height="14" /></td>
            <td width="35" class="STYLE7"><div align="center"><a href="javascript:history.go(1);">前进</a></div></td>
            <td width="21" class="STYLE7"><img src="images/main_19.gif" width="19" height="14" /></td>
            <td width="35" class="STYLE7"><div align="center"><a href="javascript:window.parent.location.reload();">刷新</a></div></td>
            <td width="21" class="STYLE7"><img src="images/main_21.gif" width="19" height="14" /></td>
            <td width="35" class="STYLE7"><div align="center"><a href="help.html" target="rightFrame">帮助</a></div></td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td width="248" background="images/main_11.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="16%"><span class="STYLE5"></span></td>
            <td width="75%"><div align="center"><span class="STYLE7" id="time" style=" font-size: 9px;"></span></div></td>
            <td width="9%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30" background="images/main_31.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="8" height="30"><img src="images/main_28.gif" width="8" height="30" /></td>
        <td width="147" background="images/main_29.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="24%">&nbsp;</td>
            <td width="43%" height="20" valign="bottom" class="STYLE1">管理菜单</td>
            <td width="33%">&nbsp;</td>
          </tr>
        </table></td>
        <td width="39"><img src="images/main_30.gif" width="39" height="30" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="20" valign="bottom"><span class="STYLE1">
     <?php 
     $logintype = $_SESSION['logintype'];
     if($logintype == 'admin')
     {
     ?>       
            当前登录用户：<?php
            echo $myAdmin->getAdmin_name();?> &nbsp;用户角色：<?php 
            if ($myAdmin->getAdmin_purview()==3)
            {
            	?>
            	超级管理员
            	<?php 
            }else if ($myAdmin->getAdmin_purview()==2)
            {
            ?>
            管理员
            <?php 
            }else if ($myAdmin->getAdmin_purview()>=1)
            {
            ?>
            普通管理员
            <?php
            }
     }else if($logintype == 'ls')
     {
            ?>
            
            欢迎<?php echo $myAdmin->getLs_name();?>老师使用该系统
            <?php 
     }else if($logintype == "xs")
     {
            ?>
             欢迎<?php echo $myAdmin->getXs_name();?>同学使用该系统
             <?php 
     }
             ?>
            </span></td>
            <td valign="bottom" class="STYLE1"><div align="right"></div></td>
          </tr>
        </table></td>
        <td width="17"><img src="images/main_32.gif" width="17" height="30" /></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
