【声明】：本系统仅供学习参考，不可用于商业用途。本系统的版权只属于《江西理工大学信息工程学院软件081》所有！如本作者发现将采取法律行为！不得使用本系统从事违法行为！如有次行为，与本人无关！谢谢使用本系统！如对本系统有建议或者疑问可联系：1184350505@qq.com
【系统说明】：本系统是采用PHP+mysql+wamp开发
（1）对wamp的配置
需要打开php_gd2
（2）数据库配置
本系统连接数据库的文件在项目目录的admin_core\utils\mysql_class.php，部署前需修改连接数据库的信息，同时需要在admin_core\control.php配置备份数据库连接信息，代码如下：
$connectid = mysql_connect('localhost','root','root');
mysql_query ( "set names 'gbk'" ) or die ( "设置失败...." );
$backupDir = '../bakup';
$DbBak = new DBBak($connectid,$backupDir);
$DbBak->backupDb('task_examination_db');
$fun->addLog("备份数据库");
$fun->closeDB();
+++++++++++++++++++++++++++++++++++++++++++++++++++
$connectid = mysql_connect('localhost','root','root');
mysql_query ( "set names 'gbk'" ) or die ( "设置失败...." );
$backupDir = '../bakup';
$DbBak = new DbBak($connectid,$backupDir);
$DbBak->restoreDb('task_examination_db');
$fun->addLog("恢复数据库");
$fun->closeDB();
（3）数据库结构
本系统的数据库结构在项目目录的task_examination_db.sql文件，数据库名和文件名一样（除去.sql）
系统的初始管理员名为：jxustie
密码为：jxustie
将sql文件中的所有信息导入数据库即使