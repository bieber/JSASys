<?php 
session_start();
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/services/WenTiService.php';
include_once 'admin_core/models/BanJi.php';
include_once 'admin_core/services/BanJiService.php';
include_once 'admin_core/utils/Function.php';
$hwId= $_GET['hwId'];
$wtService= new WenTiService();
$wt=$wtService->getWenTiById($hwId);
$bjList = $wt->getBjList();
$fun = new fun();
$fun->closeDB();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>《<?php echo $wt->getWt_name();?>》作业信息</title>
<script type="text/javascript">
function changeBackColor(obj)
{
obj.style.backgroundColor="#CCC";

	}
	function removeBackColor(obj)
	{
		obj.style.backgroundColor="#F3F3F3";
		}
			function getStudentList(bjId)
	{
		window.open('getStudentListByBJ.php?bjId='+bjId,'班级详细信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
		}
</script>
<style type="text/css">
a{
	text-decoration:none;}
</style>
</head>

<body>
<center>
<br />
<table width="80%" style="border:1px #333 solid; border-collapse:collapse;">
<caption style="font-size:15px; font-weight:bold; color:#000;">《<?php echo $wt->getWt_name();?>》作业详细信息</caption>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业标题：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left"><?php echo $wt->getWt_name();?></td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业类型：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
<?php 
if($wt->getWt_type()==0)
{
?>
选做
<?php 
}
else
{
?>
必做
<?php 
}
?>
</td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业指定班级：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
<?php 
if(count($bjList)>0)
{
for($i=0; $i<count($bjList); $i++)
{
	if($_SESSION['logintype']!='xs')
	{
	echo "<a href='#' title='查看该班的提交情况' onclick='getStudentList(".$bjList[$i]->getBj_id().")'>[".$bjList[$i]->getZhy()->getZhy_name().']'.$bjList[$i]->getBj_name()."</a>&nbsp;&nbsp;&nbsp;";
	}
	else
	{
		echo "[".$bjList[$i]->getZhy()->getZhy_name().']'.$bjList[$i]->getBj_name()."&nbsp;&nbsp;&nbsp;";
	}
	}}
else
{
	echo "你未选择班级！";
}
?>
</td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">发布作业老师：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
<?php 
echo $wt->getLs()->getLs_name();
?>
</td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业来自的学院/部门：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
<?php 
echo $wt->getXy()->getXy_name();
?>
</td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业提交时间/截至时间：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
开始提交时间：<?php echo $wt->getStart_time();?>&nbsp;&nbsp;截至提交时间：<?php echo $wt->getEnd_time();?>
</td>
</tr>
<tr style="background-color:#F3F3F3; " onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style=" font-size:14px; border:1px #333 solid;color:#666; font-weight:bold; height:25px; line-height:25px;" width="25%" align="right">作业描述：</td>
<td style=" font-size:14px; border:1px #333 solid; color:#960;height:25px; line-height:25px; padding-left:10px;" align="left">
<div style="width:80%; height:200px; overflow:auto;">
<?php 
echo $wt->getWt_describe();
?>
</div>
</td>
</tr>
</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
