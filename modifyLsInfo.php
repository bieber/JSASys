<?php 
session_start();
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/models/LaoShi.php';
include_once 'admin_core/models/System.php';
include_once 'admin_core/utils/Function.php';
include 'fckeditor/fckeditor.php';
$sBasePath = $_SERVER['PHP_SELF'];
$sBasePath = dirname($sBasePath).'/fckeditor/';
$ed=new FCKeditor('descript');
$ed->BasePath=$sBasePath;
$ed->Height=150;
$ed->Width='100%';
$ed->ToolbarSet='Basic';

$ls = unserialize($_SESSION["user"]);
$xyService = new XueYuanService();
$xyList = $xyService->getAllList();
$ed->Value = $ls->getLs_describe();
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>作业提交审查管理系统注册 Powered By 江西理工大学 信息工程学院 软件081班</title>
<script src="js/CJL.0.1.min.js" charset="gbk"></script>
<script src="js/ImagePreview.js" charset="gbk"></script>
<script src="js/regist.js" charset="utf-8"></script>
</head>
<style type="text/css">
#main{
	width:800px;
	margin:auto;}
	#blank{
		width:800px;
		}
		#form
		{
			width:600px;
			margin:auto;
			height:auto;}
			#head
			{
				background-image:url(images/login_12.gif);
				background-repeat:repeat-x;
				height:97px;
				text-align:center;
				color:#09F;
				font-size:24px;
				font-weight:bolder;
				line-height:97px;
				font-family:"宋体";
				border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				}
				.label{
					color:#09F;
					font-size:14px;
					font-family:"幼圆";
					text-align:right;
				}
				table
				{
					
					width:90%;
					margin:auto;}
					.textFile{
						width:200px;
						height:20px;
						border:1px solid #39F;
						text-align:left;
						margin-bottom:5px;
						color:#39F;}
						#top{
							height:7px;}
							#regist{
								border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				
			
								}
								select{
									font-size:12px;}
						
					
</style>
<script type="text/javascript">

function changeButton(obj,image)
{
obj.style.backgroundImage = image;
	}

</script>
<script src="js/registAjax.js" type="text/javascript" charset="gbk"></script>
<script src="js/ajax.js" type="text/javascript" charset="utf-8"></script>

<body>
<div id="main">
<div id="blank">
<fieldset>
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">修改个人信息</span></legend>
<form name="regist" action="admin_core/controlLs.php?action=updateLsInfo" method="post" id="registForm" enctype="multipart/form-data">
<table>
<tr>
<td class="label">
真实姓名：
</td>
<td>
<input type="text" name="rname" id="rname" readonly="readonly" value="<?php echo $ls->getLs_name();?>" class="textFile" onmousemove="addBackGround(this)"  onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)" />
<input type="hidden" id="loginName" value="none111" />
<input type="hidden" id="pwd" value="none11" />
<input type="hidden" id="pwdAgain" value="none11" />
<input type="hidden" id="validata" value="none11" />
<input type="hidden" id="namestate" value="1" />
</td>
</tr>
<tr>
<td class="label">性别：
</td>
<td class="label" style=" text-align:left;">
<input type="radio" name="xb" id="xb" value="1"  
<?php 
if($ls->getLs_xb()==1)
{
?>
checked="checked"
<?php 
}
?>
/> 男 
<input type="radio" name="xb" value="2" 
<?php 
if($ls->getLs_xb()==2)
{
?>
checked="checked"
<?php 
}
?>
 /> 女
</td>
</tr>
<tr>
<td class="label">
Email(邮箱)：
</td>
<td>
<input type="text" name="email" id="email" value="<?php echo $ls->getLs_email();?>" class="textFile" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)" />
</td>
</tr>
<tr>
<td class="label">电话(手机)：
</td>
<td>
<input type="text" name="tele" id="tele" class="textFile" value="<?php echo $ls->getLs_tele();?>" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)" />
</td>
</tr>

<tr>
<td class="label">学院：
</td>
<td align="left">
<select class="textFile" name="xy" id="xy" style="text-align:center;" onchange="getZhy(this)">
<option  value="none" >=========请 选 择=========</option>
<?php 
for($i = 0; $i<count($xyList); $i++)
{
?>
<option style=" text-align:center;" value="<?php echo $xyList[$i]->getXy_id();?>"
<?php 
if($ls->getXy()->getXy_id()==$xyList[$i]->getXy_id())
{
?>
selected="selected"
<?php 
}
?>
><?php echo $xyList[$i]->getXy_name();?></option>
<?php 
}
?>
</select>
</td>
</tr>
<tr>
<td class="label">个人简介：
</td>
<td>
<?php 
$ed->Create();
?>
</td>
</tr>
<tr>
  <td class="label">头像：
  </td>
  <td>
  <img id="file" />
  <img id="image" src="teacherTX/<?php echo $ls->getLs_tx();?>" width="100" height="100" />
  <input id="upload" name="tx" type="file"   style="width:200px;height:20px;border:1px solid #39F;margin-bottom:5px;" onmousemove="addBackGround(this)" onmouseout="removeBackGround(this)" onchange="hiddenImage()" />
    
  </td>
</tr>
<tr>
<td align="right">

</td>
<td>
<input type="button" value=" 修 改 " name="rigist" id="submitid" onclick="submitForm()" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)')" />
<input type="reset" value=" 重 置 " name="reset" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)')" />
</td>
</tr>
</table>
<script>
var ip = new ImagePreview( $$("upload"), $$("file"), {
											maxWidth:100 ,maxHeight:100
});
ip.img.src = ImagePreview.TRANSPARENT;
ip.file.onchange = function(){ ip.preview(); document.getElementById("image").style.display = "none";};
</script>
</form>

</fieldset>
</div>
</div>
</body>
</html>
