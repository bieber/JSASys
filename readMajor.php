<?php 
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/services/ZhuanYeService.php';
$zhyId = $_GET['zhyId'];

$zhyService = new ZhuanYeService();
$zhy = $zhyService->getZhuanYeById($zhyId);
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>查看学院/部门信息</title>
</head>

<body>
<center>
<table width="80%" style=" border:#666 1px solid; border-collapse:collapse; font-size:13px; color:#999;">
<caption>
专业信息
</caption>
<tr>
<td width="30%" align="right" style=" border:#666 1px solid; ">专业名：</td>
<td align="left" style=" border:#666 1px solid; "><?php echo $zhy->getZhy_name();?></td>

</tr>
<tr>
<td width="20%" align="right" style=" border:#666 1px solid; ">专业简介：</td>
<td align="left" style=" border:#666 1px solid; ">
<div style="width:80%; height:200px; overflow:auto;">
<?php 
echo $zhy->getZhy_describe();
?>
</div>
</td>

</tr>



</table>
<br />
[<a href="#" onclick="window.close();" style="font-size:13px;">关闭</a>]
</center>
</body>
</html>
