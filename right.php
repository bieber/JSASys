<?php 
session_start();
include_once 'admin_core/services/NewsService.php';
include_once 'admin_core/utils/Function.php';
$loginType = $_SESSION['logintype'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
function changeBackColor(obj)
{
obj.style.backgroundColor="#F2F2F2";
	}
	function removeBackColor(obj)
	{
		obj.style.backgroundColor="#fff";
		}
	function checkNewsDetail(newsId)
	{
		window.open('readNewsDetail.php?newsId='+newsId,'新闻信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
		}
</script>
</head>

<body style="text-align:center;">
<center>
<div style="width:100%; height:50px;">
</div>
<div style="width:65%; float:left; font-size:13px;" >
<?php 
if($loginType=="admin")
{
?>
<fieldset>
<legend><span style="color:#333; font-size:15px; font-weight:bold;">服务器属性：</span></legend>
<table style="width:100%;  border: 1px #CCC solid; border-collapse:collapse; border-spacing:0 0;">
<tr style="border: 1px #CCC solid; background:#CCC;">
<td style="border: 1px #CCC solid;font-weight:bold; color:#666; text-align:center;  height:20px; line-height:20px;">
检查项目
</td>
<td style="border: 1px #CCC solid;  font-weight:bold; color:#666; text-align:center;  height:20px; line-height:20px;">
当前环境
</td>
<td style="border: 1px #CCC solid;  font-weight:bold; color:#666; text-align:center;  height:20px; line-height:20px;">
建议
</td>

</tr>
<tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">操作系统</td>
<td style="border: 1px #CCC solid;   color:#090; text-align:center;  height:20px; line-height:20px;">
<?php echo PHP_OS;?>
</td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;"><p>Windows_NT/Linux<br />/Freebsd</p></td>

</tr>
<tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">WEB 服务器</td>
<td style="border: 1px #CCC solid;  color:#090; text-align:center;  height:20px; line-height:20px;"><?php echo $_SERVER['SERVER_SOFTWARE'];?></td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;">Apache/Nginx/IIS</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">PHP 版本</td>
<td style="border: 1px #CCC solid;   color:#090;  text-align:center;  height:20px; line-height:20px;">
<?php 
if(phpversion()>'5.2.0')
echo "PHP ".PHP_VERSION;
else {
	echo "PHP版本过低！";
}?>
</td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;">PHP 5.2.0 及以上</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">MYSQL 扩展</td>
<td style="border: 1px #CCC solid;   color:#090;  text-align:center;  height:20px; line-height:20px;">
<?php if(extension_loaded('mysql'))
{
	echo '√';
}
else {
	echo "<span style='color:red;'>X  未开启！建议开启该项设置！</span>";
}
?>
</td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;">必须开启</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">ICONV/MB_STRING 扩展</td>
<td style="border: 1px #CCC solid;    color:#090; text-align:center;  height:20px; line-height:20px;">
<?php 
if(extension_loaded('mbstring'))
{
	echo '√';
}
else {
	echo '<span style=\'color:red;\'>X  该项为开启！</span>';
}
?>
</td>
<td style="border: 1px #CCC solid; color:#F60; text-align:center;  height:20px; line-height:20px;">必须开启</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">JSON扩展</td>
<td style="border: 1px #CCC solid;    color:#090; text-align:center;  height:20px; line-height:20px;">
<?php 
if(extension_loaded('json'))
{
	if(function_exists('json_decode')&&function_exists('json_encode'))
	{
		echo '√';
	}
	else {
		echo "<span style='color:red;'>该项未开启！</span>";
	}
}
?>
</td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;">必须开启</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">GD 扩展</td>
<td style="border: 1px #CCC solid;    color:#090; text-align:center;  height:20px; line-height:20px;">
<?php if(extension_loaded('gd'))
{
	echo '√';
	if(function_exists('imagepng'))
	{
		echo '支持'.'(png';
	}
	if(function_exists('imagejpeg'))
	{
		echo ' jpeg';
	}
	if(function_exists('imagegif'))
	{
		echo ' gif)';
	}
	
}
else {
	echo "<span style='color:red;'>X  未开启！建议开启该项设置！</span>";
}
?>
</td>
<td style="border: 1px #CCC solid; color:#F60; text-align:center;  height:20px; line-height:20px;">建议开启</td>

</tr><tr style="border: 1px #CCC solid; ">
<td style="border: 1px #CCC solid;   color:#666; text-align:center;  height:20px; line-height:20px;">ZLIB 扩展</td>
<td style="border: 1px #CCC solid;   color:#090; text-align:center;  height:20px; line-height:20px;">
<?php 
if(extension_loaded('zip'))
{
	echo '√';
}
else
{
	echo '该项未开启！';
}
?>
</td>
<td style="border: 1px #CCC solid;  color:#F60; text-align:center;  height:20px; line-height:20px;">建议开启</td>

</tr>

</table>
</fieldset>
<?php 
}
else
{
	$newsService = new NewService();
	$newsList = $newsService->getListForPage(0, 10);
	$fun = new fun();
	$fun->closeDB();
?>
<fieldset>
<legend><span style="color:#333; font-size:15px; font-weight:bold;">站内新闻公告
<?php 
if(count($newsList)>0)
{
?>
( <a href="admin_core/control.php?action=newsList"> 更多新闻</a>)
<?php 
}
?>
</span></legend>
<table style="width:100%;  border: 1px #CCC solid; border-collapse:collapse; border-spacing:0 0;">
<tr style="border: 1px #CCC solid; background:#CCC;">
<td style="border: 1px #CCC solid;font-weight:bold; color:#666; text-align:center;width:15%;">
新闻标题
</td>
<td style="border: 1px #CCC solid;  font-weight:bold; color:#666; text-align:center;width:10%;">
发布部门
</td>
<td style="border: 1px #CCC solid;  font-weight:bold; color:#666; text-align:center;">
内容概要
</td>
<td style="border: 1px #CCC solid;  font-weight:bold; color:#666; text-align:center;width:12%;">
发布时间
</td>
</tr>
<?php 
if(count($newsList)>0)
{
for($i=0; $i<count($newsList); $i++)
{
?>
<tr onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style="border: 1px #CCC solid; color:#666; text-align:center; width:15%;">
<a href="#" onclick="checkNewsDetail(<?php echo $newsList[$i]->getNews_id();?>)" style="color:#666;">
<?php if (strlen($newsList[$i]->getNews_title())<=10)
echo $newsList[$i]->getNews_title();
else
echo  substr($newsList[$i]->getNews_title(),0,10)."......";
?></a>
</td>
<td style="border: 1px #CCC solid;  color:#666; text-align:center; width:10%;">
<?php echo $newsList[$i]->getNews_department();?>
</td>
<td style="border: 1px #CCC solid;   color:#666; text-align:center; ">
<a href="#" onclick="checkNewsDetail(<?php echo $newsList[$i]->getNews_id();?>)" style="color:#666;"><?php echo substr($newsList[$i]->getNews_content(),0,85);?>.....</a>
</td>
<td style="border: 1px #CCC solid;  color:#666; text-align:center; font-size:10px;    width:12%">
[<?php echo $newsList[$i]->getNews_date();?>]
</td>
</tr>
<?php 
}
}else {
?>
<tr>
<td style="border: 1px #CCC solid;font-weight:bold; color:#666; text-align:center;" colspan="4">暂无新闻！</td>
</tr>
<?php 
}
?>
</table>
</fieldset>
<?php 
}
?>
</div>
<div style="text-align:left; font-size:14px; font-family:'宋体';font-weight:bold; width:30%; margin-left:1%; margin-right:2%; float:right;">
<fieldset>
<legend><span style="color:#333; font-size:15px; font-weight:bold;">系统开发说明：</span></legend>
<span style="color:#F60; ">本系统主要功能：作业提交系统<br /><br /></span>
<span style="color:#9C0; ">开发环境：Zend 8.0 Wamp 1.5 Mysql 5.1<br /><br /></span>
<span style="color:#999;; ">开发语言：PHP5 HTML JavaScript<br /><br /></span>
<span style="color:#990; ">开发成员：何碧波 吴志伟<br /><br /></span>
<span style="color:#990; "><a href="mailto:hebibo314@163.com" title="发送邮件" style=" text-decoration:none;">Email:hebibo314@163.com</a><br /><br /></span>
</fieldset>
</div>
</center>
</body>
</html>
