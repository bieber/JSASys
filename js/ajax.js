// JavaScript Document\
var xmlHttp;
var parent;
var name;
/*AJAX开始*/
function setXmlHttpRequest()
{
	if(window.ActiveXObject)
	{
		xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
		}
		else if(window.XMLHttpRequest)
		{
			xmlHttp = new XMLHttpRequest();
			}
	}
	
	function ajaxFun(url,funName,obj)
	{
		setXmlHttpRequest();
	    parent = obj.parentNode;
	    name = obj.name;
		url = encodeURI(url);
		xmlHttp.open("GET",url,true);
		xmlHttp.onreadystatechange = funName;
		xmlHttp.send(null);
		}
		function getContentUserName()
		{
			/*此处要注意：必须判断，否则在IE下会出错*/
		
			var content = xmlHttp.responseText;
		
			if(content=="true")
			{
				addSpan("action_stop.gif","该用户名已存在！");
				document.getElementById("submitid").disabled="disabled";
				}
				else
				{
					
					addSpan("icon_accept.gif","该用户名可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					}
				
			}
			/*AJAX结束*/
			
			/*将检测内容显示到前台页面*/
		function getContentXY()
		{
	
			var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该学院名已存在！");
				document.getElementById("submitid").disabled="disabled";
				}
				else
				{
					addSpan("icon_accept.gif","该学院名可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					}
			
}
function checkXY(obj)
{
	var value = obj.value;
	var oldValue = "";
    if(document.getElementById("oldName")!=null)
	{
		oldValue = document.getElementById("oldName").value;
		
	}
	ajaxFun("admin_core/control.php?action=checkXY&xyName="+value+"&oldName="+oldValue,getContentXY,obj);
	}
/*检测学院END*/



/*检测老师是否同名*/
function checkTeacherName(obj)
{
	var value = obj.value;
	var oldValue = "";
    if(document.getElementById("oldName")!=null)
	{
		oldValue = document.getElementById("oldName").value;
	}
	ajaxFun("admin_core/control.php?action=checkTeacherName&lsName="+value+"&oldName="+oldValue,getContentTeacher,obj);
	}
	function getContentTeacher()
	{

			var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该用户名已被注册！");
				document.getElementById("submitid").disabled="disabled";
				document.getElementById('namestate').value=0;
				}
				else
				{
					addSpan("icon_accept.gif","该用户名可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					document.getElementById('namestate').value=1;
					}
			
		}
/*end*/

/*检测学生登录名是否存在*/

function checkStudentName(obj)
{
	var value = obj.value;
	var oldValue = "";
    if(document.getElementById("oldName")!=null)
	{
		oldValue = document.getElementById("oldName").value;
	}
	ajaxFun("admin_core/control.php?action=checkStudentName&xsName="+value+"&oldName="+oldValue,getContentStudent,obj);
	}
	function getContentStudent()
	{
		
		
			var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该用户名已被注册！");
				document.getElementById("submitid").disabled="disabled";
				document.getElementById('namestate').value=0;
				}
				else
				{
					addSpan("icon_accept.gif","该用名户可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					document.getElementById('namestate').value=1;
					}
				
		}
/*end*/
/*检测专业*/
function checkZhY(obj)
{
var value = obj.value;
var oldValue = "";
var xyId = document.getElementById("xyId").value;
if(document.getElementById("oldName")!=null)
{
	oldValue = document.getElementById("oldName").value;
	}
	ajaxFun("admin_core/control.php?action=checkZhY&zhyName="+value+"&oldName="+oldValue+"&xyId="+xyId,getContentZhY,obj);
	}
	
	
	function getContentZhY()
	{
		
	
		var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该专业名已在该学院存在！");
				document.getElementById("submitid").disabled="disabled";
				}
				else
				{
					addSpan("icon_accept.gif","该专业名可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					}
			
		}
		
		/*******专业检测结束*****/

/******检测是否重复班级****/

function checkBJ(obj)
{
	var value = obj.value;
	var zhyId = document.getElementById('zhyId').value;
	var oldValue = "";
	if(document.getElementById("oldName")!=null)
	{
		oldValue = document.getElementById("oldName").value;
		}
		ajaxFun("admin_core/control.php?action=checkBJ&zhyId="+zhyId+"&oldName="+oldValue+"&bjName="+value,getContentBJ,obj);
	}
	function getContentBJ()
	{
		

		var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该班级名已在该专业存在！");
				document.getElementById("submitid").disabled="disabled";
				}
				else
				{
					addSpan("icon_accept.gif","该班级名可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					}
				
		
		}
/*****end**/

/*检测该学生注册的学号是否在该班中注册过*/
function checkXH(obj)
{
	var value = obj.value;
	var oldXH = "";
	if(document.getElementById("oldXH")!=null)
	{
		oldXH = document.getElementById("oldXH").value;
		}
		var xyId = document.getElementById('xy').value;
		var zhyId = document.getElementById('zhy').value;
		var bjId = document.getElementById('bj').value;
		if(xyId=="none"||zhyId=="none"||bjId=="none")
		{
			alert("你未确定你在哪个学院或者那个专业或者哪个班级！请确认！");
			}
			else
			{
				var url = "admin_core/control.php?action=checkXH&xyId="+xyId+"&zhyId="+zhyId+"&bjId="+bjId+"&xh="+value+"&oldXH="+oldXH;
				ajaxFun(url,getContentXH,obj);
				}
	}
	function getContentXH()
	{
		var content = xmlHttp.responseText;
			
			if(content=="true")
			{
				addSpan("action_stop.gif","该学号已注册！");
				document.getElementById("submitid").disabled="disabled";
				}
				else
				{
					addSpan("icon_accept.gif","该学号可用！");
					document.getElementById("submitid").removeAttribute("disabled");
					}
		}
/*end*/
/*添加DOM元素到页面中*/
	function addSpan(image,content)
	{
		var idName = "mes"+name;
		if(document.getElementById(idName)==null)
		{
		    var span = document.createElement("span");
		    span.innerHTML ="<img src='images/"+image+"' width='12' height='12'></img>&nbsp;";
	        span.appendChild(document.createTextNode(content));
			span.style.border = "solid 1px red";
			span.style.backgroundColor = "#CCC";
			span.style.color = "red";
			span.style.width = "30px";
			span.style.fontSize = "12px";
			span.style.marginLeft = "5px";
			span.id ="mes"+name;
	        parent.appendChild(span);
		}
		else 
		{
			var span = document.getElementById("mes"+name);
			span.innerHTML = "<img src='images/"+image+"' width='12' height='12'></img>&nbsp;"+content;
			}
		}
		/*end。。。。。*/