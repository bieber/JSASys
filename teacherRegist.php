﻿<?php 
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
$xyService = new XueYuanService();
$xyList = $xyService->getAllList();
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>作业提交审查管理系统注册 Powered By 江西理工大学 信息工程学院 软件081班</title>
<script src="js/CJL.0.1.min.js"></script>
<script src="js/ImagePreview.js"></script>

<style type="text/css">
<!--
.textFile1 {						width:200px;
						height:20px;
						border:1px solid #39F;
						text-align:left;
						margin-bottom:5px;
						color:#39F;}
.textFile1 {						width:200px;
						height:20px;
						border:1px solid #39F;
						text-align:left;
						margin-bottom:5px;
						color:#39F;}
						select{
							font-size:12px;}
-->
</style>
</head>
<style type="text/css">
#main{
	width:800px;
	margin:auto;}
	#blank{
		width:800px;
		}
		#form
		{
			width:600px;
			margin:auto;
			height:auto;}
			#head
			{
				background-image:url(images/login_12.gif);
				background-repeat:repeat-x;
				height:97px;
				text-align:center;
				color:#09F;
				font-size:24px;
				font-weight:bolder;
				line-height:97px;
				font-family:"宋体";
				border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				}
				.label{
					color:#09F;
					font-size:14px;
					font-family:"幼圆";
					text-align:right;
				}
				table
				{
					
					width:90%;
					margin:auto;}
					.textFile{
						width:200px;
						height:20px;
						border:1px solid #39F;
						text-align:left;
						margin-bottom:5px;
						color:#39F;}
						#top{
							height:7px;}
							#regist{
								border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				
			
								}
						
					
</style>
<script src="js/regist.js" type="text/javascript" charset="utf-8"></script>
<script src="js/ajax.js" type="text/javascript" charset="utf-8"></script>
<body>
<div id="main">
<div id="blank">
<div style="height:50px;">
</div>
<div id="form">
<div id="top">
<img src="images/login_07.gif" width="600"/>
</div>
<div id="head">
老师注册
</div>
<div id="regist">
<form name="regist" id="registForm"  action="admin_core/control.php?action=registTeacher" method="post" enctype="multipart/form-data" >
<table>
<tr>
<td class="label">
登 录 名：
</td>
<td align="left" colspan="2">
<input type="text" name="loginName" id="loginName" class="textFile" onmousemove="addBackGround(this)" onmouseout="removeBackGround(this)"  onkeyup="checkValue(this)" onblur="checkValue(this)" onfocus="alertMessage(this)"/>
<input type="hidden" id="namestate" value="0" />
</td>
</tr>
<tr>
<td class="label">登录密码：
</td>
<td align="left" colspan="2">
<input type="password" name="pwd" id="pwd" class="textFile" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)"/>
</td>
</tr>
<tr>
<td class="label">确认密码：
</td>
<td align="left" colspan="2">
<input type="password" name="pwdAgain" id="pwdAgain" class="textFile" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)"/>
</td>
</tr>
<tr>
<td class="label">
真实姓名：
</td>
<td align="left" colspan="2"><input type="text" name="rname" id="rname" class="textFile1" onmousemove="addBackGround(this)"  onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)"/></td>
</tr>
<tr>
<td class="label">性别：
</td>
<td class="label" style=" text-align:left;" align="left" colspan="2">
<input type="radio" name="xb" value="1"  checked="checked"/> 男 <input type="radio" name="xb" value="2"  /> 女
</td>
</tr>
<tr>
<td class="label">
Email(邮箱)：
</td>
<td align="left" colspan="2">
<input type="text" name="email" id="email" class="textFile" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)"/>
</td>
</tr>
<tr>
<td class="label">电话(手机)：
</td>
<td align="left" colspan="2">
<input type="text" name="tele" id="tele" class="textFile" onmousemove="addBackGround(this)" onkeyup="checkValue(this)" onblur="checkValue(this)" onmouseout="removeBackGround(this)" onfocus="alertMessage(this)"/>
</td>
</tr>

<tr>
<td class="label">学院/部门：
</td>
<td align="left" colspan="2">
<select class="textFile" name="xy" id="xy"  style=" text-align:center;" >
<option style=" text-align:center; " value="none">========请 选 择========</option>
<?php 
for($i = 0; $i<count($xyList); $i++)
{
?>
<option style=" text-align:center;" value="<?php echo $xyList[$i]->getXy_id();?>"><?php echo $xyList[$i]->getXy_name();?></option>
<?php 
}
?>
</select>
</td>
</tr>


<tr>
<td class="label">头像：
</td>
<td align="left" colspan="2">
<img id="file" />
<input id="upload" name="tx" type="file"   style="width:200px;height:20px;border:1px solid #39F;margin-bottom:5px;" onmousemove="addBackGround(this)" onmouseout="removeBackGround(this)"/>

</td>
</tr>
<tr>
<td class="label">验证码：
</td>
<td align="left">
<input type="text" name="validata" id="validata" class="textFile" style="width:100px;"onmousemove="addBackGround(this)" onmouseout="removeBackGround(this)"/></td>
<td>
<img src="admin_core/utils/validate_code.php" id="safecode" style="width:100px; cursor:pointer; height:20px;" onclick="javascript:changeimage();" title="点击换一张"/><span style="cursor:pointer; font-size:14px; color:#666;" onclick="javascript:changeimage();" >看不清！换一张</span>
</td>
</tr>
<tr>
<td align="right">
<input type="button" value=" 注 册 " name="rigist" id="submitid" onclick="submitForm()" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;"  onmouseout="changeButton(this,'url(images/40.gif)')" />
</td>
<td colspan="2">
<input type="reset" value=" 重 置 " name="reset" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)')"/>
</td>
</tr>
</table>
<script>
var ip = new ImagePreview( $$("upload"), $$("file"), {
											maxWidth:100 ,maxHeight:100
});
ip.img.src = ImagePreview.TRANSPARENT;
ip.file.onchange = function(){ ip.preview(); };

</script>
</form>
</div>
<div style=" height:5px;">
<img src="images/login_46.gif" width="600"  />
</div>
</div>
</div>
</div>
</body>
</html>
