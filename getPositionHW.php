<?php 
session_start();
include_once 'admin_core/utils/Function.php';
include_once 'admin_core/models/WenTi.php';
include_once 'admin_core/services/WenTiService.php';
$xs = unserialize($_SESSION['user']);
$wtService = new WenTiService();
$wtList =  $wtService->getUndoWtByXs($xs->getBj()->getBj_id(),$xs->getXs_id());
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>指定作业列表</title>
<style type="text/css">
img{
	border:0px;}
	a{
		text-decoration:none;}
</style>
<script type="text/javascript">
function checkTeacherDetail(lsId)
	{
		window.open('readTeacherDetail.php?lsId='+lsId,'老师信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');
		}
		function readHWDetails(hwId)
		{
			window.open('readHW.php?hwId='+hwId,'作业信息' ,'height=500, width=900, top=0,left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');
			}
				function changeBackColor(obj)
{
obj.style.backgroundColor="#F3F3F3";

	}
	function removeBackColor(obj)
	{
		obj.style.backgroundColor="#FFFFFF";
		}
</script>
</head>
<body>
<center>
<fieldset  style=" width:1110px;">
<legend><img src="images/311.gif"/>&nbsp;<span style="color:#333; font-size:15px; font-weight:bold;">指定作业列表</span></legend>
<table  width="90%" style="border:1px #333 solid; border-collapse:collapse;">
<tr style="background-color:#999;" >
<th height="23" style="border:1px #333 solid; text-align:center; font-size:14px;">作业标题</th>
<th style="border:1px #333 solid; width:15%; text-align:center; font-size:14px;">布置作业的老师</th>
<th style="border:1px #333 solid; width:15%; text-align:center; font-size:14px;">开始提交时间</th>
<th style="border:1px #333 solid; width:15%; text-align:center; font-size:14px;">截至提交时间</th>
<th style="border:1px #333 solid; width:10%; text-align:center; font-size:14px;">作业类型</th>
<th style="border:1px #333 solid; width:10%; text-align:center; font-size:14px;">提交作业</th>
</tr>

<?php 
$now = mktime();
if(count($wtList)>0)
{
for($i=0; $i<count($wtList); $i++)
{
$start = strtotime($wtList[$i]->getStart_time());
$end  = strtotime($wtList[$i]->getEnd_time());
	?>
<tr onmousemove="changeBackColor(this)" onmouseout="removeBackColor(this)">
<td style="border:1px #333 solid;  text-align:left; font-size:13px; color:#666; padding-left:10px;">
<a href="#" onclick="readHWDetails(<?php echo $wtList[$i]->getWt_id();?>)" title="点击查看详细信息">
<?php if (strlen($wtList[$i]->getWt_name())<=30)
echo $wtList[$i]->getWt_name();
else
echo  substr($wtList[$i]->getWt_name(),0,30)."......";
?>

</a>
<span style="font-size:11px; float: right; text-align: left;width: 100px;">[<?php 
if($now<$start)
{
	echo "<font color=blue>还未到提交时间</font>";
}
else if($now>$end)
{
	echo "<font color=red>该作业已停止提交</font>";
}
else 
{
	echo "<font color=green>该作业可提交</font>";
}
?>]</span>
</td>
<td style="border:1px #333 solid; width:15%; text-align:left; font-size:13px;  color:#666; padding-left:10px;" >
<a href="#" onclick="checkTeacherDetail(<?php echo $wtList[$i]->getLS()->getLs_id(); ?>)" title="点击查看老师信息"><?php 
echo $wtList[$i]->getLS()->getLs_name();
?>&nbsp;</a>
</td>
<td style="border:1px #333 solid; width:15%; text-align:center; font-size:13px; color:#666;">
<?php 
echo $wtList[$i]->getStart_time();
?>
</td>
<td style="border:1px #333 solid; width:15%; text-align:center; font-size:13px; color:#666;"><?php 
echo $wtList[$i]->getEnd_time();
?></td>
<td style="border:1px #333 solid; width:10%; text-align:center; font-size:13px; color:#666;">
<?php 
if($wtList[$i]->getWt_type()==0)
echo "选做";
else 
echo "必做"
?>

</td>
<td style="border:1px #333 solid;   width:10%; text-align:center; font-size:14px; color:#666;"><img src="images/anchor_(edit)_16x16.gif" width="14" height="14" />&nbsp;<?php 
if($now>=$start&&$end>=$now)
{
?>
<a href="submitHomework.php?hwId=<?php echo $wtList[$i]->getWt_id();?>" style="color:green;" title="可提交">提交作业</a>
<?php 
}
else if($now<$start)
{
?>
<font color=blue title="还未到提交时间！请稍后..." style="cursor:pointer;">提交作业</font>
<?php 
}
else {
?>
<font color=red title="已过提交时间"  style="cursor:pointer;">提交作业</font>
<?php 
}
?>
</td>
</tr>
<?php 
}
}else
{
?>
<tr>

<td style="border:1px #333 solid; width:15%; text-align:center; font-size:14px;" colspan="6">没有有指定提交作业！</td>
</tr>
<?php 
}
?>
</table>
</fieldset>
</center>
</body>
</html>
