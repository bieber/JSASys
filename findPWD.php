<?php 
include_once 'admin_core/services/XueYuanService.php';
include_once 'admin_core/utils/Function.php';
$xyService = new XueYuanService();
$xyList = $xyService->getAllList();
$fun = new fun();
$fun->closeDB();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>作业提交审查管理系统注册 Powered By 江西理工大学 信息工程学院 软件081班</title>
</head>
<style type="text/css">
#main{
	width:800px;
	margin:auto;}
	#blank{
		width:800px;
		}
		#form
		{
			width:600px;
			margin:auto;
			height:auto;}
			#head
			{
				background-image:url(images/login_12.gif);
				background-repeat:repeat-x;
				height:97px;
				text-align:center;
				color:#09F;
				font-size:24px;
				font-weight:bolder;
				line-height:97px;
				font-family:"宋体";
				border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				}
				.label{
					color:#09F;
					font-size:14px;
					font-family:"幼圆";
					text-align:right;
					width:25%;
				}
				table
				{
					
					width:90%;
					margin:auto;}
					.textFile{
						width:200px;
						height:20px;
						border:1px solid #39F;
						text-align:left;
						margin-bottom:5px;
						color:#39F;}
						#top{
							height:7px;}
							#regist{
								border-left:#CCC 1px solid;
				border-right:#CCC 1px solid;
				
			
								}
								select{
									font-size:12px;}
						
					
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
		function JqueryShow(nodeObj)
		{
			nodeObj.show();
			}
			function Jqueryhidden(nodeObj)
		{
			nodeObj.hide();
			}
function changeimage()
{
document.getElementById("safecode").src='admin_core/utils/validate_code.php?time'+Math.random();
}
function changeButton(obj,image)
{
obj.style.backgroundImage = image;

	}
	function checkValue()
	{
		var type = document.getElementById('userType').value;
		var validata = document.getElementById('validata').value;
		if(type==1)
		{
			var bj =document.getElementById('bj').value;
			var xh = document.getElementById('xh').value;
		if(xh==""&&xh.search(/[0-9]/)==-1)
		{
			alert("学号格式不正确！");}
		
		else if(bj=="none")
		{
			alert("请选择班级！");
			}
			else if(validata=="")
			{
				alert("请输入验证码！");
				}
				else 
				{
					document.getElementById('findForm').submit();
					
					}
		}
		else
		{
			var rname = document.getElementById('rname').value;
			var tele = document.getElementById('tele').value;
			var xy = document.getElementById('xyLs').value;
			if(rname=="")
			{
				alert("请输入真实姓名！");
				}
				else if(tele=="")
				{
					alert("请输入验证码！");
					}
					else if(xy=="none")
					{
						alert("请选择所属学院/部门！");
						}
						else
						{
							document.getElementById('findForm').submit();
							}
			}
	
		}
		function changeType(obj)
		{
			if(obj.value==1)
			{
				Jqueryhidden($("#teacher"));
				JqueryShow($("#student"));
				//$("#teacher").css("display","none");
				//$("#student").css("display","table");
				//document.getElementById('teacher').style.display='none';
				//document.getElementById('student').style.display='table';
				}
				else
				{
					Jqueryhidden($("#student"));
				JqueryShow($("#teacher"));
					}
			}
			function resetFun()
			{
				Jqueryhidden($("#student"));
				JqueryShow($("#teacher"));
				}

</script>
<script src="js/registAjax.js" type="text/javascript" charset="gbk"></script>
<body>
<div id="main">
<div id="blank">
<div style="height:50px;">
</div>
<div id="form">
<div id="top">

<img src="images/login_07.gif" width="600"/>
</div>
<div id="head">找回密码</div>
<div id="regist">
<form name="regist" action="admin_core/helpControl.php?action=findPWD" method="post" id="findForm" >
<table>
<tr>
<td class="label">
选择你的用户类型：
</td>
<td align="left" colspan="2">

<select id="userType" name="userType" class="textFile" onchange="changeType(this)">
<option value="1">========学 生========</option>
<option value="2" selected="selected">========老 师========</option>
</select>
</td>
</tr>
</table>
<table id="teacher">
<tr>
<td class="label">
真实姓名：
</td>
<td align="left" colspan="2"><input type="text" name="rname" id="rname" class="textFile" /></td>
</tr>
<tr>
<tr>
<td class="label">电话(手机)：
</td>
<td align="left" colspan="2">
<input type="text" name="tele" id="tele" class="textFile" />
</td>
</tr>
<tr>
<td class="label">学院/部门：
</td>
<td align="left" colspan="2">
<select class="textFile" name="xyLs" id="xyLs"  style=" text-align:center; float:left;" >
<option style=" text-align:center; " value="none">========请 选 择========</option>
<?php 
for($i = 0; $i<count($xyList); $i++)
{
?>
<option style=" text-align:center;" value="<?php echo $xyList[$i]->getXy_id();?>"><?php echo $xyList[$i]->getXy_name();?></option>
<?php 
}
?>
</select>
</td>
</tr>
</table>
<table id="student" style="display:none;">
<tr>
<td class="label">学院：
</td>
<td align="left">
<select class="textFile" name="xy" id="xy" style="text-align:center;" onchange="getZhy(this)">
<option  value="none">=========请 选 择=========</option>
<?php 
for($i = 0; $i<count($xyList); $i++)
{
?>
<option style=" text-align:center;" value="<?php echo $xyList[$i]->getXy_id();?>"><?php echo $xyList[$i]->getXy_name();?></option>
<?php 
}
?>
</select>
</td>
</tr>
<tr>
<td class="label">专业：
</td>
<td align="left">
<span id="zhySpan">
<select class="textFile" name="zhy" id="zhy" style="text-align:center;" onchange="getBJ(this)"  >
<option style=" text-align:center;" value="none">=========请 选 择=========</option>
</select>
</span>

</td>
</tr>
<tr>
<td class="label">班级：
</td>
<td align="left">
<span id="bjSpan">
<select class="textFile" name="bj" id="bj" style="text-align:center;" >
<option style=" text-align:center;" value="none">========请 选 择========</option>
</select>
</span>

</td>
</tr>
<tr>
<td colspan="2" align="center">
<span style="font-size:11px; color:#F00;" >(*在学号信息之前请正确的选择学院、专业、班级。否则无效！)</span>
</td>
</tr>
<tr>
<td class="label">学号：
</td>
<td>
<input type="text" name="xh" id="xh" class="textFile"  />
</td>
</tr>
</table>
<table id="comment">
<tr>
<td class="label">验证码：
</td>
<td>
<input type="text" name="validata" id="validata" class="textFile" style="width:100px;" />&nbsp;<img src="admin_core/utils/validate_code.php" id="safecode" style="width:100px; cursor:pointer; height:20px;" onclick="javascript:changeimage();" title="点击换一张"/><span style="cursor:pointer; font-size:14px; color:#666;" onclick="javascript:changeimage();" >看不清！换一张</span>
</td>
</tr>
<tr>
<td align="right">
<input type="button" value=" 提 交 " name="rigist" id="button" onclick="checkValue()" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)')" />
</td>
<td>
<input type="reset" value=" 重 置 " name="reset" onclick="resetFun()" onmousemove="changeButton(this,'url(images/61.gif)')" style="background-image:url(images/40.gif); width:65px; height:23px; border:0px;" onmouseout="changeButton(this,'url(images/40.gif)')" />
</td>
</tr>
</table>

</form>
</div>
<div style=" height:5px;">
<img src="images/login_46.gif" width="600"  />
</div>
</div>
</div>
</div>
</body>
</html>
